﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Lysis
{
    public class DebugFileParser
    {
        public static string[] notStringNamesArgs = new string[] {
            "origin"
        };

        static string[] stringNamesArgs = new string[] {
            "function",
            "cmd",
            "message",
            "info",
            "output",
            "value",
            "name",
            "team",
            "string",
            "text",
            "buffer",
            "authid",
            "ip",
            "file",
            "cvar",
            "cvarname",
            "format"
        };

        public class DefinedEnum
        {
            public class EnumValue
            {
                public EnumValue(string key, int value)
                {
                    this.key = key;
                    this.value = value;
                }

                string key;
                int value;

                public string Key
                {
                  get { return key; }
                }

                public int Value
                {
                  get { return this.value; }
                }
            }

            public DefinedEnum(string name)
            {
                this.name = name;
                this.defined = new List<EnumValue>();
            }

            public void Add(EnumValue value)
            {
                defined.Add(value);
            }

            public int Count()
            {
                return defined.Count();
            }

            public EnumValue LookupValue(int value)
            {
                return defined.First(e => e.Value == value);
            }

            string name;
            List<EnumValue> defined;

            public string Name
            {
                get { return name; }
            }

            public List<EnumValue> Defined
            {
                get { return defined; }
            }
        }

        List<Tag> tags;
        List<DefinedEnum> enums;
        List<Native> natives;

        public DebugFileParser(string[] dirs, List<Tag> tags_)
        {
            tags = tags_;
            enums = new List<DefinedEnum>();
            natives = new List<Native>();

            foreach (var dir in dirs)
            {
                try
                {
                    ParseDir(dir);
                }
                catch { }
            }
        }

        public DefinedEnum.EnumValue LookupValue(string enumname, int value)
        {
            try
            {
                return enums.First(e => e.Name == enumname).LookupValue(value);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void ParseDir(string dir)
        {
            if (!Directory.Exists(dir))
                throw new Exception("Dir not exists.");

            string[] files = Directory.GetFiles(dir, "*.inc");

            foreach (var file in files)
            {
                using (StreamReader reader = new StreamReader(file))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.StartsWith(";") || line.StartsWith("/") || line.StartsWith(" ") || line.StartsWith("*"))
                            continue;

                        if (line.StartsWith("enum"))
                        {
                            try
                            {
                                ParseEnum(reader, line);
                            }
                            catch (Exception) { }
                        }
                        else if (line.StartsWith("native"))
                        {
                            ParseNative(line);
                        }
                    }
                }
            }
        }

        /*public List<Tag> getTags()
        {
            return tags;
        }*/

        public void writeNativeDebug(Native[] natives_)
        {
            foreach (var n in natives_)
            {
                for (int i = 0; i < natives.Count; i++)
                {
                    if (n.name == natives[i].name)
                    {
                        Native rnative = natives[i];
                        n.setDebugInfo((int)rnative.tag_id, rnative.returnType, rnative.args);
                    }
                }
            }
        }

        void ParseEnum(StreamReader reader, string firstline)
        {
            // Remove unnecessary things.
            firstline = firstline.Substring("enum".Length).Trim();

            if (firstline == "")
                return;

            firstline = removeComment(firstline).Trim();

            if (firstline == "" || firstline == "{")        // If the enum has no name.
                return;

            if (firstline.IndexOf('{') != -1)
                firstline = firstline.Substring(0, firstline.IndexOf('{')).Trim();

            if (firstline == "")
                return;

            DefinedEnum denum = new DefinedEnum(firstline);

            int lastvalue = -1;  // Enums by default starts with 0, and set to first -1 + 1 = 0

            string line;
            while ((line = reader.ReadLine()) != null)
            {
                line = removeComment(line).Trim();

                if (line == "{" || line == "")
                    continue;
                if (line.StartsWith("}"))
                    break;

                if (!line.StartsWith("#define") && !char.IsLetter(line[0]))
                    continue;

                string key = "", value = "";

                if (line.StartsWith("#define"))
                {
                    line = line.Substring("#define".Length).Trim();

                    string[] lines = line.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                    if (lines.Length != 2)
                        continue;

                    key = lines[0];
                    value = lines[1];

                    if (key == "" || value == "")
                        continue;

                    int intvalue = parseValue(value);

                    denum.Add(new DefinedEnum.EnumValue(key, intvalue));
                }
                else
                {
                    if (line.IndexOf(',') != -1)
                        line = line.Substring(0, line.IndexOf(',')).Trim();

                    string[] lines = line.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);

                    if (lines.Length > 2)
                        continue;

                    int intvalue = lastvalue + 1;

                    key = lines[0].Trim();

                    if (lines.Length == 2)
                    {
                        value = lines[1].Trim();
                        intvalue = parseValue(value);
                    }

                    denum.Add(new DefinedEnum.EnumValue(key, intvalue));
                    lastvalue = intvalue;
                }
            }

            if (denum.Count() != 0)
                enums.Add(denum);
        }

        public int parseValue(string str)
        {
            if (str.IndexOf("<<") != -1)
            {
                try
                {
                    str = str.Replace("(", "");
                    str = str.Replace(")", "");

                    string[] args = str.Split(new string[] { "<<" }, StringSplitOptions.RemoveEmptyEntries);

                    int oper1 = int.Parse(args[0].Trim());
                    int oper2 = int.Parse(args[1].Trim());

                    return (oper1 << oper2);
                }
                catch (Exception)
                {
                    return -100500;
                }
            }
            else
            {
                try
                {
                    return int.Parse(str);
                }
                catch (Exception)
                {
                    return -1337;
                }
            }
        }

        public string removeComment(string str)
        {
            int comment1 = str.IndexOf("//");
            int comment2 = str.IndexOf(';');
            int comment3 = str.IndexOf("/*");   // I know, this not correct, but in include files "/*" located in the end of line.
            comment1 = comment1 == -1 ? int.MaxValue : comment1;
            comment2 = comment2 == -1 ? int.MaxValue : comment2;
            comment3 = comment3 == -1 ? int.MaxValue : comment3;

            int comment = Math.Min(comment1, comment2);
            comment = Math.Min(comment, comment3);

            if (comment != int.MaxValue)
            {
                return str.Substring(0, comment);
            }

            return str;
        }

        void ParseNative(string line)
        {
            line = line.Substring("native".Length).Trim();
            line = line.Substring(0, line.LastIndexOf(')'));

            string[] strargs = line.Split(new char[] {'(', ','});

            Argument arg = ParseArg(strargs[0]);

            Native native = new Native(arg.name, 0);
            
            var args = new List<Argument>();

            for (int i = 1; i < strargs.Length; i++)
            {
                if (strargs[i] != "")
                    args.Add(ParseArg(strargs[i]));
            }

            native.setDebugInfo((int)arg.tag.tag_id, arg.tag, args.ToArray());

            natives.Add(native);
        }

        Argument ParseArg(string str)
        {
            str = str.Trim();

            if (str.StartsWith("const"))
                str = str.Substring("const".Length).Trim();

            bool isref = str[0] == '&';

            if (isref)
            {
                str = str.Substring(1).Trim();
            }

            string tagname = "_";
            int dpoint = str.IndexOf(':');

            if (dpoint != -1)
            {
                tagname = str.Substring(0, dpoint).Trim();
                str = str.Substring(dpoint + 1);

                if (tagname == "")
                    tagname = "_";
            }

            int dimcount = str.Count(c => c == '[');
            
            if (dimcount > 0)
                str = str.Substring(0, str.IndexOf('['));
            str = str.Trim();

            VariableType type = VariableType.Normal;

            if (dimcount > 0)
            {
                type = VariableType.ArrayReference; // Or Array?
            }
            else if (isref)
            {
                type = VariableType.Reference;
            }
            else if (tagname == "any" || str == "...")
            {
                type = VariableType.Variadic;
            }

            Tag tag = findOrCreateTag(tagname);

            // When String not set in the debug file, try detect String argument
            if (type == VariableType.ArrayReference && dimcount == 1 && tag.name == "_")
            {
                if (stringNamesArgs.Any(s => str == s))
                {
                    tag = findOrCreateTag("String");
                }
            }

            return new Argument(type, str, (int)tag.tag_id, tag, dimcount > 0 ? new Dimension[dimcount] : null);
        }

        Tag findOrCreateTag(string TagName)
        {
            for (int i = 0; i < tags.Count; i++)
            {
                if (tags[i].name == TagName)
                    return tags[i];
            }

            Tag newtag = new Tag(TagName, tags[tags.Count - 1].tag_id + 1);
            tags.Add(newtag);

            return newtag;
        }

        public static bool NotStringArgument(string arg_name)
        {
            return notStringNamesArgs.Contains(arg_name);
        }
    }
}
