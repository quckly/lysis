﻿using System;
using System.IO;
using System.IO.Compression;
using System.Collections;
using System.Collections.Generic;
using Lysis;

using Amx.Cod;
using Amx.Dat;
using Amx.Strings;
using Amx.Ops;
using Amx.Logic.Jumps;
using Amx.Logic.Debug;  // class name clash with system.IO.File and Debug.File
using Amx.Logic.Labels;
using Amx.Core;

using Files;
using Files.Amx;

namespace AMXModX
{
    public class AmxxLysisContainer : IAmxContainer
    {
        public AmxxLysisContainer(IAmxInstance _amx, byte[] _binary)
        {
            this.amx = _amx;
            this.binary = _binary;
        }

        IAmxInstance amx;
        byte[] binary;

        public virtual int Count
        {
            get { return 1; }
        }

        public virtual string Path
        {
            get { return "."; }
        }

        public virtual string Name
        {
            get { return "Plugin"; }
        }

        public virtual AmxStream GetStream(IAmxDescription description)
        {
            return new AmxStream(new MemoryStream(binary, 0, binary.Length));
        }

        public virtual IAmxInstance GetFile(IAmxDescription description)
        {
            return amx;
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return null;
        }

        #endregion
    }

    public class AmxFileLysis : AmxFile
    {
        public AmxFileLysis(byte[] binary)
        {
            MemoryStream amx_stream = new MemoryStream();
            amx_stream.Write(binary, 0, binary.Length);
            amx_stream.Seek(0L, SeekOrigin.Begin);

            AmxFileLoad(amx_stream, Bits._32, ".");

            this.container = new AmxxLysisContainer(this, binary);
            this.internaldescription = null;
        }
    }

    public class AMXWraithFile : AMXModXFile
    {
        protected Disassembly disassembly;

        public AMXWraithFile(byte[] binary)
            : base(binary)
        {
            var file = new AmxFileLysis(binary);
            var ops = new Ops("opcodes.txt");

            disassembly = new Disassembly(file, ops);
            disassembly.Disassemble();

            // Set functions
            List<Function> functions = new List<Function>();

            foreach (var it in Publics)
            {
                functions.Add(new Function(it.address, it.address, (uint)this.code_.bytes.Length, it.name, FindOrCreateTag("_")));
            }

            foreach (DictionaryEntry it in disassembly.PrivateFunctions)
            {
                functions.Add(new Function((uint)((int)it.Key), (uint)((int)it.Key), (uint)this.code_.bytes.Length, (string)it.Value, FindOrCreateTag("_")));
            }
            
            functions.Sort(delegate(Function fun1, Function fun2)
            {
                return (int)(fun1.address - fun2.address);
            });

            functions_ = functions.ToArray();

            // For every function, set arguments and codeEnd;
            for (int i = 0; i < functions_.Length; i++)
            {
                Function fun = functions_[i];
                fun.setArguments(new List<Argument>());

                if (i < functions_.Length - 1)
                    fun.setCodeEnd(functions_[i + 1].codeStart);
            }
            
            // Variables
            globals_ = new Variable[0];
            variables_ = new Variable[0];
            allvars_ = new Variable[0];
            
        }
    }

    
}