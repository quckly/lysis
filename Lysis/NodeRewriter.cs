﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using SourcePawn;

namespace Lysis
{
    public class NodeRewriter : NodeVisitor
    {
        private NodeGraph graph_;
        private NodeBlock current_;
        private NodeList.iterator iterator_;

        public override void visit(DConstant node)
        {
        }
        public override void visit(DDeclareLocal local)
        {
        }
        public override void visit(DLocalRef lref)
        {
        }
        public override void visit(DJump jump)
        {
        }

        public override void visit(DJumpCondition jcc)
        {
        }

        public override void visit(DSysReq sysreq)
        {
            if (sysreq.numOperands != 2)
                return;
            // Replace basic float operation natives with normal inline syntax.
            // FloatAdd(x, y) gets x +  y
            SPOpcode spop;
            switch (sysreq.native.name.ToLower())
            {
                case "floatadd":
                    spop = SPOpcode.add;
                    break;
                case "floatsub":
                    spop = SPOpcode.sub;
                    break;
                case "floatmul":
                    spop = SPOpcode.smul;
                    break;
                case "floatdiv":
                    spop = SPOpcode.sdiv_alt;
                    break;
                default:
                    return;
            }

            DNode lhs, rhs;

            // For commutative operations, try to avoid having a constant on the lhs.
            // This helps to coalesce to = and *=
            if ((spop == SPOpcode.add || spop == SPOpcode.smul)
            && sysreq.getOperand(0).type == NodeType.DeclareLocal)
            {
                lhs = sysreq.getOperand(1);
                rhs = sysreq.getOperand(0);
            }
            else
            {
                lhs = sysreq.getOperand(0);
                rhs = sysreq.getOperand(1);
            }

            DBinary binary = new DBinary(spop, lhs, rhs);
            sysreq.getOperand(0).addType(new TypeUnit(new PawnType(CellType.Float)));
            sysreq.getOperand(1).addType(new TypeUnit(new PawnType(CellType.Float)));
            sysreq.replaceAllUsesWith(binary);
            sysreq.removeFromUseChains();
            current_.replace(iterator_, binary);
            return;
        }

        public override void visit(DBinary binary)
        {
        }

        public override void visit(DBoundsCheck check)
        {
        }
        public override void visit(DArrayRef aref)
        {
#if B
            DNode node = aref.getOperand(0);
            DNode replacement = node.applyType(graph_.file, null, VariableType.ArrayReference);
            if (replacement != node)
            {
                node.block.replace(node, replacement);
                aref.replaceOperand(0, replacement);
            }
#endif
        }
        public override void visit(DStore store)
        {
        }
        public override void visit(DLoad load)
        {
        }
        public override void visit(DReturn ret)
        {
        }
        public override void visit(DGlobal global)
        {
        }
        public override void visit(DString node)
        {
        }

        public override void visit(DPhi phi)
        {
            // Convert a phi into a move on each incoming edge. Declare the
            // temporary name in the dominator.
            NodeBlock idom = graph_[phi.block.lir.idom.id];

            DTempName name = new DTempName(graph_.tempName());
            idom.prepend(name);

            for (int i = 0; i < phi.numOperands; i++)
            {
                DNode input = phi.getOperand(i);
                DStore store = new DStore(name, input);
                NodeBlock pred = graph_[phi.block.lir.getPredecessor(i).id];
                pred.prepend(store);
            }

            phi.replaceAllUsesWith(name);
        }

        public override void visit(DCall call)
        {
            // Operators can be overloaded for floats, and we want these to print
            // normally, so here is some gross peephole stuff. Maybe we should be
            // looking for bytecode patterns instead or something, but that would
            // need a whole-program analysis.
            if (call.function.name.Length < 8)
                return;
            if (call.function.name.Substring(0, 8) != "operator")
                return;

            string op = "";
            for (int i = 8; i < call.function.name.Length; i++)
            {
                if (call.function.name[i] == '(')
                    break;
                op += call.function.name[i];
            }

            SPOpcode spop;
            if (call.numOperands == 2)
            {
                 switch (op)
                 {
                     // Binary operators (%[modulo] isn't implemented for Floats)
                     case ">":
                         spop = SPOpcode.sgrtr;
                         break;
                     case ">=":
                         spop = SPOpcode.sgeq;
                         break;
                     case "<":
                         spop = SPOpcode.sless;
                         break;
                     case "<=":
                         spop = SPOpcode.sleq;
                         break;
                     case "==":
                         spop = SPOpcode.eq;
                         break;
                     case "!=":
                         spop = SPOpcode.neq;
                         break;
                     case "+":
                         spop = SPOpcode.add;
                         break;
                     case "-":
                         spop = SPOpcode.sub;
                         break;
                     case "*":
                         spop = SPOpcode.smul;
                         break;
                     case "/":
                         spop = SPOpcode.sdiv_alt;
                         break;
                     default:
                         throw new Exception(String.Format("unknown operator ({0})", op.ToString()));
                 }
             }
             else // if(call.numOperands == 1)
             {
                 switch (op)
                 {
                     // Unary operators
                     case "-":
                         spop = SPOpcode.neg;
                         break;
                     case "!":
                         spop = SPOpcode.not;
                         break;
                     case "++":
                         spop = SPOpcode.inc;
                         break;
                     case "--":
                         spop = SPOpcode.dec;
                         break;
                     default:
                         throw new Exception(String.Format("unknown operator ({0})", op.ToString()));
                 }
             }

            switch (spop)
            {
                case SPOpcode.sgeq:
                case SPOpcode.sleq:
                case SPOpcode.sgrtr:
                case SPOpcode.sless:
                case SPOpcode.sdiv:
                case SPOpcode.smul:
                case SPOpcode.eq:
                case SPOpcode.neq:
                case SPOpcode.add:
                case SPOpcode.sub:
                case SPOpcode.sdiv_alt:
                {
                    //if (call.numOperands != 2)
                    //    return;
                    DBinary binary = new DBinary(spop, call.getOperand(0), call.getOperand(1));
                    call.replaceAllUsesWith(binary);
                    call.removeFromUseChains();
                    current_.replace(iterator_, binary);
                    break;
                }
                // Incremental/Decremental operators ++/--
                case SPOpcode.inc:
                case SPOpcode.dec:
                {
                    // Replace var++/--; statements with var += 1.0;
                    // So create the 1 as rhs of that binary.
                    DBinary binary = new DBinary((spop == SPOpcode.inc?SPOpcode.add:SPOpcode.sub), call.getOperand(0), new DFloat(1.0f));
                    call.replaceAllUsesWith(binary);
                    call.removeFromUseChains();
                    current_.replace(iterator_, binary);
                    break;
                }
                // Only real unary operators for Floats
                case SPOpcode.neg:
                case SPOpcode.not:
                {
                    DUnary unary = new DUnary(spop, call.getOperand(0));
                    call.replaceAllUsesWith(unary);
                    call.removeFromUseChains();
                    current_.replace(iterator_, unary);
                    break;
                }
                default:
                    throw new Exception("unknown spop");
            }
        }

        private void rewriteBlock(NodeBlock block)
        {
            current_ = block;
            iterator_ = block.nodes.begin();
            while (iterator_.more())
            {
                // Iterate before accepting so we can replace the node.
                iterator_.node.accept(this);
                iterator_.next();
            }
        }

        public NodeRewriter(NodeGraph graph)
        {
            graph_ = graph;
        }

        public void rewrite()
        {
            // We rewrite nodes in forward order so they are collapsed by the time we see their uses.
            for (int i = 0; i < graph_.numBlocks; i++)
                rewriteBlock(graph_[i]);
        }
    }
}
