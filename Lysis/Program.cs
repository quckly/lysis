﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SourcePawn;

namespace Lysis
{
    class RemoveDeadCodeException : Exception
    {
        public RemoveDeadCodeException()
        {
        }

    }

    class Program
    {
        public delegate void DumpMethodDelegate(PawnFile file, SourceBuilder source, Function func);

        static void DumpMethod(PawnFile file, SourceBuilder source, uint addr)
        {
            MethodParser mp = new MethodParser(file, addr);
            LGraph graph = mp.parse();
            //DebugSpew.DumpGraph(graph.blocks, System.Console.Out);

            NodeBuilder nb = new NodeBuilder(file, graph);
            NodeBlock[] nblocks = nb.buildNodes();

            NodeGraph ngraph = new NodeGraph(file, nblocks);

            // Remove dead phis first.
            if (RemoveDeadCode)
                NodeAnalysis.RemoveDeadCode(ngraph);

            NodeRewriter rewriter = new NodeRewriter(ngraph);
            rewriter.rewrite();

            NodeAnalysis.CollapseArrayReferences(ngraph);

            // Propagate type information.
            ForwardTypePropagation ftypes = new ForwardTypePropagation(ngraph);
            ftypes.propagate();

            BackwardTypePropagation btypes = new BackwardTypePropagation(ngraph);
            btypes.propagate();

            // We're not fixpoint, so just iterate again.
            ftypes.propagate();
            btypes.propagate();

            // Try this again now that we have type information.
            NodeAnalysis.CollapseArrayReferences(ngraph);

            ftypes.propagate();
            btypes.propagate();

            // Coalesce x[y] = x[y] + 5 into x[y] += 5
            NodeAnalysis.CoalesceLoadStores(ngraph);

            // After this, it is not legal to run type analysis again, because
            // arguments expecting references may have been rewritten to take
            // constants, for pretty-printing.
            NodeAnalysis.AnalyzeHeapUsage(ngraph);

            // Do another DCE pass, this time, without guards.
            NodeAnalysis.RemoveGuards(ngraph);
            NodeAnalysis.RemoveDeadCode(ngraph);

            NodeRenamer renamer = new NodeRenamer(ngraph);
            renamer.rename();

            // Do a pass to coalesce declaration+stores.
            NodeAnalysis.CoalesceLoadsAndDeclarations(ngraph);

            // Simplify conditional expressions.
            // BlockAnalysis.NormalizeConditionals(ngraph);
            var sb = new SourceStructureBuilder(ngraph);
            var structure = sb.build();

            source.write(structure);

            //System.Console.In.Read();
            //System.Console.In.Read();
        }

        static Function FunctionByName(SourcePawnFile file, string name)
        {
            for (int i = 0; i < file.Functions.Length; i++)
            {
                if (file.Functions[i].name == name)
                    return file.Functions[i];
            }
            return null;
        }

        public static string BuildFloatSpec = "F";

        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                System.Console.Error.Write("usage: <file.smx> or <file.amxx>");
                return;
            }

            SetFormat();

            string path = args[0];
            PawnFile file = PawnFile.FromFile(path);
            SourceBuilder source = new SourceBuilder(file, System.Console.Out);
            source.writeGlobals();

            for (int i = 0; i < file.Functions.Length; i++)
            {
                Function fun = file.Functions[i];

                try
                {
                    //DumpMethod(file, source, fun.address);

                    DumpMethodDelegate dlg = new DumpMethodDelegate(ThreadDecompileFunction);
                    IAsyncResult res = dlg.BeginInvoke(file, source, fun, null, null);
                    bool completed = res.AsyncWaitHandle.WaitOne(20 * 1000, true);  // time to finish decompile

                    if (!completed)
                        throw new Exception("Decompile function " + fun.name + " timeout");

                    System.Console.WriteLine("");
                }
                catch (Exception e)
                {
                    System.Console.WriteLine("");
                    System.Console.WriteLine("/ * ERROR! " + e.Message + " * /");
                    System.Console.WriteLine(" function \"" + fun.name + "\" (number " + i + ")");
                    source = new SourceBuilder(file, System.Console.Out);
                }

            }

            int breakpointvar = 0;
        }

        public const bool DefaultRemovePhiDeadCode = true;                  // Normally must set to True

        public static bool RemovePhiDeadCode = DefaultRemovePhiDeadCode;
        public static bool RemoveDeadCode = true;                            // Normally must set to True

        public static void ThreadDecompileFunction(PawnFile file, SourceBuilder source, Function func)
        {
            SetFormat();

#if false
            if (func.name == "RespawnMenuHandler")
            {
                try
                {
                    DumpMethod(file, source, func.address);
                }
                catch (RemoveDeadCodeException)
                {
                    Program.RemovePhiDeadCode = false;

                    DumpMethod(file, source, func.address);
                }
                finally
                {
                    Program.RemovePhiDeadCode = true;
                }
            }
#else
            try
            {
                Program.RemovePhiDeadCode = DefaultRemovePhiDeadCode;
                DumpMethod(file, source, func.address);
            }
            catch (RemoveDeadCodeException)
            {
                if (Program.RemovePhiDeadCode != false)
                {
                    Program.RemovePhiDeadCode = false;
                    DumpMethod(file, source, func.address);
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("");
                System.Console.WriteLine("/ * ERROR! " + e.Message + " * /");
                System.Console.WriteLine(" function \"" + func.name + "\"");

                source = new SourceBuilder(file, System.Console.Out);
                source.writeSignature(func);
            }
#endif
        }

        public static void SetFormat()
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }
    }
}
