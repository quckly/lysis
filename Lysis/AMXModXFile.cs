using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using Lysis;

namespace AMXModX
{
    public class AMXModXFile : PawnFile
    {
        public const uint MAGIC2 = 0x414D5858;
        private const uint MAGIC2_VERSION = 0x0300;
        private const uint AMX_MAGIC = 0xF1E0;
        private const uint AMX_DBG_MAGIC = 0xF1EF;
        private const uint MIN_FILE_VERSION = 6;
        private const uint MIN_DEBUG_FILE_VERSION = 8;
        private const uint CUR_FILE_VERSION = 8;
        private const int DEFSIZE = 8;
        private const int AMX_FLAG_DEBUG = 0x2;
        private const int AMX_FLAG_COMPACT = 0x4;
        private const byte IDENT_VARIABLE = 1;
        private const byte IDENT_REFERENCE = 2;
        private const byte IDENT_ARRAY = 3;
        private const byte IDENT_REFARRAY = 4;
        private const byte IDENT_FUNCTION = 9;
        private const byte IDENT_VARARGS = 11;

        protected const uint Q_USER_TAG_STRING = 100500;

        protected class PluginHeader
        {
            public byte cellsize;
            public int disksize;
            public int imagesize;
            public int memsize;
            public int offset;
        }

        protected class AMX_HEADER
        {
            public int size;
            public ushort magic;
            public byte file_version;
            public byte amx_version;
            public short flags;
            public short defsize;
            public int cod;
            public int dat;
            public int hea;
            public int stp;
            public int cip;
            public int publics;
            public int natives;
            public int libraries;
            public int pubvars;
            public int tags;
            public int nametable;
        }

        protected class AMX_DEBUG_HDR
        {
            public int size;
            public ushort magic;
            public byte file_version;
            public byte amx_version;
            public short flags;
            public short files;
            public short lines;
            public short symbols;
            public short tags;
            public short automatons;
            public short states;

            public const int SIZE = 4 +
                                    2 +
                                    (1 * 2) +
                                    (4 * 7);
        }

        protected Variable[] allvars_;
        public DebugFileParser parser;

        public static PawnFile GetPawnFile(byte[] binary)
        {
            // Unpack amxx container
            BinaryReader reader = new BinaryReader(new MemoryStream(binary));
            uint magic = reader.ReadUInt32();

            switch (magic)
            {
                case MAGIC2:
                    {
                        uint version = reader.ReadUInt16();
                        if (version > MAGIC2_VERSION)
                            throw new Exception("unexpected version");

                        PluginHeader ph = null;
                        uint numPlugins = reader.ReadByte();
                        for (uint i = 0; i < numPlugins; i++)
                        {
                            PluginHeader p = new PluginHeader();
                            p.cellsize = reader.ReadByte();
                            p.disksize = reader.ReadInt32();
                            p.imagesize = reader.ReadInt32();
                            p.memsize = reader.ReadInt32();
                            p.offset = reader.ReadInt32();
                            if (p.cellsize == 4)
                            {
                                ph = p;
                                break;
                            }
                        }
                        if (ph == null)
                            throw new Exception("could not find applicable cell size (32 bit)");

                        int bufferSize = ph.imagesize > ph.memsize
                                         ? ph.imagesize + 1
                                         : ph.memsize + 1;
                        byte[] bits = new byte[bufferSize];

                        MemoryStream ms = new MemoryStream(binary, ph.offset + 2, ph.disksize - 2);
                        DeflateStream gzip = new DeflateStream(ms, CompressionMode.Decompress);
                        int read = gzip.Read(bits, 0, bufferSize);
                        if (read != ph.imagesize)
                            throw new Exception("uncompress error");
                        binary = bits;
                        break;
                    }

                default:
                    throw new Exception("unrecognized file");
            }

            // Select debug or no debug amx file parser
            reader = new BinaryReader(new MemoryStream(binary));

            reader.ReadBytes(8);    // amx.flags 8-offset
            short flags = reader.ReadInt16();

            if ((flags & AMX_FLAG_DEBUG) == AMX_FLAG_DEBUG)
            {
                return new AMXModX.AMXModXFile(binary);
            }
            else
            {
                return new AMXModX.AMXWraithFile(binary);
            }
        }

        public AMXModXFile(byte[] binary)
        {
            BinaryReader reader = new BinaryReader(new MemoryStream(binary));

            AMX_HEADER amx = new AMX_HEADER();
            amx.size = reader.ReadInt32();
            amx.magic = reader.ReadUInt16();
            amx.file_version = reader.ReadByte();
            amx.amx_version = reader.ReadByte();
            amx.flags = reader.ReadInt16();
            amx.defsize = reader.ReadInt16();
            amx.cod = reader.ReadInt32();
            amx.dat = reader.ReadInt32();
            amx.hea = reader.ReadInt32();
            amx.stp = reader.ReadInt32();
            amx.cip = reader.ReadInt32();
            amx.publics = reader.ReadInt32();
            amx.natives = reader.ReadInt32();
            amx.libraries = reader.ReadInt32();
            amx.pubvars = reader.ReadInt32();
            amx.tags = reader.ReadInt32();
            amx.nametable = reader.ReadInt32();

            if (amx.magic != AMX_MAGIC)
                throw new Exception("unrecognized amx header");

            if (amx.file_version < MIN_FILE_VERSION ||
                amx.file_version > CUR_FILE_VERSION)
            {
                throw new Exception("unrecognized amx version");
            }

            if (amx.defsize != DEFSIZE)
                throw new Exception("unrecognized header defsize");

            DAT_ = new byte[amx.hea - amx.dat];
            for (int i = 0; i < DAT_.Length; i++)
                DAT_[i] = binary[amx.dat + i];

            // Public functions
            if (amx.publics > 0)
            {
                int count = (amx.natives - amx.publics) / DEFSIZE;
                publics_ = new Public[count];
                BinaryReader r = new BinaryReader(new MemoryStream(binary, amx.publics, count * DEFSIZE));
                for (int i = 0; i < publics_.Length; i++)
                {
                    uint address = r.ReadUInt32();
                    int nameoffset = r.ReadInt32();
                    string name = ReadName(binary, nameoffset);
                    publics_[i] = new Public(name, address);
                }
            }

            // Unpack compact
            if ((amx.flags & AMX_FLAG_COMPACT) == AMX_FLAG_COMPACT)
            {
                MemoryStream ms = AMXModXFile.Decompress(amx, new MemoryStream(binary, 0, binary.Length));
                binary = ms.ToArray();
            }

            // .CODE
            {
                byte[] codeBytes = Slice(binary, amx.cod, amx.dat - amx.cod);
                code_ = new Code(codeBytes, 0, 0);
            }

            // .DATA
            {
                //BinaryReader br = new BinaryReader(new MemoryStream(binary, amx.dat, amx.size - amx.dat));
                byte[] dataBytes = null;
                int dataLength = amx.size - amx.dat;

                if ((amx.flags & AMX_FLAG_COMPACT) == AMX_FLAG_COMPACT)
                {
                    dataLength = binary.Length - amx.dat;
                    dataBytes = Slice(binary, amx.dat, dataLength);
                }
                else
                {
                    dataBytes = Slice(binary, amx.dat, dataLength);
                }

                data_ = new Data(dataBytes, dataLength);
            }
            // .NATIVES
            {
                if (amx.natives > 0)
                {
                    int count = (amx.libraries - amx.natives) / DEFSIZE;
                    natives_ = new Native[count];
                    BinaryReader nr = new BinaryReader(new MemoryStream(binary, amx.natives, count * DEFSIZE));
                    for (int i = 0; i < count; i++)
                    {
                        uint address = nr.ReadUInt32();
                        int nameoffset = nr.ReadInt32();
                        string name = ReadName(binary, nameoffset);
                        natives_[i] = new Native(name, i);
                    }
                }
            }
            // .PUBVARS
            {
                if (amx.pubvars > 0)
                {
                    int count = (amx.tags - amx.pubvars) / DEFSIZE;
                    pubvars_ = new PubVar[count];
                    BinaryReader nr = new BinaryReader(new MemoryStream(binary, amx.pubvars, count * DEFSIZE));
                    for (int i = 0; i < count; i++)
                    {
                        uint address = nr.ReadUInt32();
                        int nameoffset = nr.ReadInt32();
                        string name = ReadName(binary, nameoffset);
                        pubvars_[i] = new PubVar(name, address);
                    }
                }
            }

            // Tags
            tags_ = new List<Tag>();
            tags_.Add(new Tag("String", Q_USER_TAG_STRING));

            #region amxdebug
            if (amx.file_version >= MIN_DEBUG_FILE_VERSION &&
                (amx.flags & AMX_FLAG_DEBUG) == AMX_FLAG_DEBUG)
            {
                int debugOffset = amx.size;
                BinaryReader r = new BinaryReader(new MemoryStream(binary, debugOffset, AMX_DEBUG_HDR.SIZE));
                AMX_DEBUG_HDR dbg = new AMX_DEBUG_HDR();
                dbg.size = r.ReadInt32();
                dbg.magic = r.ReadUInt16();
                dbg.file_version = r.ReadByte();
                dbg.amx_version = r.ReadByte();
                dbg.flags = r.ReadInt16();
                dbg.files = r.ReadInt16();
                dbg.lines = r.ReadInt16();
                dbg.symbols = r.ReadInt16();
                dbg.tags = r.ReadInt16();
                dbg.automatons = r.ReadInt16();
                dbg.states = r.ReadInt16();

                if (dbg.magic != AMX_DBG_MAGIC)
                    throw new Exception("unrecognized debug magic");

                r = new BinaryReader(new MemoryStream(binary, debugOffset + AMX_DEBUG_HDR.SIZE,
                                                      dbg.size - AMX_DEBUG_HDR.SIZE));

                // Init SourcePawnDebug
                debugHeader_.numFiles = dbg.files;
                debugHeader_.numLines = dbg.lines;
                debugHeader_.numSyms = dbg.symbols;

                // Read files.
                debugFiles_ = new DebugFile[debugHeader_.numFiles];
                for (short i = 0; i < dbg.files; i++)
                {
                    uint offset = r.ReadUInt32();
                    string name = ReadName(r);

                    debugFiles_[i] = new DebugFile(name, offset);
                }

                // Read lines.
                debugLines_ = new DebugLine[debugHeader_.numLines];
                for (short i = 0; i < dbg.lines; i++)
                {
                    uint offset = r.ReadUInt32();
                    int lineno = r.ReadInt32();

                    debugLines_[i] = new DebugLine((int)lineno, offset);
                }

                List<Function> functions = new List<Function>();
                List<Variable> globals = new List<Variable>();
                List<Variable> locals = new List<Variable>();
                List<Variable> allvars = new List<Variable>();

                bool printed_ignore_xs_vars_msg = false;

                // Read symbols.
                for (short i = 0; i < dbg.symbols; i++)
                {
                    int addr = r.ReadInt32();
                    ushort tagid = r.ReadUInt16();
                    uint codestart = r.ReadUInt32();
                    uint codeend = r.ReadUInt32();
                    byte ident = r.ReadByte();
                    Scope vclass = (Scope)r.ReadByte();
                    ushort dimcount = r.ReadUInt16();
                    string name = ReadName(r);

                    if (ident == IDENT_FUNCTION)
                    {
                        Function func = new Function((uint)addr, codestart, codeend, name, tagid);
                        functions.Add(func);
                    }
                    else
                    {
                        VariableType type = FromIdent(ident);
                        Dimension[] dims = null;
                        if (dimcount > 0)
                        {
                            dims = new Dimension[dimcount];
                            for (ushort j = 0; j < dimcount; j++)
                            {
                                short tag_id = r.ReadInt16();
                                int size = r.ReadInt32();
                                dims[j] = new Dimension(tag_id, null, size);
                            }
                        }

                        // Ignore "xs__" vars
                        if (name.StartsWith("xs__"))
                        {
                            if (!printed_ignore_xs_vars_msg)
                            {
                                printed_ignore_xs_vars_msg = true;
                                System.Console.WriteLine("// Ignored xs__ variables");
                            }

                            continue;
                        }

                        Variable var = new Variable(addr, tagid, null, codestart, codeend, type, vclass, name, dims);
                        if (vclass == Scope.Global)
                            globals.Add(var);
                        else
                            locals.Add(var);
                    }
                }

                globals.Sort((var1, var2) => var1.address - var2.address);
                functions.Sort((fun1, fun2) => (int) (fun1.address - fun2.address));

                allvars.AddRange(locals);
                allvars.AddRange(globals);

                allvars.Sort((var1, var2) => var1.address - var2.address);

                variables_ = locals.ToArray();
                globals_ = globals.ToArray();
                functions_ = functions.ToArray();
                allvars_ = allvars.ToArray();

                // Find tags. + String tag
                for (short i = 0; i < dbg.tags; i++)
                {
                    uint tagId = r.ReadUInt16();
                    string name = ReadName(r);
                    tags_.Add(new Tag(name, tagId));
                }

                // Update symbols.
                for (int i = 0; i < functions_.Length; i++)
                    functions_[i].setTag(FindTag(functions_[i].tag_id));
                for (int i = 0; i < variables_.Length; i++)
                    variables_[i].setTag(FindTag(variables_[i].tag_id, variables_[i]));
                for (int i = 0; i < globals_.Length; i++)
                    globals_[i].setTag(FindTag(globals_[i].tag_id, globals_[i]));

                // For every function, attempt to build argument information.
                for (int i = 0; i < functions_.Length; i++)
                {
                    Function fun = functions_[i];
                    int argOffset = 12;
                    var args = new List<Argument>();
                    do
                    {
                        Variable var = LookupVariable(fun.address, argOffset);
                        if (var == null)
                            break;

                        // Set to functions arguments String tag.
                        if (var.type == VariableType.ArrayReference && var.dims != null && var.dims.Length == 1 && var.dims[0].size == 0 && var.tag != null && var.tag.name == "_")
                        {
                            var.setTag(FindOrCreateTag("String"));
                            var.tag_id = Q_USER_TAG_STRING;
                        }

                        Argument arg = new Argument(var.type, var.name, (int)var.tag.tag_id, var.tag, var.dims);
                        args.Add(arg);
                        argOffset += 4;
                    } while (true);
                    fun.setArguments(args);
                }
            }
            #endregion

            try
            {
                parser = new DebugFileParser(new string[] { "amxx_include", "amxx_user" }, tags_);

                //tags_ = parser.getTags();
                parser.writeNativeDebug(natives_);
            }
            catch
            {
                // ignored
            }
        }

        protected static string ReadName(BinaryReader r)
        {
            List<byte> buffer = new List<byte>();
            do
            {
                byte b = r.ReadByte();
                if (b == 0)
                    break;
                buffer.Add(b);
            } while (true);
            return System.Text.Encoding.UTF8.GetString(buffer.ToArray());
        }

        protected static string ReadName(byte[] bytes, int offset)
        {
            int count = offset;
            for (; count < bytes.Length; count++)
            {
                if (bytes[count] == 0)
                    break;
            }
            return System.Text.Encoding.UTF8.GetString(bytes, offset, count - offset);
        }

        protected static string ReadString(byte[] bytes, int offset)
        {
            List<byte> buffer = new List<byte>();
            int count = offset;
            for (; count < bytes.Length; count += 4)
            {
                if (bytes[count] == 0)
                    break;
                int cell = BitConverter.ToInt32(bytes, count);

                if (cell >= 0 && cell <= 31 && cell != 13 && cell != 10) // Replace 1 to '\1', and ignore '\n', '\r'
                {
                    buffer.Add(Convert.ToByte('\\'));

                    char[] chars = cell.ToString().ToCharArray();

                    for (int i = 0; i < chars.Length; i++)
                        buffer.Add(Convert.ToByte(chars[i]));
                }
                else
                {
                    buffer.Add((byte)cell);
                }
            }

            string ret = System.Text.Encoding.UTF8.GetString(buffer.ToArray(), 0, buffer.Count);

            return ret;
        }

        protected static VariableType FromIdent(byte ident)
        {
            switch (ident)
            {
                case IDENT_VARIABLE:
                    return VariableType.Normal;
                case IDENT_REFERENCE:
                    return VariableType.Reference;
                case IDENT_ARRAY:
                    return VariableType.Array;
                case IDENT_REFARRAY:
                    return VariableType.ArrayReference;
                case IDENT_VARARGS:
                    return VariableType.Variadic;
                default:
                    return VariableType.Normal;
            }
        }

        protected Tag FindTag(uint tagId, Variable var)
        {
            Tag tag = FindTag(tagId);

            if (tag.name != "_")
                return tag;

            Tag maybe = FindTagString(var);

            if (maybe != null)
                return maybe;

            return tag;
        }

        protected Tag FindTagString(Variable var)
        {
            // Check to string
            if (var.scope == Scope.Local)
                return null;

            if (var.dims == null)
                return null;

            if (var.dims.Length == 1 && (var.type == VariableType.ArrayReference || var.type == VariableType.Array || var.type == VariableType.Reference))
            {
                int count = var.address;
                int size = 0;

                // Get size
                for (int i = 0; i < allvars_.Length; i++)
                {
                    Variable vart = allvars_[i];

                    if (vart == var)
                    {
                        if (i < allvars_.Length - 1)
                        {
                            size = allvars_[i + 1].address - count;
                        }

                        break;
                    }
                }

                if (size <= 0)  // last string not detected
                    return null;

                byte[] bytes = DAT;
                int end = count + size - 1;

                byte lastchb = bytes[end];
                if (lastchb != 0)   // Chech to zero-terminated array
                    return null;

                bool isstring = true;

                for (; count < bytes.Length && count < end; count += 4)
                {
                    if (bytes[count] == 0)
                        break;

                    string cht = string.Empty;

                    try
                    {
                        int cell = BitConverter.ToInt32(bytes, count);
                        cht = char.ConvertFromUtf32(cell);
                    }
                    catch
                    {
                        isstring = false;
                        break;
                    }

                    char ch = cht.Length > 0 ? cht[0] : '\0';

                    if (ch == '\0')
                    {
                        isstring = false;
                        break;
                    }

                    if (!(char.IsLetterOrDigit(ch) || char.IsPunctuation(ch) || char.IsSeparator(ch) || char.IsWhiteSpace(ch) || char.IsSymbol(ch)))
                    {
                        isstring = false;
                        break;
                    }
                }

                // if array have zero byte not in the end of string
                if (count != end)
                    return null;

                if (isstring)
                {
                    for (int i = 0; i < tags_.Count; i++)
                    {
                        if (tags_[i].tag_id == Q_USER_TAG_STRING)
                            return tags_[i];
                    }
                }
            }
            else if (var.dims.Length > 1 && var.type == VariableType.Array)
            {
                if (IsStringArray(var, var.address, 0))
                {
                    for (int i = 0; i < tags_.Count; i++)
                    {
                        if (tags_[i].tag_id == Q_USER_TAG_STRING)
                            return tags_[i];
                    }
                }
            }

            return null;
        }

        public override bool IsStartOfString(int offset, bool inarray = false, bool allowEmptyStrings = true)
        {
            if (offset <= 0 || offset >= DAT.Length)   // bug: first string not empliment
                return false;

            int count = offset;
            byte[] bytes = DAT;

            bool isstring = true;

            int cell;
            string cht;
            char ch;

            for (; count < bytes.Length; count += 4)
            {
                if (bytes[count] == 0)
                    break;

                try
                {
                    cell = BitConverter.ToInt32(bytes, count);
                    cht = char.ConvertFromUtf32(cell);
                    ch = cht.Length > 0 ? cht[0] : '\0';
                }
                catch
                {
                    return false;
                }

                if (ch == '\0')
                {
                    isstring = false;
                    break;
                }

                if (!(char.IsLetterOrDigit(ch) || char.IsPunctuation(ch) || char.IsSeparator(ch) || char.IsWhiteSpace(ch) || char.IsSymbol(ch)))
                {
                    isstring = false;
                    break;
                }
            }

            if (!isstring)
                return false;

            if (count == offset && !allowEmptyStrings) // if string == ""
            {
                return false;
            }

            // if char in origin before string is 0 or not symbol
            try
            {
                cell = BitConverter.ToInt32(bytes, offset - 4);
                cht = char.ConvertFromUtf32(cell);
                ch = cht.Length > 0 ? cht[0] : '\0';
            }
            catch
            {
                return true;            // ?!?!??! may be bug
            }

            if (!inarray && (char.IsLetterOrDigit(ch) || char.IsPunctuation(ch) || char.IsSeparator(ch) || char.IsWhiteSpace(ch) || char.IsSymbol(ch)))
                return false;

            return true;
        }

        protected bool IsStrigArray(int address, int size)
        {
            for (int i = 0; i < size; i++)
            {
                int abase = address + i * 4;
                int inner = Int32FromData(abase);
                int final = abase + inner;

                if (!IsStartOfString(final, true))
                    return false;
            }

            return true;
        }

        protected bool IsStringArray(Variable var, int address, int level)
        {
            if (level == var.dims.Length - 2)
            {
                if (!IsStrigArray(address, var.dims[level].size))
                    return false;

                return true;
            }

            for (int i = 0; i < var.dims[i].size; i++)
            {
                int abase = address + i * 4;
                int inner = Int32FromData(abase);
                int final = abase + inner;

                if (!IsStringArray(var, final, level + 1))
                    return false;
            }

            return true;
        }

        public override string StringFromData(int address)
        {
            return ReadString(DAT, address);
        }

        public override float FloatFromData(int address)
        {
            return BitConverter.ToSingle(DAT, address);
        }

        public override int Int32FromData(int address)
        {
            return BitConverter.ToInt32(DAT, address);
        }

        public override byte[] DAT
        {
            get { return DAT_; }
        }

        // Helpers
        private static MemoryStream Decompress(AMX_HEADER header, Stream from)
        {
            BinaryWriter output;
            BinaryReader input;
            MemoryStream mem;
            System.Collections.Stack stack;
            byte[] code;

            input = new BinaryReader(from);
            mem = new MemoryStream((int)input.BaseStream.Length);
            output = new BinaryWriter(mem);

            output.Write(input.ReadBytes(header.cod));
            input.BaseStream.Seek(header.cod, SeekOrigin.Begin);

            code = new byte[header.hea - header.cod];
            input.BaseStream.Seek((long)header.cod, SeekOrigin.Begin); // seek to the beginning of the code section
            code = input.ReadBytes(code.Length);
            input.Close();

            stack = new System.Collections.Stack(code.Length);

            uint c;      // 32 bit unsigned
            short shift; // 16 bit signed
            uint codesize = (uint)code.Length;

            while (codesize > 0)
            {
                c = 0;
                shift = 0;

                do
                {
                    codesize--;
                    c = c | (uint)(code[codesize] & 0x7f) << shift;
                    shift += 7;
                } while ((codesize > 0) && ((code[codesize - 1] & 0x80) != 0));

                if ((code[codesize] & 0x40) != 0)
                {
                    while (shift < (8 * 4))  // hard coded 4 byte long
                    {
                        c = c | (uint)0xff << shift;
                        shift += 8;
                    }
                }
                stack.Push(c);
            }
            for (int x = 0, counter = stack.Count; x < counter; x++) output.Write((uint)stack.Pop());
            output.Flush();
            output = null;

            return mem;
        }
    }
}

