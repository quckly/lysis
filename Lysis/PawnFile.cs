using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Lysis
{
    public class Public
    {
        private uint address_;
        private string name_;

        public Public(string name, uint address)
        {
            name_ = name;
            address_ = address;
        }

        public string name
        {
            get { return name_; }
        }
        public uint address
        {
            get { return address_; }
        }
    }

	public abstract class PawnFile
	{
        public static PawnFile FromFile(string path)
        {
            FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            List<byte> bytes = new List<byte>();
            int b;
            while ((b = fs.ReadByte()) >= 0)
                bytes.Add((byte)b);
            byte[] vec = bytes.ToArray();
            uint magic = BitConverter.ToUInt32(vec, 0);

            if (magic == SourcePawn.SourcePawnFile.MAGIC)
            {
                return new SourcePawn.SourcePawnFile(vec);
            }

            if (magic == AMXModX.AMXModXFile.MAGIC2)
            {
                return AMXModX.AMXModXFile.GetPawnFile(vec);
            }

            throw new Exception("not a .amxx or .smx file!");
        }

        protected Function[] functions_;
        protected Public[] publics_;
        protected Variable[] globals_;
        protected Variable[] variables_;
        protected List<Tag> tags_;

        protected byte[] DAT_;
        protected Code code_;
        protected Data data_;

        protected PubVar[] pubvars_;
        protected Native[] natives_;

        protected List<DebugFileParser.DefinedEnum> enums;

        // Not necessarily
        protected DebugFile[] debugFiles_;
        protected DebugLine[] debugLines_;
        protected DebugHeader debugHeader_;

        protected Tag FindTag(uint tagId)
        {
            for (int i = 0; i < tags_.Count; i++)
            {
                if (tags_[i].tag_id == tagId)
                    return tags_[i];
            }
            return null;
        }

        protected Tag FindTag(string tagName)
        {
            for (int i = 0; i < tags_.Count; i++)
            {
                if (tags_[i].name == tagName)
                    return tags_[i];
            }
            return null;
        }

        protected Tag FindOrCreateTag(string tagName)
        {
            for (int i = 0; i < tags_.Count; i++)
            {
                if (tags_[i].name == tagName)
                    return tags_[i];
            }

            Tag newtag = new Tag(tagName, tags_[tags_.Count - 1].tag_id + 1);
            tags_.Add(newtag);

            return newtag;
        }

        // Abstract
        public abstract string StringFromData(int address);
        public abstract float FloatFromData(int address);
        public abstract int Int32FromData(int address);

        public abstract byte[] DAT
        {
            get;
        }

        public abstract bool IsStartOfString(int offset, bool inarray = false, bool allowEmptyStrings = true);

        // Virtual
        public virtual string LookupFile(uint address)
        {
            if (debugFiles_ == null)
                return null;

            int high = debugFiles_.Length;
            int low = -1;

            while (high - low > 1)
            {
                int mid = (low + high) >> 1;
                if (debugFiles_[mid].address <= address)
                    low = mid;
                else
                    high = mid;
            }
            if (low == -1)
                return null;
            return debugFiles_[low].name;
        }

        public virtual int LookupLine(uint address)
        {
            if (debugLines_ == null)
                return -1;

            int high = debugLines_.Length;
            int low = -1;

            while (high - low > 1)
            {
                int mid = (low + high) >> 1;
                if (debugLines_[mid].address <= address)
                    low = mid;
                else
                    high = mid;
            }
            if (low == -1)
                return -1;
            return debugLines_[low].line;
        }

        public virtual Variable LookupDeclarations(uint pc, ref int i, Scope scope = Scope.Local)
        {
            for (i++; i < variables_.Length; i++)
            {
                Variable var = variables_[i];
                if (pc != var.codeStart)
                    continue;
                if (var.scope == scope)
                    return var;
            }
            return null;
        }

        public virtual Variable LookupVariable(uint pc, int offset, Scope scope = Scope.Local)
        {
            for (int i = 0; i < variables_.Length; i++)
            {
                Variable var = variables_[i];
                if ((pc >= var.codeStart && pc < var.codeEnd) &&
                    (offset == var.address && var.scope == scope))
                {
                    return var;
                }
            }
            return null;
        }

        public virtual Variable LookupGlobal(int address)
        {
            for (int i = 0; i < globals_.Length; i++)
            {
                Variable var = globals_[i];
                if (var.address == address)
                    return var;
            }
            return null;
        }

        public virtual Function LookupFunction(uint pc)
        {
            for (int i = 0; i < functions_.Length; i++)
            {
                Function f = functions_[i];
                if (pc >= f.codeStart && pc < f.codeEnd)
                    return f;
            }
            return null;
        }
        public virtual Public LookupPublic(string name)
        {
            for (int i = 0; i < publics_.Length; i++)
            {
                if (publics_[i].name == name)
                    return publics_[i];
            }
            return null;
        }

        public virtual Public LookupPublic(uint addr)
        {
            for (int i = 0; i < publics_.Length; i++)
            {
                if (publics_[i].address == addr)
                    return publics_[i];
            }
            return null;
        }

        public virtual Function[] Functions
        {
            get { return functions_; }
        }
        public virtual Public[] Publics
        {
            get { return publics_; }
        }
        public virtual Variable[] Globals
        {
            get { return globals_; }
        }

        public virtual Code SecCode
        {
            get { return code_; }
        }

        public virtual Data SecData
        {
            get { return data_; }
        }

        public virtual PubVar[] Pubvars
        {
            get { return pubvars_; }
        }
        public virtual Native[] Natives
        {
            get { return natives_; }
        }

        // Declare classes
        public struct DebugHeader
        {
            public int numFiles;
            public int numLines;
            public int numSyms;
        }

        public struct Section
        {
            public int dataoffs;
            public int size;

            public Section(int dataoffs, int size)
            {
                this.dataoffs = dataoffs;
                this.size = size;
            }
        }

        public class Code
        {
            private byte[] code_;
            private int flags_;
            private int version_;

            public Code(byte[] code, int flags, int version)
            {
                code_ = code;
                flags_ = flags;
                version_ = version;
            }

            public byte[] bytes
            {
                get { return code_; }
            }
            public int flags
            {
                get { return flags_; }
            }
            public int version
            {
                get { return version_; }
            }
        }

        public class Data
        {
            private byte[] data_;
            private int memory_;

            public Data(byte[] data, int memory)
            {
                data_ = data;
                memory_ = memory;
            }

            public byte[] bytes
            {
                get { return data_; }
            }
            public int memory
            {
                get { return memory_; }
            }
        }

        public class PubVar
        {
            private uint address_;
            private string name_;

            public PubVar(string name, uint address)
            {
                name_ = name;
                address_ = address;
            }

            public string name
            {
                get { return name_; }
            }
            public uint address
            {
                get { return address_; }
            }
        }

        public class DebugFile
        {
            private uint address_;
            private string name_;

            public DebugFile(string name, uint address)
            {
                name_ = name;
                address_ = address;
            }

            public string name
            {
                get { return name_; }
            }
            public uint address
            {
                get { return address_; }
            }
        }

        public class DebugLine
        {
            private uint address_;
            private int line_;

            public DebugLine(int line, uint address)
            {
                line_ = line;
                address_ = address;
            }

            public int line
            {
                get { return line_; }
            }
            public uint address
            {
                get { return address_; }
            }
        }

        protected static byte[] Slice(byte[] bytes, int offset, int length)
        {
            byte[] shadow = new byte[length];
            for (int i = 0; i < length; i++)
                shadow[i] = bytes[offset + i];
            return shadow;
        }
	}
}

