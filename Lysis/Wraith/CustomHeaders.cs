﻿using System;
using System.Collections;

using Amx.Core;
using Amx.Logic.Debug;

namespace Files
{
    using System.IO;

    public class AmxHeader_4_win : AmxHeader
    {
        public AmxHeader_4_win(AmxStream reader, AmxHeader header)
            : base(header)
        {
            publics_location = (int)reader.Position;
            publics_count = (int)reader.ReadInt16();
            publics_ptr = reader.ReadInt32();

            natives_location = (int)reader.Position;
            natives_count = (int)reader.ReadInt16();
            natives_ptr = reader.ReadInt32();

            libraries_location = (int)reader.Position;
            libraries_count = (int)reader.ReadInt16();
            libraries_ptr = reader.ReadInt32();

            pubvars_location = (int)reader.Position;
            pubvars_count = (int)reader.ReadInt16();
            pubvars_ptr = reader.ReadInt32();

            tags_location = -1;
            names_location = -1;
        }

    }


    public class AmxHeader_5_win : AmxHeader
    {
        private int reserved;

        public AmxHeader_5_win(AmxStream reader, AmxHeader header)
            : base(header)
        {
            publics_location = (int)reader.Position;
            publics_count = (int)reader.ReadInt16();
            publics_ptr = reader.ReadInt32();

            natives_location = (int)reader.Position;
            natives_count = (int)reader.ReadInt16();
            natives_ptr = reader.ReadInt32();

            libraries_location = (int)reader.Position;
            libraries_count = (int)reader.ReadInt16();
            libraries_ptr = reader.ReadInt32();

            pubvars_location = (int)reader.Position;
            pubvars_count = (int)reader.ReadInt16();
            pubvars_ptr = reader.ReadInt32();

            tags_location = (int)reader.Position;
            tags_count = (int)reader.ReadInt16();
            tags_ptr = reader.ReadInt32();

            reserved = (int)reader.ReadInt16();

            names_location = -1;
            names_ptr = -1;
        }


        public int Reserved
        {
            get
            {
                return reserved;
            }
        }

    }


    public class AmxHeader_4_lin : AmxHeader
    {
        public AmxHeader_4_lin(AmxStream reader, AmxHeader header)
            : base(header)
        {
            publics_location = (int)reader.Position;
            publics_count = reader.ReadInt32();
            publics_ptr = reader.ReadInt32();

            natives_location = (int)reader.Position;
            natives_count = reader.ReadInt32();
            natives_ptr = reader.ReadInt32();

            libraries_location = (int)reader.Position;
            libraries_count = reader.ReadInt32();
            libraries_ptr = reader.ReadInt32();

            pubvars_location = (int)reader.Position;
            pubvars_count = reader.ReadInt32();
            pubvars_ptr = reader.ReadInt32();

            names_location = -1;
            tags_location = -1;
        }

    }


    public class AmxHeader_5_lin : AmxHeader
    {
        private int reserved;

        public AmxHeader_5_lin(AmxStream reader, AmxHeader header)
            : base(header)
        {
            publics_location = (int)reader.Position;
            publics_count = reader.ReadInt32();
            publics_ptr = reader.ReadInt32();

            natives_location = (int)reader.Position;
            natives_count = reader.ReadInt32();
            natives_ptr = reader.ReadInt32();

            libraries_location = (int)reader.Position;
            libraries_count = reader.ReadInt32();
            libraries_ptr = reader.ReadInt32();

            pubvars_location = (int)reader.Position;
            pubvars_count = reader.ReadInt32();
            pubvars_ptr = reader.ReadInt32();

            tags_location = (int)reader.Position;
            tags_count = reader.ReadInt32();
            tags_ptr = reader.ReadInt32();

            reserved = reader.ReadInt32();

            names_location = -1;
            names_ptr = -1;
        }


        public int Reserved
        {
            get
            {
                return reserved;
            }
        }

    }


    public class AmxHeader_6 : AmxHeader
    {

        public AmxHeader_6(AmxStream reader, AmxHeader header)
            : base(header)
        {
            publics_ptr = reader.ReadInt32();
            natives_ptr = reader.ReadInt32();
            libraries_ptr = reader.ReadInt32();
            pubvars_ptr = reader.ReadInt32();
            tags_ptr = reader.ReadInt32();

            publics_count = (int)((natives_ptr - publics_ptr) / defsize);
            natives_count = (int)((libraries_ptr - natives_ptr) / defsize);
            libraries_count = (int)((pubvars_ptr - libraries_ptr) / defsize);
            pubvars_count = (int)((tags_ptr - pubvars_ptr) / defsize);
            tags_count = (int)((tags_ptr - this.cod) / defsize);

            names_location = -1;
            names_ptr = -1;
        }
    }


    public class AmxHeader_7 : AmxHeader
    {
        public AmxHeader_7(AmxStream reader, AmxHeader header)
            : base(header)
        {
            publics_ptr = reader.ReadInt32();
            natives_ptr = reader.ReadInt32();
            libraries_ptr = reader.ReadInt32();
            pubvars_ptr = reader.ReadInt32();
            tags_ptr = reader.ReadInt32();
            names_ptr = reader.ReadInt32();

            publics_count = (int)((natives_ptr - publics_ptr) / defsize);
            natives_count = (int)((libraries_ptr - natives_ptr) / defsize);
            libraries_count = (int)((pubvars_ptr - libraries_ptr) / defsize);
            pubvars_count = (int)((tags_ptr - pubvars_ptr) / defsize);
            tags_count = (int)((names_ptr - tags_ptr) / defsize);
        }


        public sealed override void LoadTables(AmxStream reader)
        {
            publics = LoadTable(reader, publics_ptr, publics_count);
            if (!ValidateNatives(publics, false))
            {
                this.bithint = IncreaseBitHint(this.bithint);
                publics = LoadTable(reader, publics_ptr, publics_count);
            }

            natives = LoadTable(reader, natives_ptr, natives_count);
            libraries = LoadTable(reader, libraries_ptr, libraries_count);
            pubvars = LoadTable(reader, pubvars_ptr, pubvars_count);
            tags = LoadTable(reader, tags_ptr, tags_count);
        }


        private AmxNative[] LoadTable(AmxStream reader, long baseaddr, int number)
        {
            AmxNative[] retval = null;
            if (number > 0)
            {
                long pos = reader.Position;
                reader.Seek(baseaddr, SeekOrigin.Begin);
                retval = new AmxNativeVariable[number];
                for (int i = 0; i < number; i++)
                {
                    retval[i] = new AmxNativeVariable(reader, this.bithint);
                }
                reader.Seek(pos, SeekOrigin.Begin);
            }
            else if (number == 0)
            {
                retval = new AmxNativeVariable[0];
            }
            else
            {
                throw new Exception("negative count for a header table is not permissable");
            }
            return retval;
        }

    }


    public class AmxHeader_8 : AmxHeader_7
    {
        public AmxHeader_8(AmxStream reader, AmxHeader header)
            : base(reader, header)
        {

        }
    }

    public class AmxHeader_10 : AmxHeader_7
    {
        //size          4 bytes size of the memory image, excluding the stack/heap
        //magic         2 bytes indicates the format and cell size
        //file version  1 byte file format version, currently 8
        //amx version   1 byte required minimal version of the abstract machine
        //flags         2 bytes flags, see below
        //defsize       2 bytes size of a structure in the “native functions” and the “public functions” tables
        //cod           4 bytes offset to the start of the code section
        //dat           4 bytes offset to the start of the data section
        //hea           4 bytes initial value of the heap, end of the data section
        //stp           4 bytes stack top value (the total memory requirements)
        //cip           4 bytes starting address (main() function), -1 if none
        //publics       4 bytes offset to the “public functions” table
        //natives       4 bytes offset to the “native functions” table
        //libraries     4 bytes offset to the table of libraries
        //pubvars       4 bytes offset to the “public variables” table
        //tags          4 bytes offset to the “public tags” table
        //nametable     4 bytes offset to the symbol name table (file version 7+)

        //overlays      4 bytes offset to the overlay table (file version 10+)

        //publics table variable public functions table (see below)
        //natives table variable native functions table (see below)
        //library table variable library table (see below)
        //pubvars table variable public variables table (see below)
        //tags table variable public tags table (see below)
        //overlay table variable the overlay table (file version 10+; see below)
        //name table variable the symbol name table (file version 7+; see below)

        public AmxHeader_10(AmxStream reader, AmxHeader header)
            : base(reader, header)
        {

        }

        public override bool Valid
        {
            //F1E0 for a 32-bit cell;
            //F1E1 for a 64-bit cell;
            //F1E2 for a 16-bit cell.
            get
            {
                return (magic == 61920 || magic == 61921 || magic == 61922);
            }
        }
    }

    public class EmptyAmxHeader : AmxHeader
    {
        public EmptyAmxHeader()
        {
        }

        public int WriteLength
        {
            set { size = value; }
        }

        public int WriteMagic
        {
            set { magic = value; }
        }

        public int WriteFileVersion
        {
            set { file_version = value; }
        }

        public int WriteAmxVersion
        {
            set { amx_version = value; }
        }

        public AMX_FLAGS WriteFlags
        {
            set { flags = value; }
        }

        //public int WriteDefsize
        //{
        //    set { value = defsize; }
        //}

        public Bits WriteBits
        {
            set { bithint = value; }
        }

        public int WriteCodOffset
        {
            set { cod = value; }
        }
        public int WriteDatOffset
        {
            set { dat = value; }
        }
        public int WriteCodSize
        {
            set { cod_size = value; }
        }
        public int WriteDatSize
        {
            set { dat_size = value; }
        }
        public int WriteHeaOffset
        {
            set { hea = value; }
        }
        public int WriteStpOffset
        {
            set { stp = value; }
        }
        public int WriteCipOffset
        {
            set { cip = value; }
        }

        public AmxNative[] FillNatives
        {
            set { natives = value; }
        }
        public AmxNative[] FillPublicVars
        {
            set { pubvars = value; }
        }

        public AmxNative[] FillTags
        {
            set { tags = value; }
        }

        public AmxNative[] FillPublics
        {
            set { publics = value; }
        }

        public AmxNative[] FillLibraries
        {
            set { libraries = value; }
        }

        //public int WriteNativesPtr
        //{
        //    set { value = natives_ptr; }
        //}
        //public int WriteNamesPtr
        //{
        //    set { value = names_ptr; }
        //}
        //public int WritePubVarsPtr
        //{
        //    set { value = pubvars_ptr; }
        //}
        //public int WriteTagsPtr
        //{
        //    set { value = tags_ptr; }
        //}
        //public int WriteLibrariesPtr
        //{
        //    set { value = libraries_ptr; }
        //}
        //public int WritePublicsPtr
        //{
        //    set { value = publics_ptr; }
        //}

        //public int WritePublicsCount
        //{
        //    set { value = publics_count; }
        //}
        //public int WriteNativesCount
        //{
        //    set { value = natives_count; }
        //}
        //public int WritePubVarsCount
        //{
        //    set { value = pubvars_count; }
        //}
        //public int WriteTagsCount
        //{
        //    set { value = tags_count; }
        //}
        //public int WriteLibrariesCount
        //{
        //    set { value = libraries_count; }
        //}

        //Not Supported:
        //protected int publics_location;
        //protected int natives_location;
        //protected int libraries_location;
        //protected int pubvars_location;
        //protected int tags_location;
        //protected int names_location;

    }

    public class EmptyAmxDebugHeader : AmxDebugHeader
    {
        public EmptyAmxDebugHeader()
        {
        }

        public int WriteFlags
        {
            set { flags = value; }
        }

        public int WriteNumberOfFiles
        {
            set { files_count = value; }
        }

        public int WriteNumberOfLines
        {
            set { lines_count = value; }
        }

        public int WriteNumberOfSymbols
        {
            set { symbols_count = value; }
        }

        public int WriteNumberOfTags
        {
            set { tags_count = value; }
        }

        public int WriteNumberOfAutomatons
        {
            set { automatons_count = value; }
        }

        public int WriteNumberOfStates
        {
            set { states_count = value; }
        }

        public int WriteFileVersion
        {
            set { file_version = value; }
        }

        public int WriteAmxVersion
        {
            set { amx_version = value; }
        }

        //public int WriteLength
        //{
        //    set { value = size; }
        //}
        public SymbolTable FillFileTable
        {
            set
            {
                files = value;
            }
        }
        public SymbolTable FillLinesTable
        {
            set
            {
                lines = value;
            }
        }
        public SymbolTable FillFunctionSymbolTable
        {
            set
            {
                functionsymbols = value;
            }
        }
        public SymbolTable FillGlobalSymbolTable
        {
            set
            {
                globalsymbols = value;
            }
        }

        public ArrayList FillLocalSymbolTable
        {
            set
            {
                localSymbols = value;
            }
        }

        public ArrayList FillTagTable
        {
            set
            {
                tags = value;
            }
        }

        public ArrayList FillAutomatonsTable
        {
            set
            {
                automatons = value;
            }
        }
        public ArrayList FillStateTable
        {
            set
            {
                states = value;
            }
        }
    }
}
