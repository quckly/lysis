﻿#define newstrings
using System;

namespace Amx.Dat
{
    using System.Collections;
    using System.Text;
    using Amx.Core;
    using Amx.Strings;

    public sealed class DatHelpers
    {
        private DatHelpers() { }

        public static IDatEntry CreateDatEntryWithKnownLength(byte[] data, int sizeof_cell, int start, int length, bool trymulti, bool trystring, bool tryarray, bool tryvariable)
        {   //We need another method to avoid deep recursion in base CreateDatEntry
            IDatEntry retval = null;

            if (trymulti)
            {
                retval = new MultiEntry(data, sizeof_cell, start);

                if (retval.Type != DatEntryType.Bad)
                {
                    if (retval.End + 1 == (start + length))
                    {
                        return retval;
                    }
                }
            }

            if (trystring || tryarray || tryvariable)
            {
                retval = DatHelpers.CreateDatEntry(data, sizeof_cell, start, length, false, trystring, tryarray, tryvariable);
                return retval;
            }

            return new BadEntry();
        }

        public static IDatEntry CreateDatEntry(byte[] data, int sizeof_cell, DatEntryMultiarrayDescription description, bool trymulti, bool trystring, bool tryarray, bool tryvariable)
        {

            IDatEntry retval = null;

            if (trymulti)
            {
                //retval = new MultiEntry(ref data,description);
                retval = MultiEntry.CreateFromDescription(data, sizeof_cell, description);
                if ((retval.Start % sizeof_cell) != 0 || (retval.Length % sizeof_cell) != 0)
                {
                    //retval = retval; //TODO: wtf ?!
                }
                if (retval.Type != DatEntryType.Bad)
                {
                    return retval;
                }
            }

            if (trystring)
            {
                retval = new StringEntry(data, sizeof_cell, description.value, description.bound_value * sizeof_cell);//4);
                if (retval.Type != DatEntryType.Bad)
                {
                    return retval;
                }
            }

            if (tryarray)
            {
                retval = new ArrayEntry(data, sizeof_cell, description.value, description.bound_value * sizeof_cell);
                if (retval.Type != DatEntryType.Bad)
                {
                    if (retval.Length > 1 || !tryvariable)
                    {
                        return retval;
                    }
                    else
                    {
                        retval = new VariableEntry(data, sizeof_cell, description.value);
                    }
                }
            }

            if (tryvariable)
            {
                retval = new VariableEntry(data, sizeof_cell, description.value);
                if (retval.Type != DatEntryType.Bad)
                {
                    return retval;
                }
            }
            return new BadEntry();
        }

        public static IDatEntry CreateDatEntry(byte[] data, int sizeof_cell, int start, int length, bool trymulti, bool trystring, bool tryarray, bool tryvariable)
        {
            IDatEntry retval = null;

            if (trymulti)
            {
                if ((length % sizeof_cell) == 0)
                {   //Catch here creating of multiArrays in MultiArrays (arr[][][]) in recursion
                    retval = new MultiEntry(data, sizeof_cell, start, length / sizeof_cell);
                    if (retval.Type != DatEntryType.Bad)
                    {
                        return retval;
                    }
                }

                retval = new MultiEntry(data, sizeof_cell, start, length);

                if (retval.Type != DatEntryType.Bad)
                {                    
                    return retval;
                }
            }

            if (trystring)
            {
                retval = new StringEntry(data, sizeof_cell, start, length);
                if (retval.Type != DatEntryType.Bad)
                {
                    return retval;
                }
            }

            if (tryarray)
            {
                retval = new ArrayEntry(data, sizeof_cell, start, length);
                if (retval.Type != DatEntryType.Bad)
                {
                    if (retval.Length > 1 || !tryvariable)
                    {
                        return retval;
                    }
                    else
                    {
                        retval = new VariableEntry(data, sizeof_cell, start);
                    }
                }
            }

            if (tryvariable)
            {
                retval = new VariableEntry(data, sizeof_cell, start);
                if (retval.Type != DatEntryType.Bad)
                {
                    return retval;
                }
            }

            return new BadEntry();
        }

        public static IDatEntry CreateDatEntry(byte[] data, int sizeof_cell, int start, bool trymulti, bool trystring, bool tryarray, bool tryvariable)
        {
            IDatEntry retval = null;

            if (trymulti)
            {
                retval = new MultiEntry(data, sizeof_cell, start);
                if (retval.Type != DatEntryType.Bad)
                {
                    if ((retval.Start % sizeof_cell) != 0 || (retval.Length % sizeof_cell) != 0)
                    {
                        //retval = retval; //TODO: wtf ?! is it return null; ??
                    }
                    return retval;
                }
            }

            if (trystring)
            {
                retval = new StringEntry(data, sizeof_cell, start);

                if (retval.Type != DatEntryType.Bad)
                {
                    return retval;
                }
            }

            if (tryarray)
            {
                retval = new ArrayEntry(data, sizeof_cell, start);
                if (retval.Type != DatEntryType.Bad)
                {
                    if (retval.Length > 1 || !tryvariable)
                    {
                        return retval;
                    }
                    else
                    {
                        retval = new VariableEntry(data, sizeof_cell, start);
                    }
                }
            }

            if (tryvariable)
            {
                retval = new VariableEntry(data, sizeof_cell, start);
                if (retval.Type != DatEntryType.Bad)
                {
                    return retval;
                }
            }

            return new BadEntry(); ;
        }

        public static bool ResolveOcclusion(ResourceList entries, byte[] data, int sizeof_cell)
        {

            bool again = true;
            bool changemade = false;

            IDatEntry dat1 = null;
            IDatEntry dat2 = null;

            while (again)
            {
                again = false;
                for (int i = 0; i < entries.Count - 1; i++)
                {
                    dat1 = entries[i];
                    dat2 = entries[i + 1];

                    if (
                        dat1.Contains(dat2)
                        ||
                        dat1.Overlaps(dat2)
                        )
                    {
                        if (dat1.Type >= dat2.Type)
                        {
                            entries.Remove(dat2);
                        }
                        else
                        {
                            entries.Remove(dat1);
                        }
                        again = true;
                        changemade = true;
                        break;
                    }
                }
            }
            return changemade;
        }

        public static bool EqualContent(ArrayList entrie1, ArrayList entrie2)
        {
            if (entrie1.Count == entrie2.Count)
            {
                bool similar = false;
                for (int i = 0; i < entrie1.Count; i++)
                {
                    similar = false;
                    for (int j = 0; j < entrie2.Count; j++)
                    {
                        if (((ValInt)entrie1[i]).Value == ((ValInt)entrie2[j]).Value)
                        {
                            similar = true;
                            break;
                        }
                    }

                    if (!similar)
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;

        }

        public static bool ResolveOcclusionWith(ResourceList entries, byte[] data, int sizeof_cell, ArrayList possibleEntry)
        {//TODO: not tested
            bool changemade = false;
            IDatEntry dat1 = null;
            //bool again = false;
            //while (again)
            {
                //again = false;
                for (int i = 0; i < entries.Count; i++)
                {
                    dat1 = entries[i];
                    for (int j = 0; j < possibleEntry.Count; j++)
                    {
                        if (dat1.Contains((ValInt)possibleEntry[j]))
                        {
                            possibleEntry.Remove(possibleEntry[j]);
                            changemade = true;
                            j--;
                            //again = true;
                        }
                    }
                }
            }

            return changemade;
        }

        public static bool ResolveGaps(ResourceList entries, byte[] data, int sizeof_cell)
        {
            if (entries.Count == 0)
            {
                return false;
            }

            bool changemade = true;

            IDatEntry datx = null;
            IDatEntry daty = null;

            while (changemade)
            {
                changemade = false;

                datx = entries[0];
                if (datx.Start != 0)
                {
                    daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, 0, datx.Start, true, true, true, true);
                    if (daty.Type != DatEntryType.Bad)
                    {
                        entries.Add(daty);
                        changemade = true;
                        continue;
                    }
                }

                for (int index = 0; index < entries.Count - 1; index++)
                {
                    datx = entries[index];
                    daty = entries[index + 1];
                    if (datx.End + 1 < daty.Start)
                    {
                        IDatEntry entry = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, datx.End + 1, (daty.Start - 1) - datx.End
                                , true, true, true, true);
                        if (entry.Type != DatEntryType.Bad)
                        {
                            entries.Add(entry);
                            changemade = true;
                            break;
                        }
                    }

                    datx = null;
                    daty = null;
                }
                if (changemade)
                {
                    continue;
                }

                datx = entries[entries.Count - 1];
                daty = null;
                if (datx.End + 1 < data.Length)
                {
                    //daty = DatHelpers.CreateDatEntry(data, sizeof_cell,
                    //    //datx.Start,
                    //        datx.End + 1,
                    //    //data.Length,
                    //        true, false, true, true //need here false, because of bug with variables assigned to constant value (new var1 = 72; new var2;), and next variable that assigned to zero(default)
                    //    //this may cause other bugs TODO: check it
                    //    );
                    IDatEntry entry = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, datx.End + 1, data.Length
                                , true, true, true, true);
                    if (daty.Type != DatEntryType.Bad)
                    {
                        entries.Add(daty);
                        changemade = true;
                        continue;
                    }
                }

            }
            return changemade;
        }

        public static bool ResolveCellGaps(ResourceList entries, byte[] data, int sizeof_cell)
        {
            if (entries.Count == 0)
            {
                return false;
            }
            bool changemade = false;

            IDatEntry datx = null;
            IDatEntry daty = null;

            datx = entries[0];
            if (datx.Start != 0 && datx.Start == sizeof_cell)
            {
                daty = DatHelpers.CreateDatEntry(data, sizeof_cell, 0, false, false, false, true);

                if (daty != null && daty.Type != DatEntryType.Bad)
                {
                    changemade = true;
                    entries.AddUnique(daty, (ValInt)daty.Start);
                }
            }

            for (int index = 0; index < entries.Count - 1; index++)
            {
                datx = entries[index];
                daty = entries[index + 1];

                if ((daty.Start - datx.End - 1) == sizeof_cell)
                {
                    daty = DatHelpers.CreateDatEntry(data, sizeof_cell, datx.End + 1, false, false, false, true);
                }
                else
                {
                    daty = null;
                }
                if (daty != null && daty.Type != DatEntryType.Bad)
                {
                    changemade = true;
                    entries.AddUnique(daty, (ValInt)daty.Start);
                }
            }

            datx = entries[entries.Count - 1];
            daty = null;
            if (((data.Length - sizeof_cell) - (datx.End + 1)) == 0)
            {
                daty = DatHelpers.CreateDatEntry(data, sizeof_cell, datx.End + 1, false, false, false, true);

                if (daty != null && daty.Type != DatEntryType.Bad)
                {
                    changemade = true;
                    entries.AddUnique(daty, (ValInt)daty.Start);
                }
            }

            return changemade;
        }

        public static bool ResolveArrayGaps(ResourceList entries, byte[] data, int sizeof_cell, ArrayList possibleEntry)
        {
            if (entries.Count == 0)
            {
                return false;
            }
            bool changemade = false;

            IDatEntry datx = null;
            IDatEntry daty = null;

            int count = 0;

            datx = entries[0];
            if (datx.Start != 0)
            {
                for (int i = 0; i < possibleEntry.Count; i++)
                {
                    if (((ValInt)possibleEntry[i]) < datx.Start)
                    {
                        count++;
                    }
                }
                if (count <= 1)
                {
                    daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, 0, datx.Start, true, false, true, false);
                }

                if (daty != null && daty.Type != DatEntryType.Bad)
                {
                    changemade = true;
                    entries.AddUnique(daty, (ValInt)daty.Start);
                }
            }

            for (int index = 0; index < entries.Count - 1; index++)
            {
                count = 0;

                datx = entries[index];
                daty = entries[index + 1];

                if (daty.Start != datx.End + 1)
                {
                    for (int i = 0; i < possibleEntry.Count; i++)
                    {
                        if ((ValInt)possibleEntry[i] < daty.Start && (ValInt)possibleEntry[i] > datx.End)
                        {
                            count++;
                        }
                    }
                    if (count <= 1)
                    {
                        daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, datx.End + 1, (daty.Start - datx.End - 1), true, false, true, false);
                    }
                    else
                    { daty = null; }
                }
                else
                { daty = null; }

                if (daty != null && daty.Type != DatEntryType.Bad)
                {
                    changemade = true;
                    entries.AddUnique(daty, (ValInt)daty.Start);
                }
            }

            datx = entries[entries.Count - 1];
            daty = null;
            if (datx.End + 1 < data.Length)
            {
                for (int i = 0; i < possibleEntry.Count; i++)
                {
                    if (((ValInt)possibleEntry[i]) > datx.End)
                    {
                        count++;
                    }
                }
                if (count <= 1)
                {
                    daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, datx.End + 1, (data.Length - datx.End - 1), true, false, true, false);
                }

                if (daty != null && daty.Type != DatEntryType.Bad)
                {
                    changemade = true;
                    entries.AddUnique(daty, (ValInt)daty.Start);
                }
            }

            return changemade;
        }

        public static bool ResolveGaps(ResourceList entries, byte[] data, int sizeof_cell, ArrayList possibleEntry)
        {
            if (entries.Count == 0)
            {
                return false;
            }

            bool changemade = true;

            IDatEntry datx = null;
            IDatEntry daty = null;

            while (changemade)
            {
                changemade = false;

                ArrayList found = new ArrayList(8);

                int startPos;
                int endPos;

                datx = entries[0];
                if (datx.Start != 0)
                {
                    startPos = 0;
                    endPos = datx.Start;

                    #region SameCode
                    for (int i = 0; i < possibleEntry.Count; i++)
                    {
                        ValInt foundI = new ValInt(((ValInt)possibleEntry[i]).Value);
                        if (foundI >= startPos && foundI < endPos)
                        {
                            found.Add(possibleEntry[i]);
                            //count++;
                        }
                    }

                    if (found.Count <= 1)
                    {
                        found.Sort();
                        if (endPos - startPos == sizeof_cell)
                        {
                            daty = DatHelpers.CreateDatEntry(data, sizeof_cell, startPos, endPos, false, false, false, true); //TODO: NOT TESTED
                        }
                        else
                        {
                            daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, startPos, endPos - startPos, true, true, true, false); //TODO: NOT TESTED
                        }

                        if (daty != null && daty.Type != DatEntryType.Bad)
                        {
                            changemade = true;
                            entries.AddUnique(daty, (ValInt)daty.Start);
                        }
                    }
                    else
                    { //vars + arrays
                        found.Add((ValInt)endPos);
                        found.Sort();
                        for (int i = 1; i < found.Count; i++)
                        {
                            ValInt foundI = new ValInt(((ValInt)found[i]).Value);
                            if ((foundI - startPos) == sizeof_cell)
                            {
                                daty = DatHelpers.CreateDatEntry(data, sizeof_cell, startPos, false, false, false, true); //TODO: NOT TESTED
                                startPos = foundI;
                            }
                            else
                            {
                                daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, startPos, foundI - startPos, true, true, true, false); //TODO: NOT TESTED
                                startPos = foundI;
                            }

                            if (daty != null && daty.Type != DatEntryType.Bad)
                            {
                                changemade = true;
                                entries.AddUnique(daty, (ValInt)daty.Start);
                            }
                        }
                    }
                    #endregion

                    if (changemade)
                    {
                        continue;
                    }
                }

                for (int index = 0; index < entries.Count - 1; index++)
                {
                    found = new ArrayList(8);

                    datx = entries[index];
                    daty = entries[index + 1];

                    if (daty.Start != datx.End + 1)
                    {
                        startPos = datx.End + 1;
                        endPos = daty.Start;
                        daty = null;

                        #region SameCode
                        for (int i = 0; i < possibleEntry.Count; i++)
                        {
                            ValInt foundI = new ValInt(((ValInt)possibleEntry[i]).Value);
                            if (foundI >= startPos && foundI < endPos)
                            {
                                found.Add(possibleEntry[i]);
                            }
                        }

                        if (found.Count <= 1)
                        {
                            found.Sort();
                            if (endPos - startPos == sizeof_cell)
                            {
                                daty = DatHelpers.CreateDatEntry(data, sizeof_cell, startPos, endPos, false, false, false, true); //TODO: NOT TESTED
                            }
                            else
                            {
                                daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, startPos, endPos - startPos, true, true, true, false); //TODO: NOT TESTED
                            }

                            if (daty != null && daty.Type != DatEntryType.Bad)
                            {
                                changemade = true;
                                entries.AddUnique(daty, (ValInt)daty.Start);
                            }
                        }
                        else
                        { //vars + arrays
                            found.Add((ValInt)endPos);
                            found.Sort();
                            for (int i = 1; i < found.Count; i++)
                            {
                                ValInt foundI = new ValInt(((ValInt)found[i]).Value);
                                if ((foundI - startPos) == sizeof_cell)
                                {
                                    daty = DatHelpers.CreateDatEntry(data, sizeof_cell, startPos, false, false, false, true); //TODO: NOT TESTED
                                    startPos = foundI;
                                }
                                else
                                {
                                    daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, startPos, (foundI - startPos), true, true, true, false); //TODO: NOT TESTED
                                    startPos = foundI;
                                }

                                if (daty != null && daty.Type != DatEntryType.Bad)
                                {
                                    changemade = true;
                                    entries.AddUnique(daty, (ValInt)daty.Start);
                                }
                            }
                        }

                        #endregion

                        if (changemade)
                        {
                            break;
                        }
                    }
                }

                if (changemade)
                {
                    continue;
                }

                found = new ArrayList(8);

                datx = entries[entries.Count - 1];
                daty = null;
                if (datx.End + 1 < data.Length)
                {
                    startPos = datx.End + 1;
                    endPos = data.Length;

                    #region SameCode
                    for (int i = 0; i < possibleEntry.Count; i++)
                    {
                        ValInt foundI = new ValInt(((ValInt)possibleEntry[i]).Value);
                        if (foundI >= startPos && foundI < endPos)
                        {
                            found.Add(possibleEntry[i]);
                        }
                    }

                    if (found.Count <= 1)
                    {
                        found.Sort();
                        if (endPos - startPos == sizeof_cell)
                        {
                            daty = DatHelpers.CreateDatEntry(data, sizeof_cell, startPos, endPos, false, false, false, true); //TODO: NOT TESTED
                        }
                        else
                        {
                            daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, startPos, endPos - startPos, true, true, true, false); //TODO: NOT TESTED
                        }

                        if (daty != null && daty.Type != DatEntryType.Bad)
                        {
                            changemade = true;
                            entries.AddUnique(daty, (ValInt)daty.Start);
                        }
                    }
                    else
                    { //vars + arrays
                        found.Add((ValInt)endPos);
                        found.Sort();
                        for (int i = 1; i < found.Count; i++)
                        {
                            ValInt foundI = new ValInt(((ValInt)found[i]).Value);
                            if ((foundI - startPos) == sizeof_cell)
                            {
                                daty = DatHelpers.CreateDatEntry(data, sizeof_cell, startPos, false, false, false, true); //TODO: NOT TESTED
                                startPos = foundI;
                            }
                            else
                            {
                                daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, startPos, foundI - startPos, true, true, true, false); //TODO: NOT TESTED
                                startPos = foundI;
                            }

                            if (daty != null && daty.Type != DatEntryType.Bad)
                            {
                                changemade = true;
                                entries.AddUnique(daty, (ValInt)daty.Start);
                            }
                        }
                    }

                    #endregion

                    if (changemade)
                    {
                        continue;
                    }
                }

            }
            return changemade;
        }

        public static bool ResolveGapsWarily(ResourceList entries, byte[] data, int sizeof_cell, ArrayList possibleEntry, ArrayList shureEntry)
        {
            if (entries.Count == 0)
            {
                return false;
            }

            bool changemade = true;

            IDatEntry datx = null;
            IDatEntry daty = null;
            while (changemade)
            {
                changemade = false;

                int startPos;
                int endPos;
                ArrayList found = new ArrayList(8);
                ArrayList foundShure = new ArrayList(8);

                datx = entries[0];
                if (datx.Start != 0)
                {
                    startPos = 0;
                    endPos = datx.Start;

                    #region SameCode

                    for (int i = 0; i < possibleEntry.Count; i++)
                    {
                        ValInt foundI = new ValInt(((ValInt)possibleEntry[i]).Value);
                        if (foundI >= startPos && foundI < endPos)
                        {
                            found.Add(possibleEntry[i]);
                        }
                    }

                    for (int i = 0; i < shureEntry.Count; i++)
                    {
                        ValInt foundI = new ValInt(((ValInt)shureEntry[i]).Value);
                        if (foundI >= startPos && foundI < endPos)
                        {
                            foundShure.Add(shureEntry[i]);
                        }
                    }

                    if (found.Count == foundShure.Count || ((endPos - startPos) / sizeof_cell == 33 && foundShure.Count <= 1))
                    {
                        if (found.Count <= 1 || (endPos - startPos) / sizeof_cell == 33) //BUG: possible bug, but most time fix
                        {
                            if (endPos - startPos == sizeof_cell)
                            {
                                daty = DatHelpers.CreateDatEntry(data, sizeof_cell, startPos, endPos, false, false, false, true); //TODO: NOT TESTED
                            }
                            else
                            {
                                daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, startPos, endPos - startPos, true, true, true, false); //TODO: NOT TESTED
                            }

                            if (daty != null && daty.Type != DatEntryType.Bad)
                            {
                                changemade = true;
                                entries.AddUnique(daty, (ValInt)daty.Start);
                            }
                        }
                        else
                        { //vars + arrays
                            found.Add((ValInt)endPos);
                            found.Sort();
                            for (int i = 1; i < found.Count; i++)
                            {
                                ValInt foundI = new ValInt(((ValInt)found[i]).Value);
                                if ((foundI - startPos) == sizeof_cell)
                                {
                                    daty = DatHelpers.CreateDatEntry(data, sizeof_cell, startPos, false, false, false, true); //TODO: NOT TESTED
                                    startPos = foundI;
                                }
                                else
                                {
                                    daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, startPos, foundI - startPos, true, true, true, false); //TODO: NOT TESTED
                                    startPos = foundI;
                                }

                                if (daty != null && daty.Type != DatEntryType.Bad)
                                {
                                    changemade = true;
                                    entries.AddUnique(daty, (ValInt)daty.Start);
                                }
                            }
                        }
                    }
                    #endregion
                    if (changemade)
                    {
                        continue;
                    }
                }

                for (int index = 0; index < entries.Count - 1; index++)
                {
                    found = new ArrayList(8);
                    foundShure = new ArrayList(8);

                    datx = entries[index];
                    daty = entries[index + 1];

                    if (daty.Start != datx.End + 1)
                    {
                        startPos = datx.End + 1;
                        endPos = daty.Start;
                        daty = null;

                        #region SameCode

                        for (int i = 0; i < possibleEntry.Count; i++)
                        {
                            ValInt foundI = new ValInt(((ValInt)possibleEntry[i]).Value);
                            if (foundI >= startPos && foundI < endPos)
                            {
                                found.Add(possibleEntry[i]);
                            }
                        }

                        for (int i = 0; i < shureEntry.Count; i++)
                        {
                            ValInt foundI = new ValInt(((ValInt)shureEntry[i]).Value);
                            if (foundI >= startPos && foundI < endPos)
                            {
                                foundShure.Add(shureEntry[i]);
                            }
                        }

                        //TODO: Videl9t massivi na 33 elementa ?


                        if (found.Count == foundShure.Count || ((endPos - startPos) / sizeof_cell == 33 && foundShure.Count <= 1))
                        {
                            if (found.Count <= 1 || (endPos - startPos) / sizeof_cell == 33) //BUG: possible bug, but most time fix
                            {
                                if (endPos - startPos == sizeof_cell)
                                {
                                    daty = DatHelpers.CreateDatEntry(data, sizeof_cell, startPos, endPos, false, false, false, true); //TODO: NOT TESTED
                                }
                                else
                                {
                                    daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, startPos, endPos - startPos, true, true, true, false); //TODO: NOT TESTED
                                }

                                if (daty != null && daty.Type != DatEntryType.Bad)
                                {
                                    changemade = true;
                                    entries.AddUnique(daty, (ValInt)daty.Start);
                                }
                            }
                            else
                            { //vars + arrays
                                found.Add((ValInt)endPos);
                                found.Sort();
                                for (int i = 1; i < found.Count; i++)
                                {
                                    ValInt foundI = new ValInt(((ValInt)found[i]).Value);
                                    if ((foundI - startPos) == sizeof_cell)
                                    {
                                        daty = DatHelpers.CreateDatEntry(data, sizeof_cell, startPos, false, false, false, true); //TODO: NOT TESTED
                                        startPos = foundI;
                                    }
                                    else
                                    {
                                        daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, startPos, foundI - startPos, true, true, true, false); //TODO: NOT TESTED
                                        startPos = foundI;
                                    }

                                    if (daty != null && daty.Type != DatEntryType.Bad)
                                    {
                                        changemade = true;
                                        entries.AddUnique(daty, (ValInt)daty.Start);
                                        // break; //TODO: analyze we need it here or not
                                    }
                                }
                            }
                        }
                        #endregion

                        if (changemade)
                        {
                            break;
                        }
                    }
                }
                if (changemade)
                {
                    continue;
                }

                found = new ArrayList(8);
                foundShure = new ArrayList(8);

                datx = entries[entries.Count - 1];
                daty = null;
                if (datx.End + 1 < data.Length)
                {
                    startPos = datx.End + 1;
                    endPos = data.Length;

                    #region SameCode

                    for (int i = 0; i < possibleEntry.Count; i++)
                    {
                        ValInt foundI = new ValInt(((ValInt)possibleEntry[i]).Value);
                        if (foundI >= startPos && foundI < endPos)
                        {
                            found.Add(possibleEntry[i]);
                        }
                    }

                    for (int i = 0; i < shureEntry.Count; i++)
                    {
                        ValInt foundI = new ValInt(((ValInt)shureEntry[i]).Value);
                        if (foundI >= startPos && foundI < endPos)
                        {
                            foundShure.Add(shureEntry[i]);
                        }
                    }
                    if (found.Count == foundShure.Count)// || ((endPos - startPos) / sizeof_cell == 33 && foundShure.Count <= 1))
                    {
                        if (found.Count <= 1)// || (endPos - startPos) / sizeof_cell == 33) //BUG: (endPos - startPos) / sizeof_cell == 33)
                        {
                            if (endPos - startPos == sizeof_cell)
                            {
                                daty = DatHelpers.CreateDatEntry(data, sizeof_cell, startPos, endPos, false, false, false, true); //TODO: NOT TESTED
                            }
                            else
                            {
                                daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, startPos, endPos - startPos, true, true, true, true); //TODO: NOT TESTED
                            }

                            if (daty != null && daty.Type != DatEntryType.Bad)
                            {
                                changemade = true;
                                entries.AddUnique(daty, (ValInt)daty.Start);
                            }
                        }
                        else
                        { //vars + arrays
                            found.Add((ValInt)endPos);
                            found.Sort();
                            for (int i = 1; i < found.Count; i++)
                            {
                                ValInt foundI = new ValInt(((ValInt)found[i]).Value);
                                if ((foundI - startPos) == sizeof_cell)
                                {
                                    daty = DatHelpers.CreateDatEntry(data, sizeof_cell, startPos, false, false, false, true); //TODO: NOT TESTED
                                    startPos = foundI;
                                }
                                else
                                {
                                    daty = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, startPos, foundI - startPos, true, true, true, false); //TODO: NOT TESTED
                                    startPos = foundI;
                                }

                                if (daty != null && daty.Type != DatEntryType.Bad)
                                {
                                    changemade = true;
                                    entries.AddUnique(daty, (ValInt)daty.Start);
                                }
                            }
                        }
                    }
                    #endregion

                    if (changemade)
                    {
                        continue;
                    }
                }
            }
            return changemade;
        }

        public static bool Contiguous(ResourceList entries, int sizeof_cell, int datalength)
        {
            bool gapfound = false;
            int lastend = 0;
            for (int i = 0; i < entries.Count; i++)
            {
                IDatEntry entry = entries[i];

                if (entry.Start != lastend)
                {
                    gapfound = true;
                    break;
                }

                lastend = entry.End + 1;
            }
            return ((!gapfound) && (lastend == datalength));
        }


        [System.Diagnostics.DebuggerStepThroughAttribute]
        public static ValInt ToInt(byte[] bytes, int length, int offset)
        {
            //int retval=0;
            ValInt retval = 0;
            //ValInt rtval = 0;
            if ((offset + length) <= bytes.Length)
            {
                #region OldCode
                /*
                if (length == 4)
                {
                    rtval =
                        (
                        (bytes[offset])
                        |
                        (bytes[offset+1]<<8)
                        |
                        (bytes[offset+2]<<16)
                        |
                        (bytes[offset+3]<<24)
                        );
                }
                else if (length==8)
                {
                    rtval =
                        (
                        (bytes[offset])
                        |
                        (bytes[offset+1]<<8)
                        |
                        (bytes[offset+2]<<16)
                        |
                        (bytes[offset+3]<<24)
                        |
                        (bytes[offset+4]<<32)
                        |
                        (bytes[offset+5]<<40)
                        |
                        (bytes[offset+6]<<48)
                        |
                        (bytes[offset+7]<<56)
                        );
                }
                for (int i=0;i<length;i+=1)
                {
                    int shift = (8*i);
                    int location = offset+i;
                    retval|=(bytes[location]<<shift);
                }	

                if (retval != rtval)
                {
                    throw new Exception();
                }
                */
                #endregion
                for (int i = 0; i < length; i += 1)
                {
                    retval |= (bytes[offset + i] << (8 * i));
                }

            }

            return retval;
        }


    }
}
