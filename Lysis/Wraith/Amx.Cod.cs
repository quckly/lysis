using System;

namespace Amx.Cod
{
    using System.Collections;
    using System.Collections.Specialized;
    using System.Text;
    using System.IO;

    using Amx.Core;
    using Amx.Ops;
    using Amx.Dat;
    using Amx.Logic.Jumps;
    using Amx.Logic.Labels;
    using Amx.Logic.Debug;

    using Files;

    public delegate void DisassemblyInstructionUpdateHandler(object sender, DisassemblyInstructionUpdateEventArgs e);
    public delegate void DisassemblyResourceUpdateHandler(object sender, DisassemblyResourceUpdateEventArgs e);
    public delegate void DisassemblyStatusMessageHandler(object sender, DisassemblyStatusMessageEventArgs e);
    public class DisassemblyInstructionUpdateEventArgs : EventArgs
    {
        private int count;
        private bool finished;

        public DisassemblyInstructionUpdateEventArgs(int count, bool finished)
        {
            this.count = count;
            this.finished = finished;
        }


        public int Count
        {
            get
            {
                return count;
            }
        }

        public bool Finished
        {
            get
            {
                return finished;
            }
        }
    }

    public class DisassemblyResourceUpdateEventArgs : EventArgs
    {
        private int loopcount;
        private int maxloops;
        private bool finished;

        public DisassemblyResourceUpdateEventArgs(int loopcount, int maxloops, bool finished)
        {
            this.loopcount = loopcount;
            this.maxloops = maxloops;
            this.finished = finished;
        }


        public int LoopCount
        {
            get
            {
                return loopcount;
            }
        }

        public int Loops
        {
            get
            {
                return maxloops;
            }
        }
        public bool Finished
        {
            get
            {
                return finished;
            }
        }
    }

    public class DisassemblyStatusMessageEventArgs : EventArgs
    {
        private string message;

        public DisassemblyStatusMessageEventArgs(string msg)
        {
            this.message = msg;
        }

        public string Message
        {
            get
            {
                return message;
            }
        }
    }


    public class Disassembly : IDisposable
    {
        public event DisassemblyInstructionUpdateHandler InstructionUpdate;
        public event DisassemblyResourceUpdateHandler ResourceUpdate;
        public event DisassemblyStatusMessageHandler StatusUpdate;

        private bool error;
        private bool debug;
        private IAmxInstance amx;
        private Ops ops;
        private Counters counters;
        private JumpTable jumps;

        private ResourceList l_dat;
        private InstructionList l_cod;
        private StringCollection l_src;

        private Hashtable tags;
        private Hashtable publics;
        private Hashtable privates;
        private Hashtable natives;
        private Hashtable datsegments;
        private SymbolTable symbols;

        private Hashtable datrefs;
        private SortedList localcalls;
        private SortedList systemcalls;

        public Disassembly(IAmxInstance amx, Ops ops)
        {
            error = false;
            this.ops = ops;
            this.amx = amx;

            if (amx.DebugHeader != null)
            {
                this.debug = true;
                symbols = amx.DebugHeader.GlobalSymbolTable;
            }
            else
                this.debug = false;
        }
        ~Disassembly()
        {
            if (ops != null)
            {
                this.Dispose();
            }
        }


        public void Disassemble()
        {
            if (amx.Valid)
            {
                l_cod = DisassembleCod(amx, ops);

                if (!error)
                {
                    if (debug)
                    {
                        l_dat = DisassembleDatFromDebug(amx, l_cod, l_dat); //resolved most of info
                        l_dat = DisassembleDat(amx, l_cod, l_dat);          //resolve gaps and strings
                    }
                    else
                    {
                        l_dat = DisassembleDat(amx, l_cod, l_dat);
                    }
                }
                if (!error)
                {
                    DigestCod();
                }

                if (!error)
                {
                    Correlate();
                }
                //IndexLines();
            }
        }


        public void DigestCod()
        {
            //bool debug = amx.Header.GetFlag(AMX_FLAGS.DEBUG);

            counters = new Counters();

            symbols = new SymbolTable(); //BUG: delete all known symbols... but used in Files, to remove bug need to add new storage for files, coz SymbolTable based on hastTable, and we will have double references to same values
            jumps = new JumpTable();

            publics = new Hashtable();
            privates = new Hashtable();
            datsegments = new Hashtable();
            datrefs = new Hashtable();

            localcalls = new SortedList();
            systemcalls = new SortedList();
            natives = new Hashtable();
            tags = new Hashtable();

            #region tags setup
            if (debug)
            {
                foreach (SymbolTagRealization str in amx.DebugHeader.TagTable)
                {
                    if (!tags.Contains(str.Value))
                    {
                        tags.Add(str.Value, str.Name);
                    }
                }
            }
            else
            {
                for (int i = 0; i < amx.Header.Tags.Length; i++)
                {
                    if (!tags.Contains((int)amx.Header.Tags[i].Address))
                    {
                        tags.Add(
                            //(int)amx.Header.Tags[i].Address,
                            i,
                            amx.Header.Tags[i].Data
                            );
                    }
                }
            }
            #endregion

            #region publics setup
            for (int i = 0; i < amx.Header.Publics.Length; i++)
            {
                if (!publics.Contains((int)amx.Header.Publics[i].Address))
                {
                    publics.Add(
                        (int)amx.Header.Publics[i].Address,
                        amx.Header.Publics[i].Data
                        );
                }
            }
            #endregion

            #region native setup
            for (int i = 0; i < amx.Header.Natives.Length; i++)
            {
                natives.Add(
                    i,
                    amx.Header.Natives[i].Data
                    );
            }
            #endregion

            #region dat setup
            if (l_dat != null)
            {
                for (int i = 0; i < l_dat.Count; i++)
                {
                    if (!datsegments.Contains(((IDatEntry)l_dat[i]).Start))
                    {
                        datsegments.Add(
                            new ValInt(((IDatEntry)l_dat[i]).Start),
                            l_dat[i]
                            );
                    }
                }
            }
            #endregion

            Symbol lastsymbol = null;
            foreach (Instruction line in l_cod)
            {
                //int index;

                switch (line.Op.Code)
                {
                    #region SYSREQ_C
                    case OPCODES.OP_SYSREQ_C:
                    case OPCODES.OP_SYSREQ_N:
                        if ((line.Param >= 0) && (line.Param < amx.Header.Natives.Length))
                        {
                            systemcalls.Add(line.Offset, line.Param);
                        }
                        else
                        {
                            systemcalls.Add(line.Offset, -1);
                        }
                        break;
                    #endregion
                    #region PROC
                    case OPCODES.OP_PROC:
                        if (!publics.ContainsKey(line.Offset))
                        {
                            bool isnamegiven = false;
                            if (debug)
                            {
                                foreach (Symbol symb in amx.DebugHeader.FunctionSymbolTable.Values)
                                {
                                    if (symb.Offset == line.Offset) //symb.Type == SymbolFlagType.function || symb.Type == SymbolFlagType.function_ptr)
                                    {
                                        privates.Add(line.Offset, symb.Name);
                                        isnamegiven = true;
                                        break;
                                    }
                                }
                            }

                            if (!isnamegiven)
                            {
                                privates.Add(line.Offset, "func_" + privates.Count.ToString("D2"));
                            }
                        }
                        break;
                    #endregion
                    #region CALL
                    case OPCODES.OP_CALL:
                        localcalls.Add(line.Offset, line.Param);
                        break;
                    #endregion
                    #region CASETBL
                    case OPCODES.OP_CASETBL:
                        Casetbl table = new Casetbl(line.Offset, line.GetParam(1), line.GetParam(0), counters.Casetbl);
                        jumps.Add(table);
                        CaseTriplet[] cases = (CaseTriplet[])line.Extra;
                        for (int i = 0; i < cases.Length; i++)
                        {
                            jumps.Add(new Case(cases[i], table.TableNumber, i));
                        }
                        break;
                    #endregion
                    #region SWITCH
                    case OPCODES.OP_SWITCH:
                        jumps.Add(new Switch(line.Offset, line.Param, counters.Switch));
                        break;
                    #endregion
                    #region JREL
                    case OPCODES.OP_JREL:
                        jumps.Add(new JumpRelative(line.Offset, line.Param, counters.Jump));
                        break;
                    #endregion
                    #region J*
                    case OPCODES.OP_JEQ:
                    case OPCODES.OP_JGEQ:
                    case OPCODES.OP_JGRTR:
                    case OPCODES.OP_JLEQ:
                    case OPCODES.OP_JLESS:
                    case OPCODES.OP_JNEQ:
                    case OPCODES.OP_JNZ:
                    case OPCODES.OP_JSGEQ:
                    case OPCODES.OP_JSGRTR:
                    case OPCODES.OP_JSLEQ:
                    case OPCODES.OP_JSLESS:
                    case OPCODES.OP_JUMP:
                    case OPCODES.OP_JUMP_PRI:
                    case OPCODES.OP_JZER:
                        jumps.Add(new Jump(line.Offset, line.Param, counters.Jump));
                        break;
                    #endregion
                    #region PUSH [|PRI|ALT|C]  | ZERO | INC |DEC
                    case OPCODES.OP_ZERO:
                    case OPCODES.OP_INC:
                    case OPCODES.OP_DEC:
                    case OPCODES.OP_PUSH:
                    case OPCODES.OP_PUSH_PRI:
                    case OPCODES.OP_PUSH_ALT:
                    case OPCODES.OP_PUSH_C:
                    case OPCODES.OP_CONST:
                        if (/*!debug &&*/ line.Param != 0 && ((Instruction)(l_cod[line.Offset + ((int)amx.Bits) * (line.NumParams+1)])).Op.Code != OPCODES.OP_SYSREQ_C ) 
                        {
                            if (datsegments.Contains(line.Param))
                            {
                                datrefs.Add(line.Offset, line.Param);
                            }
                        }
                        break;
                    case OPCODES.OP_PUSH2:
                    case OPCODES.OP_PUSH2_C:
                    case OPCODES.OP_PUSH3:
                    case OPCODES.OP_PUSH3_C:
                    case OPCODES.OP_PUSH4:
                    case OPCODES.OP_PUSH4_C:
                    case OPCODES.OP_PUSH5:
                    case OPCODES.OP_PUSH5_C:
                        Hashtable ths = new Hashtable(line.NumParams);
                        for (int i = 0; i < line.NumParams; i++)
                        {
                            if (line.GetParam(i) != 0)
                            {
                                ths.Add(i, line.GetParam(i));
                            }
                        }
                        if (ths.Count > 0)
                        {
                            datrefs.Add(line.Offset, ths);
                        }
                            break;
                    #endregion
                    #region CONST [ALT|PRI]
                    case OPCODES.OP_CONST_ALT:
                    case OPCODES.OP_CONST_PRI:
                    case OPCODES.OP_LOAD_ALT:
                    case OPCODES.OP_LOAD_PRI:
                    case OPCODES.OP_LOAD_S_ALT:
                    case OPCODES.OP_LOAD_S_PRI:
                    case OPCODES.OP_STOR_ALT:
                    case OPCODES.OP_STOR_PRI:
                    case OPCODES.OP_STOR_S_ALT:
                    case OPCODES.OP_STOR_S_PRI:
                        //if (line.Param!=0)
                        {
                            Instruction nextInst = l_cod[line.Offset + (int)amx.Bits * (line.NumParams + 1)];
                            if (datsegments.Contains(line.Param) && !OpHelper.isCallOp(nextInst.Op))
                            {
                                datrefs.Add(line.Offset, line.Param);
                            }
                        }
                        break;
                    #endregion
                    #region SYMBOL
                    case OPCODES.OP_SYMBOL:
                        lastsymbol = new Symbol(line.Offset, line.GetParam(1), line.GetParam(2), (byte[])line.Extra);
                        symbols.Add(line.Offset, lastsymbol);
                        break;
                    #endregion
                    #region SRANGE
                    case OPCODES.OP_SRANGE:
                        SymbolRange range = new SymbolRange(line.Offset, line.GetParam(0), line.GetParam(1));
                        if (lastsymbol != null)
                        {
                            lastsymbol.AddExtension(range);
                        }
                        else
                        {
                            throw new Exception("srange opcode with no preceeding symbol");
                        }
                        symbols.Add(line.Offset, range);
                        break;
                    #endregion
                    #region SYMTAG
                    case OPCODES.OP_SYMTAG:
                        SymbolTag tag = new SymbolTag(line.Offset, line.Param);
                        if (lastsymbol != null)
                        {
                            lastsymbol.AddExtension(tag);
                        }
                        symbols.Add(line.Offset, tag);
                        break;
                    #endregion
                    #region FILE
                    case OPCODES.OP_FILE:
                        symbols.Add(line.Offset, new Amx.Logic.Debug.SFile(line.Offset, line.Param, (byte[])line.Extra));
                        break;
                    #endregion
                    #region LINE
                    case OPCODES.OP_LINE:
                        symbols.Add(line.Offset, new SLine(line.Offset, line.GetParam(1), line.GetParam(0)));
                        break;
                    #endregion
                    case OPCODES.OP_BREAK:
                        //                        symbols.Add(
                        break;
                }
                //index=-1;
            }

            #region
            if (debug)
            {
                foreach (Amx.Logic.Debug.SFile sf in amx.DebugHeader.FileTable.Values)
                {
                    symbols.Add(sf.Offset, sf);
                }
                //symbols.Add(line.Offset, new Amx.Logic.Debug.SFile(line.Offset, line.Param, (byte[])line.Extra));
            }
            #endregion
        }

        private void Correlate()
        {
            DatCounters counters = new DatCounters();
            SortedList entries = new SortedList(l_dat.Count);
            DatEntryOffsetComparer datcmp = new DatEntryOffsetComparer();
            SortedList names = new SortedList(symbols.Count);

            foreach (ASymbol sym in symbols.Values)
            {
                Symbol symbol = sym as Symbol;
                if (
                    symbol != null &&
                    symbol.Relative == SymbolRelativeTo.DAT
                    )
                {
                    switch (symbol.Type)
                    {
                        case SymbolFlagType.array:
                        case SymbolFlagType.array_ptr:
                        case SymbolFlagType.variable:
                        case SymbolFlagType.variable_ptr:
                            names.Add(symbol.Position, symbol);
                            break;
                    }

                }
            }

            foreach (IDatEntry dat in l_dat)
            {
                if (dat.Name == null || dat.Name == "")
                {
                    Symbol symbol = names[dat.Start] as Symbol;
                    if (symbol != null)
                    {
                        dat.Name = symbol.Name;
                        dat.NameType = DatNameStrength.Symbol;
                    }
                    else
                    {
                        dat.Name = counters.GetName(dat.Type);
                        dat.NameType = DatNameStrength.Runtime;
                    }
                }
            }
        }


        public IAmxInstance Amx
        {
            get
            {
                return amx;
            }
        }

        public Ops Ops
        {
            get
            {
                return ops;
            }
        }


        public InstructionList Instructions
        {
            get
            {
                return l_cod;
            }
        }
        public ResourceList Resources
        {
            get
            {
                return l_dat;
            }
        }


        public StringCollection Source
        {
            get
            {
                return l_src;
            }
        }


        public Hashtable PublicsFunctions
        {
            get
            {
                return publics;
            }
        }
        public Hashtable PrivateFunctions
        {
            get
            {
                return privates;
            }
        }
        public Hashtable NativeFunctions
        {
            get
            {
                return natives;
            }
        }


        public Hashtable Tags
        {
            get
            {
                return tags;
            }
        }
        public SymbolTable Symbols
        {
            get
            {
                return symbols;
            }
        }

        public JumpTable Jumps
        {
            get
            {
                return jumps;
            }
        }
        public LabelTable Labels
        {
            get
            {
                return (jumps != null ? jumps.labels : null);
            }
        }


        public Hashtable ResourceReferences
        {
            get
            {
                return datrefs;
            }
        }

        public SortedList PrivateReferences
        {
            get
            {
                return localcalls;
            }
        }
        public SortedList NativeReferences
        {
            get
            {
                return systemcalls;
            }
        }


        public void LoadSource()
        {
            l_src = new StringCollection();
            string source = Path.GetDirectoryName(amx.Path) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(amx.Path) + ".sma";
            string str = amx.Path.Substring(0, amx.Path.LastIndexOf('.')) + ".sma";
            if (System.IO.File.Exists(source)/*System.IO.File.Exists(amx.Path.Substring(0,amx.Path.LastIndexOf('.')) +".sma")*/)
            {
                StreamReader sr = null;
                try
                {
                    sr = new StreamReader(source/*amx.Path.Substring(0,amx.Path.LastIndexOf('.')) +".sma"*/);
                    l_src.AddRange(sr.ReadToEnd().Split(new char[] { '\n' }));
                    for (int i = 0; i < l_src.Count; i++)
                    {
                        l_src[i] = l_src[i].TrimEnd();
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    if (sr != null)
                    {
                        sr.Close();
                        sr = null;
                    }
                }
            }
            else
            {
                l_src = null;
            }
        }


        private InstructionList DisassembleCod(IAmxInstance file, Ops ops)
        {
            int sizeof_cell = (int)file.Bits;
            Bits bits = file.Bits;
            //ArrayList lines = new ArrayList();
            InstructionList lines = new InstructionList();

            AmxStream bin = file. GetStream();

            Op curop = null;
            int opnum = 0, header_len = 0;//,op_len=0;
            Instruction line = null;

            bin.Seek((long)file.Header.Cod, SeekOrigin.Begin);
            //header_len = file.Header.Dat;// TODO: possible bug here
            header_len = (int)file.Header.CodSize + file.Header.Cod;

            while (bin.Position < header_len)//file.Header.Dat)
            {
                line = new Instruction();
                try
                {
                    //ValInt temp = (bin.ReadValInt(bits));
                    opnum = (int)(bin.ReadValInt(bits));
                }
                catch (InvalidOpException)
                {
                    error = true;
                    break;
                }
                curop = ops[opnum];
                //op_len = curop.ParamCount;
                line.Op = curop;
                line.Offset = (int)bin.Position - (file.Header.Cod + sizeof_cell);

                if (curop.VariableLength)
                {
                    #region variable length opcodes
                    switch (line.Op.Code)
                    {
                        case OPCODES.OP_CASETBL: // casetbl num default num*[case jump]
                            line.SetParam(0, bin.ReadValInt(bits));
                            line.SetParam(1, bin.ReadValInt(bits));
                            int numcases = line.GetParam(0);
                            lines.Add(line);
                            CaseTriplet[] cases = new CaseTriplet[numcases];
                            line.Extra = cases;
                            for (int i = 0; i < numcases; i++)
                            {
                                cases[i] = new CaseTriplet(
                                    (int)bin.Position - (file.Header.Cod + sizeof_cell),
                                    bin.ReadValInt(bits),
                                    bin.ReadValInt(bits)
                                    );
                                Instruction caseline = new Instruction();
                                caseline.Op = ops["NONE"];
                                caseline.Offset = cases[i].Position;
                                caseline.SetParam(0, cases[i].Value);
                                caseline.SetParam(1, cases[i].Target);
                                lines.Add(caseline);
                            }
                            line = null;
                            break;
                        case OPCODES.OP_FILE: // 124, FILE, size ord name...
                            line.SetParam(0, bin.ReadValInt(bits));
                            line.SetParam(1, bin.ReadValInt(bits));
                            line.Extra = bin.ReadBytes(line.GetParam(0) - ((line.Op.ParamCount - 2) * sizeof_cell));
                            break;
                        case OPCODES.OP_SYMBOL: // 126, SYMBOL, sze off flg name...
                            line.SetParam(0, bin.ReadValInt(bits));
                            line.SetParam(1, bin.ReadValInt(bits));
                            line.SetParam(2, bin.ReadValInt(bits));
                            line.Extra = bin.ReadBytes(line.GetParam(0) - ((line.Op.ParamCount - 2) * sizeof_cell));
                            break;
                        default:
                            throw new Exception("found op: " + curop.Name + " which has a variable opcount field and is unhandled");

                    }
                    #endregion
                }
                else
                {
                    //switch (line.OPCODE)
                    //{
                    //case OPCODES.OP_SRANGE:
                    //	line.SetParam(0,bin.ReadValInt(bits));
                    //	line.SetParam(1,bin.ReadValInt(bits));
                    //	break;
                    //	default:
                    for (int i = 0; i < curop.ParamCount; i++)
                        line.SetParam(i, bin.ReadValInt(bits));
                    //		break;
                    //}
                }

                if (line != null)
                {
                    lines.Add(line);
                }
                if ((lines.Count % 100) == 0)
                {
                    OnInstructionUpdate(lines.Count, false);
                }
            }
            OnInstructionUpdate(lines.Count, true);

            bin.Close();
            bin = null;

            return lines;
        }

        private ResourceList DisassembleDatFromDebug(IAmxInstance file, InstructionList lines, ResourceList entries)
        {
            if (entries == null)
            {
                entries = new ResourceList();
            }

            int sizeof_cell = (int)file.Bits;
            Bits bits = file.Bits;

            AmxStream bin = file.GetStream();
            byte[] data;
            bin.Seek((long)file.Header.Dat, SeekOrigin.Current);
            //data = bin.ReadBytes((int)file.Length - file.Header.Dat);
            data = bin.ReadBytes((int)file.Header.DatSize);

            bin.Close();
            bin = null;

            //Filter all Variables and Arrays
            //then find all strings and solve all gaps

            #region Parse symbol debug info
            foreach (ASymbol sym in amx.DebugHeader.GlobalSymbolTable.Values)
            {
                Symbol symbol = sym as Symbol;

                if (symbol != null && symbol.Relative == SymbolRelativeTo.DAT)
                {
                    IDatEntry newdatentry = new BadEntry();

                    int dimentionsCount = 0;
                    int lastDimension = 0;

                    foreach (ASymbol ext in symbol.Extensions)
                    {
                        SymbolRange srange = ext as SymbolRange;
                        if (srange != null)
                        {
                            dimentionsCount++;
                            lastDimension = srange.Value;
                        }
                    }

                    switch (symbol.Type)
                    {
                        case SymbolFlagType.variable:
                        case SymbolFlagType.variable_ptr:
                            newdatentry = DatHelpers.CreateDatEntry(data, sizeof_cell, symbol.Offset, false, false, false, true);
                            break;
                        case SymbolFlagType.array:
                        case SymbolFlagType.array_ptr:
                            switch (dimentionsCount)
                            {
                                //I dont want to create MuliArrays from description...
                                case 0:
                                    throw new Exception("array symbol declared with no bounds");
                                    break;
                                case 1:
                                    //array or string
                                    if (lastDimension == 0)
                                        newdatentry = DatHelpers.CreateDatEntry(data, sizeof_cell, symbol.Offset, false, true, true, false);
                                    else
                                        newdatentry = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, symbol.Offset, lastDimension * sizeof_cell, false, true, true, false);
                                    break;
                                default:
                                    newdatentry = DatHelpers.CreateDatEntry(data, sizeof_cell, symbol.Offset, true, true, true, false); //TODO: check
                                    break;
                            }


                            break;
                    }


                    if (newdatentry.Type != DatEntryType.Bad)
                    {
                        newdatentry.Name = symbol.Name;

                        foreach (SymbolExtension sy in symbol.Extensions)
                        {
                            if (sy is SymbolTag)
                            {
                                foreach (SymbolTagRealization str in amx.DebugHeader.TagTable)
                                {
                                    if (str.Value == ((SymbolTag)sy).Value)
                                    {
                                        newdatentry.TagName = str.Name;
                                    }
                                }
                            }
                        }

                        newdatentry.NameType = DatNameStrength.Symbol;
                        entries.Add(newdatentry);
                    }
                }
            }
            #endregion

            return entries;
        }



        private ResourceList DisassembleDat(IAmxInstance file, InstructionList lines, ResourceList entries)
        {
            if (entries == null)
            {
                entries = new ResourceList();
            }

            ArrayList strings = new ArrayList();
            ArrayList arrays = new ArrayList();

            int sizeof_cell = (int)file.Bits;
            Bits bits = file.Bits;

            bool debug = file.Header.GetFlag(AMX_FLAGS.DEBUG);
            //bool found = false;

            AmxStream bin = file.GetStream();
            byte[] data;
            bin.Seek((long)file.Header.Dat, SeekOrigin.Current);
            
            if (file.Header.GetFlag(AMX_FLAGS.COMPACT))
            {
                data = bin.ReadBytes((int)file.Length - file.Header.Dat);    
            }
            else
            {
                data = bin.ReadBytes((int)file.Header.DatSize);
            }
            bin.Close();
            bin = null;

            #region find all possible dat region references
            // always add 0 as a possible, it can't be detected with this method 
            // because rather than pushing a zero with CONST.pri the compiler will
            // merely ZERO.pri because its less work
            strings.Add(new ValInt(0, bits));
            //DatEntryMultiarrayDescription dem=null;

            /* In the standard sc compiler the const.* opcodes are used to push 
            * string onto the stack for arguments in function calls. If the 
            * optimizer is enabled this can be replaces with the use of a push.*
            * opcode instead. The use of the optimisation function was prohibited
            * the debug level setting meaning that if (debug) const.* else push*
            * could be used to cut down on the possible string.
            * 
            * In the amxxmod compiler BAILOPAN and PMOnoTo have changed the compiler
            * and remove the prohibition of optimisation with debugging ops included.
            * this requires that unless compiler version detection is possible all
            * push.* and const.* opcodes must be considered as possible argument
            * specification instructions. This may make the code look cleaner but it
            * results in a larger number of dat references to parse and possibly discard
            */

            //Standard compiler and optimized have a huge difference :( its more new compiler than optimized old one

            ArrayList NotConst = new ArrayList();
            ArrayList possibleEntrys = new ArrayList();

            ArrayList badEntrys = new ArrayList(); //cant start with...

            Instruction TempInstruction = null;

            foreach (Instruction dl in lines) //change with for
            {  //possible to use dat with PUSHADDR opcode...
                switch (dl.Op.Code)
                {
                    case OPCODES.OP_CONST_ALT:
                        #region
                        if (dl.Param > 0 && (dl.Param % sizeof_cell) == 0 && dl.Param <= data.Length)  // aligned with the cell size
                        {
                            Instruction nextInst = lines[dl.Offset + sizeof_cell * (dl.NumParams + 1)];
                            if (nextInst.Op.Code == OPCODES.OP_LOAD_S_PRI)
                            {
                                #region
                                Instruction nextInst2 = lines[nextInst.Offset + sizeof_cell * (nextInst.Op.ParamCount + 1)];
                                while (OpHelper.isDebugOp(nextInst2.Op)) //TODO: TAKE from here array size from BOUNDS
                                    nextInst2 = lines[nextInst2.Offset + sizeof_cell * (nextInst2.Op.ParamCount + 1)];

                                if (nextInst2.Op.Code == OPCODES.OP_LIDX || nextInst2.Op.Code == OPCODES.OP_IDXADDR || nextInst2.Op.Code == OPCODES.OP_IDXADDR_B || nextInst2.Op.Code == OPCODES.OP_LIDX_B)
                                {
                                    //IF known size from BOUNDS
                                    //IDatEntry entry = null;
                                    //entry = DatHelpers.CreateDatEntry(data, sizeof_cell, dl.Param, .Param, true, false, true, false);
                                    //    if (entry != null && entry.Type != DatEntryType.Bad)
                                    //    {
                                    //        entries.AddUnique(entry, entry.Start);
                                    //    }
                                    if (!NotConst.Contains(dl.Param))
                                        NotConst.Add(dl.Param);
                                    if (!possibleEntrys.Contains(dl.Param))
                                        possibleEntrys.Add(dl.Param);
                                    if (!badEntrys.Contains(dl.Param + sizeof_cell))
                                        badEntrys.Add(dl.Param + sizeof_cell); //��� ������� ��������� ������ �� ����� ���� ������� ���� ��������...
                                }
                                else
                                {
                                }
                                #endregion
                            }
                            else if (nextInst.Op.Code == OPCODES.OP_PUSH_ALT)
                            {
                                if (!possibleEntrys.Contains(dl.Param))
                                    possibleEntrys.Add(dl.Param);
                            }
                            else if (nextInst.Op.Code == OPCODES.OP_PUSH_PRI)
                            {
                                Instruction nextInst2 = lines[nextInst.Offset + sizeof_cell * (nextInst.Op.ParamCount + 1)];
                                while (nextInst2.Op.Code == OPCODES.OP_PUSH_PRI) //TODO: TAKE from here array size from BOUNDS
                                    nextInst2 = lines[nextInst2.Offset + sizeof_cell * (nextInst2.Op.ParamCount + 1)];
                                if (nextInst2.Op.Code == OPCODES.OP_PUSH_ALT)
                                {
                                    if (!possibleEntrys.Contains(dl.Param))
                                        possibleEntrys.Add(dl.Param);
                                }
                                else
                                {
                                }
                            }
                            else if (OpHelper.isLogicWithAltOp(nextInst.Op))
                            { //����� CONST? 
                                //dont do anything - should be constant
                            }
                            else if (OpHelper.isConditionalJump(nextInst.Op))
                            { //����� CONST?
                                //dont do anything - should be constant
                            }
                            else
                            {
                                if (!possibleEntrys.Contains(dl.Param))
                                    possibleEntrys.Add(dl.Param);
                            }

                        }
                        #endregion
                        break;
                    case OPCODES.OP_CONST_PRI:
                        #region
                        if (dl.Param > 0 && (dl.Param % sizeof_cell) == 0 && dl.Param <= data.Length)  // aligned with the cell size
                        {
                            Instruction nextInst = lines[dl.Offset + sizeof_cell * (dl.NumParams + 1)];
                            if (nextInst.Op.Code == OPCODES.OP_STOR_S_PRI)
                            {//dont do anything - should be constant
                                Instruction nextInst2 = lines[nextInst.Offset + sizeof_cell * (nextInst.Op.ParamCount + 1)];
                                if (OpHelper.isUnconditionalJump(nextInst2.Op))
                                {
                                    //dont do anything - should be constant
                                }
                                else
                                {

                                }
                            }
                            else if (nextInst.Op.Code == OPCODES.OP_LOAD_S_ALT)
                            {//dont do anything - should be constant
                                //������ ����������
                                Instruction nextInst2 = lines[nextInst.Offset + sizeof_cell * (nextInst.Op.ParamCount + 1)];
                                if (OpHelper.isCompareOp(nextInst2.Op) || OpHelper.isConditionalJump(nextInst2.Op) || OpHelper.isUnconditionalJump(nextInst2.Op) || OpHelper.isLogicWithAltOp(nextInst2.Op))
                                { //dont do anything - should be constant
                                }
                                else
                                { }
                            }
                            else if (OpHelper.isConditionalJump(nextInst.Op) || OpHelper.isCompareOp(nextInst.Op))
                            {//dont do anything - should be constant
                            }
                            else if (OpHelper.isUnconditionalJump(nextInst.Op))
                            {//most of time is constant, but possible in (xxx ? "on" : "off") get non constant - string etc
                                if (!possibleEntrys.Contains(dl.Param))
                                    possibleEntrys.Add(dl.Param);
                            }
                            else if (OpHelper.isReturnOp(nextInst.Op))
                            {//dont do anything - should be constant
                            }
                            else if (nextInst.Op.Code == OPCODES.OP_STACK)
                            {
                                Instruction nextInst2 = lines[nextInst.Offset + sizeof_cell * (nextInst.Op.ParamCount + 1)];
                                if (OpHelper.isReturnOp(nextInst2.Op))
                                {//dont do anything - should be constant
                                }
                                else
                                { }
                            }
                            else if (nextInst.Op.Code == OPCODES.OP_MOVE_ALT)
                            {
                                Instruction nextInst2 = lines[nextInst.Offset + sizeof_cell * (nextInst.Op.ParamCount + 1)];
                                if (nextInst2.Op.Code == OPCODES.OP_CONST_PRI || nextInst2.Op.Code == OPCODES.OP_ZERO_PRI)
                                {
                                    Instruction nextInst3 = lines[nextInst2.Offset + sizeof_cell * (nextInst2.Op.ParamCount + 1)];
                                    if (nextInst3.Op.Code == OPCODES.OP_MOVS)
                                    {
                                        IDatEntry entry = null;
                                        IDatEntry entry2 = null;
                                        if (nextInst3.Param != 0) //its possible xD
                                            if (nextInst3.Param == sizeof_cell) //if only 1 cell - its variable, but compilers that i know cant use it :)
                                            {
                                                entry = DatHelpers.CreateDatEntry(data, sizeof_cell, dl.Param, nextInst3.Param, false, false, false, true);
                                                entry2 = DatHelpers.CreateDatEntry(data, sizeof_cell, nextInst2.Param, nextInst3.Param, false, false, false, true);
                                            }
                                            else
                                            {
                                                entry = DatHelpers.CreateDatEntry(data, sizeof_cell, dl.Param, nextInst3.Param, false, false, true, false); //is MultiARRAY here good?
                                                entry2 = DatHelpers.CreateDatEntry(data, sizeof_cell, nextInst2.Param, nextInst3.Param, false, false, true, false); //is MultiARRAY here good?
                                            }

                                        if (entry != null && entry.Type != DatEntryType.Bad)
                                        {
                                            entries.AddUnique(entry, entry.Start);
                                        }

                                        if (entry2 != null && entry2.Type != DatEntryType.Bad)
                                        {
                                            entries.AddUnique(entry2, entry2.Start);
                                        }
                                    }
                                    else if (nextInst3.Op.Code == OPCODES.OP_STOR_I)
                                    {
                                        if (!NotConst.Contains(dl.Param))
                                            NotConst.Add(dl.Param);
                                        if (!possibleEntrys.Contains(dl.Param))
                                            possibleEntrys.Add(dl.Param);
                                    }
                                    else
                                    {

                                    }
                                }
                                else
                                {
                                    if (!possibleEntrys.Contains(dl.Param))
                                        possibleEntrys.Add(dl.Param);
                                }
                            }
                            else if (nextInst.Op.Code == OPCODES.OP_ADDR_ALT)
                            {
                                Instruction nextInst2 = lines[nextInst.Offset + sizeof_cell * (nextInst.Op.ParamCount + 1)];
                                if (nextInst2.Op.Code == OPCODES.OP_MOVS)
                                {
                                    IDatEntry entry = null;
                                    if (nextInst2.Param != 0) //its possible xD
                                        if (nextInst2.Param == sizeof_cell) //if only 1 cell - its variable, but compilers that i know cant use it :)
                                        {
                                            entry = DatHelpers.CreateDatEntry(data, sizeof_cell, dl.Param, nextInst2.Param, false, false, false, true); //is MultiARRAY here good?
                                        }
                                        else
                                        {
                                            entry = DatHelpers.CreateDatEntry(data, sizeof_cell, dl.Param, nextInst2.Param, false, false, true, false); //is MultiARRAY here good?
                                        }

                                    if (entry != null && entry.Type != DatEntryType.Bad)
                                    {
                                        entries.AddUnique(entry, entry.Start);
                                    }
                                }
                                else { }

                            }
                            else if (nextInst.Op.Code == OPCODES.OP_ADD_C || nextInst.Op.Code == OPCODES.OP_PUSH_PRI || nextInst.Op.Code == OPCODES.OP_LOAD_I)
                            {
                                if (nextInst.Op.Code == OPCODES.OP_LOAD_I)
                                {
                                    if (!NotConst.Contains(dl.Param))
                                        NotConst.Add(dl.Param);
                                    if (!possibleEntrys.Contains(dl.Param))
                                        possibleEntrys.Add(dl.Param);
                                }
                                else
                                {
                                    Instruction nextInst2 = lines[nextInst.Offset + sizeof_cell * (nextInst.Op.ParamCount + 1)];

                                    if (nextInst.Op.Code == OPCODES.OP_PUSH_PRI)
                                    {
                                        if (!possibleEntrys.Contains(dl.Param))
                                            possibleEntrys.Add(dl.Param);
                                    }

                                    while (nextInst2.Op.Code == OPCODES.OP_ADD_C || nextInst2.Op.Code == OPCODES.OP_PUSH_PRI)
                                    {
                                        if (nextInst2.Op.Code == OPCODES.OP_PUSH_PRI)
                                        {
                                            if (!possibleEntrys.Contains(dl.Param))
                                                possibleEntrys.Add(dl.Param);
                                        }
                                        nextInst2 = lines[nextInst2.Offset + sizeof_cell * (nextInst2.Op.ParamCount + 1)];
                                    }
                                    if (nextInst2.Op.Code == OPCODES.OP_LOAD_I)
                                    {
                                        if (!NotConst.Contains(dl.Param))
                                            NotConst.Add(dl.Param);
                                        if (!possibleEntrys.Contains(dl.Param))
                                            possibleEntrys.Add(dl.Param);
                                    }
                                    else if (nextInst2.Op.Code == OPCODES.OP_CONST_PRI)
                                    {
                                        if (nextInst2.Param > 0 && (nextInst2.Param % sizeof_cell) == 0 && nextInst2.Param <= data.Length)
                                        {
                                            Instruction nextInst3 = lines[nextInst2.Offset + sizeof_cell * (nextInst2.Op.ParamCount + 1)];
                                            while (nextInst3.Op.Code == OPCODES.OP_ADD_C || nextInst3.Op.Code == OPCODES.OP_PUSH_PRI)
                                            {
                                                if (nextInst3.Op.Code == OPCODES.OP_PUSH_PRI)
                                                {
                                                    if (!possibleEntrys.Contains(nextInst2.Param))
                                                        possibleEntrys.Add(nextInst2.Param);
                                                }
                                                nextInst3 = lines[nextInst3.Offset + sizeof_cell * (nextInst3.Op.ParamCount + 1)];
                                            }
                                            if (nextInst3.Op.Code == OPCODES.OP_LOAD_I)
                                            {
                                                if (!NotConst.Contains(nextInst2.Param))
                                                    NotConst.Add(nextInst2.Param);
                                                if (!possibleEntrys.Contains(nextInst2.Param))
                                                    possibleEntrys.Add(nextInst2.Param);

                                                nextInst3 = lines[nextInst3.Offset + sizeof_cell * (nextInst3.Op.ParamCount + 1)];
                                                if (nextInst3.Op.Code == OPCODES.OP_POP_ALT)
                                                {
                                                    nextInst3 = lines[nextInst3.Offset + sizeof_cell * (nextInst3.Op.ParamCount + 1)];
                                                    if (nextInst3.Op.Code == OPCODES.OP_STOR_I)
                                                    {
                                                        if (!NotConst.Contains(dl.Param))
                                                            NotConst.Add(dl.Param);
                                                        if (!possibleEntrys.Contains(dl.Param))
                                                            possibleEntrys.Add(dl.Param);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!possibleEntrys.Contains(dl.Param))
                                            possibleEntrys.Add(dl.Param);
                                        //Unknown //push_s_pri
                                    }
                                }

                            }
                            else
                            {
                                if (!possibleEntrys.Contains(dl.Param))
                                    possibleEntrys.Add(dl.Param);
                            }
                        }
                        #endregion
                        break;
                    case OPCODES.OP_PUSH_C:
                        #region
                        if (dl.Param > 0 && (dl.Param % sizeof_cell) == 0 && dl.Param <= data.Length)  // aligned with the cell size
                        {
                            //here Anything Arrays MultiArrays Strings
                            if (!strings.Contains(dl.Param))
                            {
                                //in optimized compiler we cant catch here Multiarrays, they implemented with " const.alt mult_arr; load.s.pri pushed_offset ;...; IDXADDR "
                                Instruction nextInst = lines[dl.Offset + sizeof_cell * (dl.NumParams + 1)];
                                //There is possibility of OP_SYSREQ_PRI //D
                                if (nextInst.Op.Code == OPCODES.OP_SYSREQ_PRI || nextInst.Op.Code == OPCODES.OP_SYSREQ_D)
                                {
                                    { } //TODO: debug ME
                                }

                                if (!OpHelper.isCallOp(nextInst.Op))//check next opcode, to filter number of arguments
                                {                                   //if data == 0 - end of string// Could it be Empty string or not? //TODO: check
                                    if (data[dl.Param] != 0) //Possible Leak, with not-zero variable
                                    {
                                        if (!strings.Contains(dl.Param))
                                            strings.Add(dl.Param);
                                        if (!possibleEntrys.Contains(dl.Param))
                                            possibleEntrys.Add(dl.Param);
                                    }
                                    else
                                    {
                                        if (!possibleEntrys.Contains(dl.Param))
                                            possibleEntrys.Add(dl.Param);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                        break;
                    case OPCODES.OP_PUSH2_C:
                    case OPCODES.OP_PUSH3_C:
                    case OPCODES.OP_PUSH4_C:
                    case OPCODES.OP_PUSH5_C:
                        for (int i = 0; i < dl.NumParams; i++)
                        {
                            ValInt param = dl.GetParam(i);
                            if (param > 0 && (param % sizeof_cell) == 0 && param <= data.Length)  // aligned with the cell size
                            {
                                //here Anything Arrays MultiArrays Strings
                                if (!strings.Contains(param))
                                {
                                    if (data[param] != 0) //Possible Leak, with not-zero variable
                                    {
                                        if (!strings.Contains(param))
                                            strings.Add(param);
                                        if (!possibleEntrys.Contains(param))
                                            possibleEntrys.Add(param);
                                    }
                                    else
                                    {
                                        if (!possibleEntrys.Contains(param))
                                            possibleEntrys.Add(param);
                                    }
                                }
                            }
                        }
                        break;
                }
                //TempInstruction = dl;
            }
            //in possibleEntries or Arrays and vars etc, or constant values!

            //lets change strings to SmthEsle if we acess them as storage also try to detect variables
            #region Filter strings, and finding opcodes with address params
            foreach (Instruction dl in lines)
            {
                IDatEntry entry = null;
                switch (dl.Op.Code)
                {
                    case OPCODES.OP_PUSH: //using variable
                    case OPCODES.OP_ZERO:   //Zero the variable
                    case OPCODES.OP_INC:  // Inc variable
                    case OPCODES.OP_DEC:  // Decrease variable
                        if (dl.Param >= 0 && (dl.Param % sizeof_cell) == 0 && dl.Param <= data.Length)
                        {
                            entry = DatHelpers.CreateDatEntry(data, sizeof_cell, dl.Param, false, false, false, true);

                            if (entry != null && entry.Type != DatEntryType.Bad)
                            {
                                entries.AddUnique(entry, entry.Start);
                            }

                            if (strings.Contains(dl.Param))
                                strings.Remove(dl.Param);

                            //TODO: check that we can store in muliArray and Array smth acessing with such opcodes                                                                                          
                            //its possible while doing "func_00(arr[0])"....
                            //but compiler is a bit stupid and generates  CONST.pri var ; LOAD.I ; PUSH.pri ;
                            //but should " PUSH var ; " and thats all xD
                            //CHECK: may cause bugs with good compilers

                            ////NOTE: be carefull with OP_PUSH - while using it not in standard compiler can cause bad entry
                            if (!possibleEntrys.Contains(dl.Param))
                                possibleEntrys.Add(dl.Param);
                        }
                        break;
                    case OPCODES.OP_PUSH2:
                    case OPCODES.OP_PUSH3:
                    case OPCODES.OP_PUSH4:
                    case OPCODES.OP_PUSH5:
                        for (int i = 0; i < dl.NumParams; i++)
                        {
                            //TODO: check that we can store in muliArray and Array smth acessing with such opcodes                                                                                          
                            //its possible while doing "func_00(arr[0])"....
                            //but compiler is a bit stupid and generates  CONST.pri var ; LOAD.I ; PUSH.pri ;
                            //but should " PUSH var ; " and thats all xD
                            //CHECK: may cause bugs with good compilers

                            ////NOTE: be carefull with OP_PUSH - while using it not in standard compiler can cause bad entry
                            ValInt param = dl.GetParam(i);
                            if (param >= 0 && (param % sizeof_cell) == 0 && param <= data.Length)
                            {
                                entry = DatHelpers.CreateDatEntry(data, sizeof_cell, param, false, false, false, true);

                                if (entry != null && entry.Type != DatEntryType.Bad)
                                    entries.AddUnique(entry, entry.Start);
                                if (strings.Contains(param))
                                    strings.Remove(param);
                                if (!possibleEntrys.Contains(param))
                                    possibleEntrys.Add(param);
                            }
                        }
                        break;
                    case OPCODES.OP_CONST:
                        if (dl.Param >= 0 && (dl.Param % sizeof_cell) == 0 && dl.Param <= data.Length)
                        {
                            //is it possible to use CONST with arrays and Multi-arrays 
                            //if not - create var entry here

                            if (strings.Contains(dl.Param))
                                strings.Remove(dl.Param);

                            if (!NotConst.Contains(dl.Param))
                                NotConst.Add(dl.Param);

                            if (!possibleEntrys.Contains(dl.Param))
                                possibleEntrys.Add(dl.Param);
                        }
                        break;
                    case OPCODES.OP_LOAD_PRI:
                    case OPCODES.OP_LOAD_ALT:
                        if (dl.Param >= 0 && (dl.Param % sizeof_cell) == 0 && dl.Param <= data.Length)
                        {
                            //we cant create arrays here. coz size of them unknown for us....
                            //so, we cant understand is it array or variable
                            //TODO: is it possible to create some filters of near opcodes to determine what we found : variable or array.

                            //if OP_LOAD_PRI found next could be BOUNDS

                            if (strings.Contains(dl.Param))
                                strings.Remove(dl.Param);

                            //entry = DatHelpers.CreateDatEntry(data, sizeof_cell, dl.Param, true, false, false, false);
                            if (!NotConst.Contains(dl.Param))
                            {
                                NotConst.Add(dl.Param);
                            }

                            if (!possibleEntrys.Contains(dl.Param))
                                possibleEntrys.Add(dl.Param);
                        }
                        break;
                    case OPCODES.OP_STOR_PRI:
                    case OPCODES.OP_STOR_ALT:
                        if (dl.Param >= 0 && (dl.Param % sizeof_cell) == 0 && dl.Param <= data.Length)  // aligned with the cell size
                        {
                            if (!NotConst.Contains(dl.Param))
                            {
                                NotConst.Add(dl.Param);
                            }

                            //we cant create arrays here. coz size of them unknown for us....
                            //so, we cant understand is it array or variable
                            //TODO: is it possible to create some filters of near opcodes to determine what we found : variable or array.
                            if (TempInstruction != null && (TempInstruction.Op.Code == OPCODES.OP_STACK || TempInstruction.Op.Code == OPCODES.OP_SYSREQ_N))
                            {
                                //CHECK: Its only for Test - dunno is it possible to found such places not in ( var = func() )
                                entry = DatHelpers.CreateDatEntry(data, sizeof_cell, dl.Param, false, false, false, true);

                                if (entry != null && entry.Type != DatEntryType.Bad)
                                {
                                    entries.AddUnique(entry, entry.Start);
                                }
                            }
                            else
                            {
                                if (strings.Contains(dl.Param))
                                    strings.Remove(dl.Param);
                            }

                            if (!possibleEntrys.Contains(dl.Param))
                                possibleEntrys.Add(dl.Param);
                        }
                        break;
                }
                TempInstruction = dl;
            }
            #endregion

            #region Delete bad entries from possible entries
            if (badEntrys.Count > 0)
            {
                for (int i = 0; i < badEntrys.Count; i++)
                {
                    ValInt I1 = new ValInt(((ValInt)badEntrys[i]).Value);
                    for (int j = 0; j < possibleEntrys.Count; j++)
                    {
                        ValInt J1 = new ValInt(((ValInt)possibleEntrys[j]).Value);
                        //long J1 = (long)((ValInt)badEntrys[i]).Value; //Mega Bug
                        if (I1 == J1)
                        {
                            possibleEntrys.Remove((ValInt)I1);
                            badEntrys.Remove((ValInt)I1);
                            i--;
                            break;
                        }
                    }
                }
            }

            #endregion

            //debug check
            foreach (ValInt offset in strings)
            {
                if (offset % sizeof_cell != 0)
                {
                    strings.Remove(offset);
                }
            }
            foreach (ValInt offset in possibleEntrys)
            {
                if (offset % sizeof_cell != 0)
                {
                    possibleEntrys.Remove(offset);
                }
            }
            foreach (ValInt offset in NotConst)
            {
                if (offset % sizeof_cell != 0)
                {
                    NotConst.Remove(offset);
                }
            }

            #region Check Strings and add to entries
            foreach (ValInt offset in strings)
            {
                IDatEntry entry = DatHelpers.CreateDatEntry(data, sizeof_cell, offset, true, true, false, false); //Check: supress multiArrays ?
                if (entry != null && entry.Type != DatEntryType.Bad)
                {
                    entries.AddUnique(entry, entry.Start);
                }
            }
            #endregion

            possibleEntrys.Sort();//only for debug;
            NotConst.Sort();//only for debug;

            if (DatHelpers.ResolveOcclusion(entries, data, sizeof_cell))
            {
                DatHelpers.ResolveOcclusionWith(entries, data, sizeof_cell, possibleEntrys);
                DatHelpers.ResolveOcclusionWith(entries, data, sizeof_cell, NotConst);
            }

            #region Check of FirstString - is it string or not
            if (entries.Count > 0 && entries[0].Start == 0 && entries[0].Type == DatEntryType.String)
            { //we have it, lets compare it with our notConst vars
                //we nned it if someone creates (new var1=72; new var2;) its like string - but really 2 vars
                foreach (ValInt offset in NotConst)
                {
                    if (new ValInt((offset.Value)) >= entries[0].Start && new ValInt((offset.Value)) < entries[0].End)
                    {
                        if (!possibleEntrys.Contains(offset)) //OMG why we need it ???
                            possibleEntrys.Add(offset);

                        entries.Remove(entries[0]);
                        break;
                    }
                }
            }
            #endregion

            possibleEntrys.Sort();
            NotConst.Sort();

            DatHelpers.ResolveCellGaps(entries, data, sizeof_cell); //most time did nothing
            DatHelpers.ResolveOcclusionWith(entries, data, sizeof_cell, possibleEntrys);

            DatHelpers.ResolveArrayGaps(entries, data, sizeof_cell, possibleEntrys); //most time did nothing
            DatHelpers.ResolveOcclusionWith(entries, data, sizeof_cell, possibleEntrys);
            DatHelpers.ResolveOcclusionWith(entries, data, sizeof_cell, NotConst);

            if (DatHelpers.EqualContent(possibleEntrys, NotConst))
            {
                DatHelpers.ResolveGaps(entries, data, sizeof_cell, NotConst);
            }
            else
            {
                if (NotConst.Count > 0)
                {
                    DatHelpers.ResolveGapsWarily(entries, data, sizeof_cell, possibleEntrys, NotConst);

                    DatHelpers.ResolveOcclusionWith(entries, data, sizeof_cell, possibleEntrys);
                    DatHelpers.ResolveOcclusionWith(entries, data, sizeof_cell, NotConst);
                }

                //TODO: move to DatHelper
                #region Check gaps between NotConstans and possible entries - if they are equal and not contains entries
                if (possibleEntrys.Count > 1 && NotConst.Count > 1)
                {
                    for (int i = 0; i < NotConst.Count - 1; i++)
                    {
                        ValInt I1 = new ValInt(((ValInt)NotConst[i]).Value);
                        ValInt I2 = new ValInt(((ValInt)NotConst[i + 1]).Value);

                        for (int j = 0; j < possibleEntrys.Count - 1; j++)
                        {
                            ValInt J1 = new ValInt(((ValInt)possibleEntrys[j]).Value);
                            ValInt J2 = new ValInt(((ValInt)possibleEntrys[j + 1]).Value);

                            if (I1 == J1 && I2 == J2)
                            {
                                bool overlapWithEntry = false;
                                foreach (IDatEntry entry in entries)
                                {
                                    if (entry.End < I1 || entry.End > I2)
                                        continue;
                                    for (int ii = I1; ii < I2; ii += sizeof_cell)
                                    {
                                        if (entry.Contains((ValInt)ii))
                                        {
                                            overlapWithEntry = true;
                                            break;
                                        }
                                    }
                                }
                                if (overlapWithEntry)
                                    break;

                                if (I2 - I1 == sizeof_cell)
                                {
                                    IDatEntry entry = DatHelpers.CreateDatEntry(data, sizeof_cell, I1, false, false, false, true);
                                    if (entry != null && entry.Type != DatEntryType.Bad)
                                    {
                                        entries.AddUnique(entry, entry.Start);
                                        possibleEntrys.Remove((ValInt)I1);
                                        j--;
                                        NotConst.Remove((ValInt)I1);
                                        i--;
                                        break;
                                    }
                                }
                                else
                                {
                                    IDatEntry entry = DatHelpers.CreateDatEntryWithKnownLength(data, sizeof_cell, I1, I2 - I1, true, true, true, false);
                                    if (entry != null && entry.Type != DatEntryType.Bad)
                                    {
                                        entries.AddUnique(entry, entry.Start);
                                        possibleEntrys.Remove((ValInt)I1);
                                        j--;
                                        NotConst.Remove((ValInt)I1);
                                        i--;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                }
                #endregion

                DatHelpers.ResolveGapsWarily(entries, data, sizeof_cell, possibleEntrys, NotConst);
                DatHelpers.ResolveOcclusionWith(entries, data, sizeof_cell, possibleEntrys);
                DatHelpers.ResolveOcclusionWith(entries, data, sizeof_cell, NotConst);

                //BAD: but we havent other way //BUG: 
                if (!DatHelpers.Contiguous(entries, sizeof_cell, data.Length))
                {
                    DatHelpers.ResolveGaps(entries, data, sizeof_cell, possibleEntrys);
                    DatHelpers.ResolveOcclusionWith(entries, data, sizeof_cell, possibleEntrys);
                    DatHelpers.ResolveOcclusionWith(entries, data, sizeof_cell, NotConst);
                }
            }

            #endregion
            #region Old code
            //if (!DatHelpers.Contiguous(entries, sizeof_cell, data.Length))
            //{
            //    bool changed = true;
            //    int count = 0;
            //    int max = 6;
            //    while (changed && count < max)
            //    {
            //        OnResourceUpdate(count, max, false);
            //        changed = DatHelpers.ResolveOcclusion(entries, data, sizeof_cell);
            //        changed = DatHelpers.ResolveGaps(entries, data, sizeof_cell);
            //        if (!changed)
            //        {
            //            changed = !DatHelpers.Contiguous(entries, sizeof_cell, data.Length);
            //        }
            //        count++;
            //    }
            //}
            #endregion

            //OnResourceUpdate(count, max, true);

            if (!DatHelpers.Contiguous(entries, sizeof_cell, data.Length))
            {
                OnStatusMessage("non contiguous dat region, there may be missing entries");
            }

            return entries;
        }


        protected virtual void OnStatusMessage(string message)
        {
            if (StatusUpdate != null)
            {
                StatusUpdate(this, new DisassemblyStatusMessageEventArgs(message));
            }
        }
        protected virtual void OnInstructionUpdate(int count, bool finished)
        {
            if (InstructionUpdate != null)
            {
                InstructionUpdate(this, new DisassemblyInstructionUpdateEventArgs(count, finished));
            }
        }
        protected virtual void OnResourceUpdate(int loop, int max, bool finished)
        {
            if (ResourceUpdate != null)
            {
                ResourceUpdate(this, new DisassemblyResourceUpdateEventArgs(loop, max, finished));
            }
        }


        #region IDisposable Members

        public void Dispose()
        {
            lock (this)
            {

                if (jumps != null)
                {
                    jumps.Clear();
                }

                if (l_dat != null)
                {
                    l_dat.Clear();
                }
                if (l_cod != null)
                {
                    l_cod.Clear();
                }
                if (l_src != null)
                {
                    l_src.Clear();
                }
                //if (lineindex!=null)
                //{
                //	lineindex.Clear();					
                //}
                if (publics != null)
                {
                    publics.Clear();
                }
                if (privates != null)
                {
                    privates.Clear();

                }
                if (datsegments != null)
                {
                    datsegments.Clear();
                }
                if (datrefs != null)
                {
                    datrefs.Clear();
                }
                if (symbols != null)
                {
                    symbols.Clear();
                }
                if (localcalls != null)
                {
                    localcalls.Clear();
                }
                if (systemcalls != null)
                {
                    systemcalls.Clear();
                }
                if (natives != null)
                {
                    natives.Clear();
                }
                publics = null;
                privates = null;
                datsegments = null;
                datrefs = null;
                symbols = null;
                localcalls = null;
                systemcalls = null;
                natives = null;
                amx = null;
                ops = null;
                counters = null;
                jumps = null;
                l_dat = null;
                l_cod = null;
                l_src = null;
                //lineindex=null;
                InstructionUpdate = null;
                ResourceUpdate = null;
                StatusUpdate = null;
            }
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}


namespace Amx.Cod
{
    using System.Collections;
    using System.Text;

    using Amx.Core;
    using Amx.Ops;

    public class Instruction : IDisposable
    {
        private Op l_op;
        private int l_offset;
        private ValInt[] l_params;
        private object l_extra;
        private int l_numparams;

        public Instruction()
            : this(0, null)
        {
        }

        public Instruction(bool b, int offset, Op op, ValInt param)
        {
            l_params = new ValInt[1];
            l_op = op;
            l_params[0] = param;
            l_offset = offset;
            l_extra = null;
        }

        public Instruction(int offset, Op op)
        {
            l_params = new ValInt[1];
            l_op = op;
            l_offset = offset;
            l_extra = null;
        }
        ~Instruction()
        {
            if (l_op != null)
            {
                this.Dispose();
            }
        }


        public int NumParams
        {
            get
            {
                return l_numparams + 1;
            }
        }


        public ValInt Param
        {
            get
            {
                return this.GetParam(0);//l_params[0];
            }
            //set
            //{
            //l_params[0] = value;
            //if (l_numparams<1)
            //{
            //	l_numparams=1;
            //}
            //this.SetParam(0,value);
            //}
        }

        public ValInt GetParam()
        {
            return GetParam(0);
        }
        public ValInt GetParam(int index)
        {
            if (index < l_params.Length)
            {
                return l_params[index];
            }
            else
                throw new IndexOutOfRangeException();
        }

        public void SetParam(ValInt value)
        {
            SetParam(0, value);
        }
        public void SetParam(int index, ValInt value)
        {
            if (index + 1 > l_params.Length)
            {
                ValInt[] arr = new ValInt[index + 1];
                l_params.CopyTo(arr, 0);
                l_params = arr;
            }
            //if (index<l_params.Length)
            //{
            l_params[index] = value;
            //}
            if (index > 0 && index > l_numparams)
                l_numparams = index;
        }


        public int Offset
        {
            get
            {
                return l_offset;
            }
            set
            {
                l_offset = value;
            }
        }

        public Op Op
        {
            get
            {
                return l_op;
            }
            set
            {
                l_op = value;
            }
        }

        //public OPCODES OPCODE
        //{
        //	get
        //	{
        //		return (OPCODES)l_op.Code;
        //	}
        //}

        /*
        public bool IsdatReference
        {
            get
            {
                return l_isdatreference;
            }
            set 
            {
                l_isdatreference = value;
            }
        }
        */

        public object Extra
        {
            get
            {
                return l_extra;
            }
            set
            {
                if (l_extra == null && value != null)
                {
                    l_numparams += 1;
                    l_numparams = ~l_numparams;
                }
                l_extra = value;
                if (l_extra == null)
                {
                    l_numparams = ~l_numparams;
                    l_numparams -= 1;
                }
            }
        }


        public string FormatValue(int value, NumberOptions options)
        {
            string retval = "";
            string formatter = "";

            switch (options.Format)
            {
                case NumberFormat.SignedHex:
                case NumberFormat.Hex:
                    formatter = "X";
                    break;
                case NumberFormat.Dec:
                    formatter = "D";
                    break;
            }

            if (options.NumWidth != 0)
            {
                formatter += options.NumWidth.ToString();
            }

            switch (options.Format)
            {
                case NumberFormat.SignedHex:
                    if (value < 0)
                    {
                        retval = ("-0x" + ((~value) + 1).ToString(formatter));
                    }
                    else
                    {
                        retval = ("0x" + value.ToString(formatter));
                    }
                    break;
                case NumberFormat.Hex:
                    retval = ("0x" + value.ToString(formatter));
                    break;
                case NumberFormat.Dec:
                    retval = (value.ToString(formatter));
                    break;
            }

            switch (options.Alignment)
            {
                case Align.Left:
                    retval = retval.PadRight(options.MaxWidth, options.PadChar);
                    break;
                case Align.Right:
                    retval = retval.PadLeft(options.MaxWidth, options.PadChar);
                    break;
            }

            return retval;
        }
        public string FormatValue(ValInt value, NumberOptions options)
        {
            return FormatValue(value, options, null, null);
            /*
            string retval="";
            string formatter="";

            switch (options.Format)
            {
                case NumberFormat.SignedHex:
                case NumberFormat.Hex:
                    formatter = "X";
                    break;
                case NumberFormat.Dec:
                    formatter = "D";
                    break;
            }

            if (options.NumWidth!=0)
            {
                formatter+=options.NumWidth.ToString();
            }

            switch (options.Format)
            {
                case NumberFormat.SignedHex:
                    if (value<0)
                    {
                        retval = ("-0x" + ((~value)+1).ToString(formatter));
                    }
                    else
                    {
                        retval = ("0x" + value.ToString(formatter));
                    }
                    break;
                case NumberFormat.Hex:
                    retval = ("0x" + value.ToString(formatter));
                    break;
                case NumberFormat.Dec:
                    retval = (value.ToString(formatter));
                    break;
            }

            switch (options.Alignment)
            {
                case Align.Left:
                    retval=retval.PadRight(options.MaxWidth,options.PadChar);
                    break;
                case Align.Right:
                    retval=retval.PadLeft(options.MaxWidth,options.PadChar);
                    break;
            }

            return retval;
            */
        }

        public string FormatValue(ValInt value, NumberOptions options, string prefix, string postfix)
        {
            StringBuilder str = new StringBuilder(options.MaxWidth);
            string formatter = "";
            bool writeprefix = (prefix != null && prefix.Length > 0);
            bool writepostfix = (postfix != null && postfix.Length > 0);

            if (writeprefix)
            {
                str.Append(prefix);
            }

            switch (options.Format)
            {
                case NumberFormat.SignedHex:
                    if (value < 0)
                    {
                        value = ((~(value)) + 1);
                        str.Append("-");
                    }
                    goto case NumberFormat.Hex;
                case NumberFormat.Hex:
                    str.Append("0x");
                    formatter = "X";
                    break;
                case NumberFormat.Dec:
                    formatter = "D";
                    break;
            }

            if (options.NumWidth != 0)
            {
                formatter += options.NumWidth.ToString();
            }

            str.Append(value.ToString(formatter));

            if (writepostfix)
            {
                str.Append(postfix);
            }

            int pad = Math.Max(options.MinWidth, (options.MaxWidth - str.Length));

            switch (options.Alignment)
            {
                case Align.Left:
                    str.Append(options.PadChar, pad);
                    break;
                case Align.Right:
                    str.Insert(0, new string(options.PadChar, pad));
                    break;
            }

            return str.ToString();
        }


        public string FormatString(string value, StringOptions options)
        {
            return FormatString(value, options, null, null);
        }
        public string FormatString(string value, StringOptions options, string prefix, string postfix)
        {
            if (value != null)
            {
                StringBuilder str = new StringBuilder(options.MaxWidth);

                bool useprefix = (prefix != null && prefix.Length > 0);
                bool usepostfix = (postfix != null && postfix.Length > 0);
                bool delaypad = (options.Alignment == Align.Left);

                int strlen = value.Length;
                if (useprefix)
                {
                    strlen += prefix.Length;
                }
                if (usepostfix)
                {
                    strlen += postfix.Length;
                }
                int pad = Math.Max(options.MinWidth, (options.MaxWidth - strlen));
                if (!delaypad)
                {
                    str.Append(options.PadChar, pad);
                }
                if (useprefix)
                {
                    str.Append(prefix);
                }
                str.Append(value);
                if (usepostfix)
                {
                    str.Append(prefix);
                }
                if (delaypad)
                {
                    str.Append(options.PadChar, pad);
                }
                return str.ToString();
            }
            else
            {
                return "";
            }
        }


        #region IDisposable Members

        public void Dispose()
        {
            lock (this)
            {
                l_op = null;
                l_offset = -1;
                l_params = null;
                l_extra = null;
                l_numparams = -1;
            }
            GC.SuppressFinalize(this);
        }

        #endregion
    }


    public class InstructionOffsetComparer : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            int retval = 0;
            Instruction dx = x as Instruction;
            int iy = (int)y;
            if (dx != null)
            {
                retval = dx.Offset.CompareTo(iy);
            }
            else
            {
                retval = -1;
            }
            return retval;
        }

        #endregion
    }

    public class InstructionList : IEnumerable, IDictionary
    {
        private SortedList list;

        public InstructionList() : this(10) { }
        public InstructionList(int capacity)
        {
            list = new SortedList(capacity);
        }

        ~InstructionList()
        {
            lock (this)
            {
                if (list != null)
                {
                    list.Clear();
                }
            }
        }


        public void Add(Instruction inst)
        {
            if (inst == null)
            {
                throw new ArgumentNullException();
            }
            if (list.Contains(inst.Offset))
            {
                throw new ArgumentException();
            }
            list.Add(inst.Offset, inst);
        }

        public Instruction this[int offset]
        {
            get
            {
                return list[offset] as Instruction;
            }
        }
        public Instruction this[ValInt offset]
        {
            get
            {
                return list[offset.Value] as Instruction;
            }
        }


        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return new InstructionListEnumerator(this);
        }

        #endregion

        #region IDictionary Members

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        IDictionaryEnumerator System.Collections.IDictionary.GetEnumerator()
        {
            return list.GetEnumerator();
        }

        public object this[object key]
        {
            get
            {
                if (key is ValInt || key is Int32 || key is Int64 || key is byte || key is Int16)
                {
                    return this[(ValInt)key];
                }
                else
                {
                    throw new Exception("the InstructionTable cannot be indexed by objects of type " + key.GetType().ToString());
                }
            }
            set
            {

            }
        }

        public void Remove(object key)
        {
            list.Remove(key);
        }

        public bool Contains(object key)
        {
            return list.Contains(key);
        }

        public void Clear()
        {
            list.Clear();
        }

        public ICollection Values
        {
            get
            {
                return list.Values;
            }
        }

        public void Add(object key, object value)
        {
            throw new Exception("the default dictionary add member of this type is not implemnted. Use Add(Instruction) instead");

        }

        public ICollection Keys
        {
            get
            {
                return list.Keys;
            }
        }

        public bool IsFixedSize
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region ICollection Members

        public bool IsSynchronized
        {
            get
            {
                return list.IsSynchronized;
            }
        }

        public int Count
        {
            get
            {
                return list.Count;
            }
        }

        public void CopyTo(Array array, int index)
        {
            list.CopyTo(array, index);
        }

        public object SyncRoot
        {
            get
            {
                return list.SyncRoot;
            }
        }

        #endregion

    }

    public class InstructionListEnumerator : IEnumerator
    {
        private ArrayList items;
        private int position;

        public InstructionListEnumerator(IDictionary dict)
        {
            items = new ArrayList(dict.Values);
            this.Reset();
        }


        #region IEnumerator Members

        public void Reset()
        {
            position = -1;
        }

        public object Current
        {
            get
            {
                if (position >= 0 && position < items.Count)
                {
                    return items[position];
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public bool MoveNext()
        {
            position += 1;
            return (position >= 0 && position < items.Count);
        }

        #endregion
    }

}
