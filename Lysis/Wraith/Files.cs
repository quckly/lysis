using System;

using Amx.Core;
using Amx.Strings;
using Amx.Logic.Debug;

namespace Files
{
    using System.IO;
    using System.Collections;

    public sealed class AmxHelpers
    {
        private AmxHelpers()
        {
        }

        public static AmxHeader GetHeader(string path)
        {
            AmxHeader retval = null;
            AmxHeader basic = null;
            Stream file = null;
            if (File.Exists(path))
            {
                try
                {
                    file = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                }
                catch
                {
                    file = null;
                }
            }

            if (file != null)
            {
                AmxStream reader = new AmxStream(file);

                basic = new AmxHeader(reader);

                switch (basic.FileVersion)
                {
                    case 4:
                        if (reader.Peek() < Int16.MaxValue)
                        {
                            retval = new AmxHeader_4_lin(reader, basic);
                        }
                        else
                        {
                            retval = new AmxHeader_4_win(reader, basic);
                        }
                        break;
                    case 5:
                        if (reader.Peek() < Int16.MaxValue)
                        {
                            retval = new AmxHeader_5_lin(reader, basic);
                        }
                        else
                        {
                            retval = new AmxHeader_5_win(reader, basic);
                        }
                        break;
                    case 6:
                        retval = new AmxHeader_6(reader, basic);
                        break;
                    case 7:
                        retval = new AmxHeader_7(reader, basic);
                        break;
                    case 8:
                        retval = new AmxHeader_8(reader, basic);
                        break;
                    default:
                        throw new Exception("Unknown amx file version, " + basic.FileVersion.ToString("D"));
                }
                if (retval != null)
                {
                    retval.LoadTables(reader);
                }
                reader.Close();
                reader = null;
            }

            return retval;
        }
        public static AmxHeader GetHeader(AmxStream reader)
        {
            AmxHeader basic = new AmxHeader(reader);
            AmxHeader retval = null;

            switch (basic.FileVersion)
            {
                case 3:
                case 4:
                    if (reader.Peek() < Int16.MaxValue)
                    {
                        retval = new AmxHeader_4_lin(reader, basic);
                    }
                    else
                    {
                        retval = new AmxHeader_4_win(reader, basic);
                    }
                    break;
                case 5:
                    if (reader.Peek() < Int16.MaxValue)
                    {
                        retval = new AmxHeader_5_lin(reader, basic);
                    }
                    else
                    {
                        retval = new AmxHeader_5_win(reader, basic);
                    }
                    break;
                case 6:
                    retval = new AmxHeader_6(reader, basic);
                    break;
                case 7:
                    retval = new AmxHeader_7(reader, basic);
                    break;
                case 8:
                    retval = new AmxHeader_8(reader, basic);
                    break;
                case 10:
                    retval = new AmxHeader_10(reader, basic);
                    break;
                default:
                    throw new Exception("Unknown amx file version, " + basic.FileVersion.ToString("D"));
            }
            if (retval != null)
            {
                retval.LoadTables(reader);
            }
            return retval;
        }

        public static AmxDebugHeader GetDebugHeader(AmxStream reader, Bits bits)
        {
            //TODO: idk much about debug headers, so there is possibility that exists diffrent variants of debug hedaders
            AmxDebugHeader retval = new AmxDebugHeader(reader);

            if (retval != null)
            {
                retval.LoadTables(reader, bits);
            }
            return retval;
        }

        public static MemoryStream decompress(AmxHeader header, string from)
        {
            BinaryWriter output;
            BinaryReader input;
            MemoryStream mem;
            System.Collections.Stack stack;
            byte[] code;

            input = new BinaryReader(new FileStream(from, FileMode.Open));
            mem = new MemoryStream((int)input.BaseStream.Length);
            output = new BinaryWriter(mem);

            output.Write(input.ReadBytes(header.Cod));
            input.BaseStream.Seek(header.Cod, SeekOrigin.Begin);

            code = new byte[header.Hea - header.Cod];
            input.BaseStream.Seek((long)header.Cod, SeekOrigin.Begin); // seek to the beginning of the code section
            code = input.ReadBytes(code.Length);
            input.Close();

            stack = new System.Collections.Stack(code.Length);

            uint c;      // 32 bit unsigned
            short shift; // 16 bit signed
            uint codesize = (uint)code.Length;

            while (codesize > 0)
            {
                c = 0;
                shift = 0;

                do
                {
                    codesize--;
                    c = c | (uint)(code[codesize] & 0x7f) << shift;
                    shift += 7;
                } while ((codesize > 0) && ((code[codesize - 1] & 0x80) != 0));

                if ((code[codesize] & 0x40) != 0)
                {
                    while (shift < (8 * 4))  // hard coded 4 byte long
                    {
                        c = c | (uint)0xff << shift;
                        shift += 8;
                    }
                }
                stack.Push(c);
            }
            for (int x = 0, counter = stack.Count; x < counter; x++) output.Write((uint)stack.Pop());
            output.Flush();
            output = null;

            return mem;
        }

        public static MemoryStream decompress(AmxHeader header, Stream from)
        {

            BinaryWriter output;
            BinaryReader input;
            MemoryStream mem;
            System.Collections.Stack stack;
            byte[] code;

            input = new BinaryReader(from);
            mem = new MemoryStream((int)input.BaseStream.Length);
            output = new BinaryWriter(mem);

            output.Write(input.ReadBytes(header.Cod));
            input.BaseStream.Seek(header.Cod, SeekOrigin.Begin);

            code = new byte[header.Hea - header.Cod];
            input.BaseStream.Seek((long)header.Cod, SeekOrigin.Begin); // seek to the beginning of the code section
            code = input.ReadBytes(code.Length);
            input.Close();

            stack = new System.Collections.Stack(code.Length);

            uint c;      // 32 bit unsigned
            short shift; // 16 bit signed
            uint codesize = (uint)code.Length;

            while (codesize > 0)
            {
                c = 0;
                shift = 0;

                do
                {
                    codesize--;
                    c = c | (uint)(code[codesize] & 0x7f) << shift;
                    shift += 7;
                } while ((codesize > 0) && ((code[codesize - 1] & 0x80) != 0));

                if ((code[codesize] & 0x40) != 0)
                {
                    while (shift < (8 * 4))  // hard coded 4 byte long
                    {
                        c = c | (uint)0xff << shift;
                        shift += 8;
                    }
                }
                stack.Push(c);
            }
            for (int x = 0, counter = stack.Count; x < counter; x++) output.Write((uint)stack.Pop());
            output.Flush();
            output = null;

            return mem;
        }

        public static bool ExportFile(IAmxInstance file, string outpath)
        {
            bool retval = false;
            if (file != null && file.Header.Valid && !File.Exists(outpath))
            {
                BinaryWriter writer = null;
                AmxStream reader = null;
                try
                {
                    writer = new BinaryWriter(File.OpenWrite(outpath));
                    reader = file.GetStream();
                    writer.Write(reader.ReadBytes(file.Length));
                    retval = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("an exception occured: ");
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }
                finally
                {
                    if (writer != null)
                    {
                        writer.Flush();
                        writer.Close();
                        writer = null;
                    }
                    if (reader != null)
                    {
                        reader.Close();
                        reader = null;
                    }
                }
            }
            return retval;
        }

        public static bool ExportFile(IAmxContainer container, Bits bits, string outpath)
        {
            IAmxInstance file = null;
            foreach (IAmxDescription d in container)
            {
                if (d.Bits == bits)
                {
                    file = container.GetFile(d);
                }
            }
            if (file != null)
            {
                return ExportFile(file, outpath);
            }
            return false;
        }

        public static bool ExportFile(string infile, Bits bits, string outpath)
        {
            return false;
        }

    }

    public class AmxHeader
    {
        public const int AMX_MAGIC = 0xf1e0;

        protected int size;
        protected int magic;
        protected int file_version;
        protected int amx_version;
        protected AMX_FLAGS flags;
        protected int defsize;

        protected Bits bithint;

        protected int cod;
        protected int cod_size;

        protected int dat;
        protected int dat_size;

        protected int hea;
        protected int stp;
        protected int cip;

        protected int natives_ptr;
        protected int names_ptr;
        protected int pubvars_ptr;
        protected int tags_ptr;
        protected int libraries_ptr;
        protected int publics_ptr;

        protected int publics_count;
        protected int natives_count;
        protected int libraries_count;
        protected int pubvars_count;
        protected int tags_count;

        protected int publics_location;
        protected int natives_location;
        protected int libraries_location;
        protected int pubvars_location;
        protected int tags_location;
        protected int names_location;

        protected AmxNative[] natives;
        protected AmxNative[] pubvars;
        protected AmxNative[] tags;
        protected AmxNative[] publics;
        protected AmxNative[] libraries;

        
        public AmxHeader( )
        { }

        public AmxHeader(AmxHeader header)
        {
            this.size = header.size;
            this.magic = header.magic;
            this.file_version = header.file_version;
            this.amx_version = header.amx_version;
            this.flags = header.flags;
            this.defsize = header.defsize;
            this.cod = header.cod;
            this.dat = header.dat;
            this.hea = header.hea;
            this.stp = header.stp;
            this.cip = header.cip;
            this.bithint = header.bithint;

            this.cod_size = header.dat - header.cod;
            this.dat_size = header.size - header.dat;//WRONG for plugins with COMPACT encoding... need to unpack and determine real size... For example v7 Header.Size = 4654 and Header.Data = 7068, it produce dat_size = -2414
        }
        public AmxHeader(AmxStream reader) : this(reader, Bits._32) { }
        public AmxHeader(AmxStream reader, Bits @default_bits)
        {
            size = reader.ReadInt32();
            magic = reader.ReadUInt16();
            file_version = reader.ReadByte();
            amx_version = reader.ReadByte();
            flags = (AMX_FLAGS)reader.ReadInt16();
            defsize = reader.ReadInt16();
            cod = reader.ReadInt32();
            dat = reader.ReadInt32();
            hea = reader.ReadInt32();
            stp = reader.ReadInt32();
            cip = reader.ReadInt32();
            bithint = @default_bits;

            cod_size = dat - cod;
            dat_size = size - dat; //WRONG for plugins with COMPACT encoding... need to unpack and determine real size... For example v7 Header.Size = 4654 and Header.Data = 7068, it produce dat_size = -2414
        }

        #region Acessors

        public virtual int Length
        {
            get
            {
                return size;
            }
        }
        public virtual int Magic
        {
            get
            {
                return magic;
            }
        }
        public virtual int FileVersion
        {
            get
            {
                return file_version;
            }
        }
        public virtual int AmxVersion
        {
            get
            {
                return amx_version;
            }
        }
        public virtual AMX_FLAGS Flags
        {
            get
            {
                return flags;
            }
        }
        //public virtual int DefSize
        //{
        //    get
        //    {
        //        return defsize;
        //    }
        //}


        public Bits BitHint
        {
            get
            {
                return bithint;
            }
        }


        public virtual bool Valid
        {
            get
            {
                return (magic == AMX_MAGIC);
            }
        }


        public virtual int Cod
        {
            get
            {
                return cod;
            }
        }

        public virtual int CodSize
        {
            get
            {
                return cod_size;
            }
        }

        public virtual int DatSize
        {
            get
            {
                return dat_size;
            }
        }

        public virtual int Dat
        {
            get
            {
                return dat;
            }
        }
        public virtual int Hea
        {
            get
            {
                return hea;
            }
        }
        public virtual int Stp
        {
            get
            {
                return stp;
            }
        }
        public virtual int Cip
        {
            get
            {
                return cip;
            }
        }


        public virtual AmxNative[] Natives
        {
            get
            {
                return natives;
            }
        }

        public virtual AmxNative[] Publics
        {
            get
            {
                return publics;
            }
        }

        public virtual AmxNative[] Tags
        {
            get
            {
                return tags;
            }
        }

        public virtual AmxNative[] Vars
        {
            get
            {
                return pubvars;
            }
        }

        public virtual AmxNative[] Libraries
        {
            get
            {
                return libraries;
            }
        }
        #endregion

        public bool GetFlag(AMX_FLAGS flag)
        {
            return ((flags & flag) == flag);
        }


        public virtual void LoadTables(Stream reader)
        {
            AmxStream bin = new AmxStream(reader);
            this.LoadTables(bin);
        }
        public virtual void LoadTables(AmxStream reader)
        {
            if (publics_ptr != -1)
            {
                publics = LoadTable(reader, this.bithint, publics_ptr, publics_count, defsize);
                if (!ValidateNatives(publics, false))
                {
                    this.bithint = IncreaseBitHint(this.bithint);
                }
                publics = LoadTable(reader, this.bithint, publics_ptr, publics_count, defsize);
            }
            if (natives_ptr != -1)
            {
                natives = LoadTable(reader, this.bithint, natives_ptr, natives_count, defsize);
            }
            if (libraries_ptr != -1)
            {
                libraries = LoadTable(reader, this.bithint, libraries_ptr, libraries_count, defsize);
            }
            if (pubvars_ptr != -1)
            {
                pubvars = LoadTable(reader, this.bithint, pubvars_ptr, pubvars_count, defsize);
            }
            if (publics_ptr != -1)
            {
                tags = LoadTable(reader, this.bithint, tags_ptr, tags_count, defsize);
            }
        }


        protected bool ValidateNatives(AmxNative[] table, bool checkzero)
        {
            bool invalid = false;
            if (checkzero)
            {
                if (table.Length > 0)
                {
                    foreach (AmxNative nat in table)
                    {
                        if (nat.Data == null || nat.Address == 0)
                        {
                            invalid = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                if (table.Length > 0)
                {
                    foreach (AmxNative nat in table)
                    {
                        if (nat.Data == null)
                        {
                            invalid = true;
                            break;
                        }
                    }
                }
            }
            return !invalid; ;
        }

        protected static Bits IncreaseBitHint(Bits current)
        {
            switch (current)
            {
                case Bits._8: return Bits._16;
                case Bits._16: return Bits._32;
                case Bits._32: return Bits._64;
                default: throw new ArgumentException("bithint cannot be increased beyond 64 bits");
            }
        }


        private AmxNativeFixed[] LoadTable(AmxStream reader, Bits bits, long baseaddr, int number, int size)
        {
            AmxNativeFixed[] retval = null;
            if (number > 0)
            {
                retval = new AmxNativeFixed[number];
                long pos = reader.Position;
                reader.Seek(baseaddr, SeekOrigin.Begin);
                for (int i = 0; i < number; i++)
                {
                    retval[i] = new AmxNativeFixed(reader, size, bits);
                }
                reader.Seek(pos, SeekOrigin.Begin);
            }
            else
            {
                retval = new AmxNativeFixed[0];
            }
            return retval;
        }
    }

    public class AmxNative
    {
        protected string data;
        protected ValInt address;

        public virtual string Data
        {
            get
            {
                return data;
            }
        }
        public virtual int Address
        {
            get
            {
                return address;
            }
        }
    }

    public class AmxNativeFixed : AmxNative
    {
        public AmxNativeFixed(AmxStream reader, int size, Bits bits)
        {
            address = reader.ReadValInt(bits);
            long pos = reader.Position;

            byte[] bytes = reader.ReadBytes(size - (int)(bits));
            AsciiStringDecoder dec = new AsciiStringDecoder((int)bits);
            int strlen = dec.GetCharCount(bytes, 0, bytes.Length);
            reader.Seek(pos + strlen + 1, SeekOrigin.Begin);
            if (strlen > 0)
            {
                data = dec.GetString(bytes, 0, strlen);
            }
            //data = System.Text.ASCIIEncoding.ASCII.GetString(bytes,0,bytes.Length).TrimEnd('\0');
        }
        public AmxNativeFixed(ValInt addr, string dat)
        {
            address = addr;
            data = dat;
        }
    }

    public class AmxNativeVariable : AmxNative
    {
        private int stringaddress;

        public AmxNativeVariable(AmxStream reader, Bits bits)
        {
            int position = -1;
            address = reader.ReadValInt(bits);
            stringaddress = reader.ReadValInt(bits);
            if (stringaddress > 0 && stringaddress < reader.Length)
            {
                position = (int)reader.Position;
                reader.Seek((long)stringaddress, SeekOrigin.Begin);
                byte read = reader.ReadInt8();
                ArrayList list = new ArrayList(20);
                while (read != 0 && reader.Position < reader.Length)
                {
                    list.Add(read);
                    read = reader.ReadInt8();
                }
                if (read == 0 && list.Count > 0)
                {
                    byte[] bytes = (byte[])list.ToArray(typeof(byte));
                    AsciiStringDecoder dec = new AsciiStringDecoder((int)bits);
                    data = dec.GetString(bytes, 0, bytes.Length);
                    bytes = null;
                }
                list = null;

            }

            if (position > 0)
            {
                reader.Seek(position, SeekOrigin.Begin);
            }

        }
    }

    [Flags()]
    public enum AMX_FLAGS : int
    {
        CHAR16 = 0x1,
        DEBUG = 0x2,
        COMPACT = 0x4,
        BIGENDIAN = 0x8,
        NOCHECKS = 0x10
    }


    public class AmxDebugHeader
    {
        public const int AMX_DBG_MAGIC = 0xf1ef;

        protected int size;           /* size of the debug information chunk */
        protected int magic;        /* signature, must be 0xf1ef */
        protected int file_version; /* file format version */
        protected int amx_version;  /* required version of the AMX */
        protected int flags;        /* currently unused */
        protected int files_count;        /* number of entries in the "file" table */
        protected int lines_count;        /* number of entries in the "line" table */
        protected int symbols_count;      /* number of entries in the "symbol" table */
        protected int tags_count;         /* number of entries in the "tag" table */
        protected int automatons_count;   /* number of entries in the "automaton" table */
        protected int states_count;       /* number of entries in the "state" table */

        protected long files_ptr;            /* start of files section*/
        //private int defsize;

        protected SymbolTable files = new SymbolTable();    //File starts from Line number x
        protected SymbolTable lines = new SymbolTable();    //Line x starts from Offset y in Cod section 
        protected SymbolTable globalsymbols = new SymbolTable();
        protected SymbolTable functionsymbols = new SymbolTable();
        protected ArrayList localSymbols = new ArrayList();
        protected ArrayList tags = new ArrayList();
        protected ArrayList automatons = new ArrayList();
        protected ArrayList states = new ArrayList();

        public AmxDebugHeader()
        {

        }

        public AmxDebugHeader(AmxStream reader)
        {
            this.size = reader.ReadInt32();
            this.magic = reader.ReadUInt16();
            this.file_version = reader.ReadByte();
            this.amx_version = reader.ReadByte();
            this.flags = reader.ReadInt16();
            this.files_count = reader.ReadInt16();
            this.lines_count = reader.ReadInt16();
            this.symbols_count = reader.ReadInt16();
            this.tags_count = reader.ReadInt16();
            this.automatons_count = reader.ReadInt16();
            this.states_count = reader.ReadInt16();

            this.files_ptr = reader.Position;
            //this.defsize = (int)(reader.Length - reader.Position);
        }

        #region Public acessors
        public int Flags
        {
            get { return flags; }
        }

        public int NumberOfFiles
        {
            get { return files_count; }
        }

        public int NumberOfLines
        {
            get { return lines_count; }
        }

        public int NumberOfSymbols
        {
            get { return symbols_count; }
        }

        public int NumberOfTags
        {
            get { return tags_count; }
        }

        public int NumberOfAutomatons
        {
            get { return automatons_count; }
        }

        public int NumberOfStates
        {
            get { return states_count; }
        }

        public int FileVersion
        {
            get { return file_version; }
        }

        public int AmxVersion
        {
            get { return amx_version; }
        }

        //public int Length
        //{
        //    get { return size; }
        //}

        public bool Valid
        {
            get
            {
                return (magic == AMX_DBG_MAGIC);
            }
        }

        public SymbolTable FileTable
        {
            get
            {
                return files;
            }
        }
        public SymbolTable LinesTable
        {
            get
            {
                return lines;
            }
        }
        public SymbolTable FunctionSymbolTable
        {
            get
            {
                return functionsymbols;
            }
        }
        public SymbolTable GlobalSymbolTable
        {
            get
            {
                return globalsymbols;
            }
        }

        public ArrayList LocalSymbolTable
        {
            get
            {
                return localSymbols;
            }
        }

        public ArrayList TagTable
        {
            get
            {
                return tags;
            }
        }

        public ArrayList AutomatonsTable
        {
            get
            {
                return automatons;
            }
        }
        public ArrayList StateTable
        {
            get
            {
                return states;
            }
        }
        #endregion

        public virtual void LoadTables(Stream reader, Bits bits)
        {
            AmxStream bin = new AmxStream(reader);
            this.LoadTables(bin, bits);
        }

        public virtual void LoadTables(AmxStream reader, Bits bits)
        {
            reader.Seek(files_ptr, SeekOrigin.Begin);

            if (files_count > 0)
            {
                for (int i = 0; i < files_count; i++)
                {
                    ValInt address;
                    address = new ValInt(reader.ReadIntValue(bits)); /* address in the code segment where generated code (for this file) starts */
                    string name = reader.ReadString();               /* ASCII string, zero-terminated */

                    files.Add((int)address, new SFile((int)address, name));
                }
            }

            if (lines_count > 0)
            {
                for (int i = 0; i < lines_count; i++)
                {
                    ValInt address = reader.ReadValInt(bits); /* address in the code segment where generated code (for this line) starts */
                    uint lineNumber = reader.ReadUInt32();    /* ASCII string, zero-terminated */

                    lines.Add((int)address, new SLine((int)address, lineNumber));
                }
            }

            if (symbols_count > 0)
            {
                for (int i = 0; i < symbols_count; i++)
                {
                    ValInt address = reader.ReadValInt(bits);   /* address in the data segment or relative to the frame */
                    int tag = reader.ReadInt16();               /* tag for the symbol */
                    ValInt codestart = reader.ReadValInt(bits); /* address in the code segment from which this symbol is valid (in scope) */
                    ValInt codeend = reader.ReadValInt(bits);   /* address in the code segment until which this symbol is valid (in scope) */
                    SymbolFlagType ident = (SymbolFlagType)(reader.ReadByte());  /* kind of symbol (function/variable) */
                    SymbolFlagClass vclass = (SymbolFlagClass)(reader.ReadByte());/* class of symbol (global/local) */
                    int dim = reader.ReadInt16();               /* number of dimensions */
                    string name = reader.ReadString();

                    //BUG: BUG BUG BUG from _64 section of amxx files, we cant read proper codeend, ident and vclass, alos some strange things with address!
                    //Check: Not shure about Position
                    Symbol symb = null;
                    if (bits == Bits._32) //we need it coz for _64 symbol table bugged, also its not supported by AMXX
                    {
                        symb = new Symbol((int)address, codestart, ident, vclass, name);
                        symb.AddExtension(new SymbolTag((int)address, tag));
                    }

                    for (int j = 0; j < dim; j++)
                    {
                        int tag_symdim = reader.ReadInt16();            /* tag for the array dimension */
                        ValInt size_symdim = reader.ReadValInt(bits);   /* size of the array dimension */

                        //TODO: check using of SymbolRange, is params equal to old params
                        if (bits == Bits._32) //we need it coz for _64 symbol table bugged, also its not supported by AMXX
                        {
                            var sr = new SymbolRange((int)address, tag_symdim, size_symdim);
                            symb.AddExtension(sr);
                        }
                    }

                    if (bits == Bits._32) //we need it coz for _64 symbol table bugged, also its not supported by AMXX
                        if (vclass == SymbolFlagClass.local)
                        {
                            localSymbols.Add(symb);
                        }
                        else
                        {
                            if (ident == SymbolFlagType.function || ident == SymbolFlagType.function_ptr)
                            {
                                if (!functionsymbols.Contains((int)address))
                                    functionsymbols.Add((int)address, symb);
                            }
                            else
                            {
                                if (!globalsymbols.Contains((int)address))
                                    globalsymbols.Add((int)address, symb);
                            }

                        }
                }
            }

            if (tags_count > 0)
            {
                for (int i = 0; i < tags_count; i++)
                {
                    int tag = reader.ReadInt16();      /* tag id */
                    string name = reader.ReadString(); /* ASCII string, zero-terminated */
                    tags.Add(new SymbolTagRealization(tag, name));
                }
            }

            if (automatons_count > 0)
            {
                for (int i = 0; i < automatons_count; i++)
                {
                    int automaton = reader.ReadInt16();       /* automaton id */
                    ValInt address = reader.ReadValInt(bits); /* address of state variable */
                    string name = reader.ReadString();        /* ASCII string, zero-terminated */

                    automatons.Add(new Automaton(automaton, address, name));
                }
            }

            if (states_count > 0)
            {
                for (int i = 0; i < states_count; i++)
                {
                    int state = reader.ReadInt16();           /* state id */
                    int automaton = reader.ReadInt16();       /* automaton id */
                    string name = reader.ReadString();        /* ASCII string, zero-terminated */

                    states.Add(new DebugState(state, automaton, name));
                }
            }
        }

        public void WriteDebugHeader(Stream writer)
        {
            if (writer == null || !writer.CanWrite)
            {
                return;
            }

            BinaryWriter bw = new BinaryWriter(writer);
            bw.Write((int)size);
            bw.Write((char)magic);
            bw.Write((byte)file_version);
            bw.Write((byte)amx_version);
            bw.Write((char)flags);
            bw.Write((char)files_count);
            bw.Write((char)lines_count);
            bw.Write((char)symbols_count);
            bw.Write((char)tags_count);
            bw.Write((char)automatons_count);
            bw.Write((char)states_count);

        }
    }

    #region comments
    //typedef struct tagAMX_DBG_FILE {
    //  ucell   address       PACKED; /* address in the code segment where generated code (for this file) starts */
    //  const char name[1]; 			/* ASCII string, zero-terminated */
    //} AMX_DBG_FILE;

    //typedef struct tagAMX_DBG_LINE {
    //  ucell   address       PACKED; /* address in the code segment where generated code (for this line) starts */
    //  int32_t line          PACKED; /* line number */
    //} AMX_DBG_LINE;

    //typedef struct tagAMX_DBG_SYMBOL {
    //  ucell   address       PACKED; /* address in the data segment or relative to the frame */
    //  int16_t tag           PACKED; /* tag for the symbol */
    //  ucell   codestart     PACKED; /* address in the code segment from which this symbol is valid (in scope) */
    //  ucell   codeend       PACKED; /* address in the code segment until which this symbol is valid (in scope) */
    //  char    ident;				/* kind of symbol (function/variable) */
    //  char    vclass; 				/* class of symbol (global/local) */
    //  int16_t dim           PACKED; /* number of dimensions */
    //  const char name[1]; 			/* ASCII string, zero-terminated */
    //} AMX_DBG_SYMBOL;

    //typedef struct tagAMX_DBG_SYMDIM {
    //  int16_t tag           PACKED; /* tag for the array dimension */
    //  ucell   size          PACKED; /* size of the array dimension */
    //} AMX_DBG_SYMDIM;

    //typedef struct tagAMX_DBG_TAG {
    //  int16_t tag           PACKED; /* tag id */
    //  const char name[1];			/* ASCII string, zero-terminated */
    //} AMX_DBG_TAG;

    //typedef struct tagAMX_DBG_MACHINE {
    //  int16_t automaton     PACKED; /* automaton id */
    //  ucell address         PACKED; /* address of state variable */
    //  const char name[1]; 			/* ASCII string, zero-terminated */
    //} AMX_DBG_MACHINE;

    //typedef struct tagAMX_DBG_STATE {
    //  int16_t state         PACKED; /* state id */
    //  int16_t automaton     PACKED; /* automaton id */
    //  const char name[1]; 			/* ASCII string, zero-terminated */
    //} AMX_DBG_STATE;

    //typedef struct tagAMX_DBG {
    //  AMX_DBG_HDR     _FAR *hdr         PACKED; /* points to the AMX_DBG header */
    //  AMX_DBG_FILE    _FAR **filetbl    PACKED;
    //  AMX_DBG_LINE    _FAR *linetbl     PACKED;
    //  AMX_DBG_SYMBOL  _FAR **symboltbl  PACKED;
    //  AMX_DBG_TAG     _FAR **tagtbl     PACKED;
    //  AMX_DBG_MACHINE _FAR **automatontbl PACKED;
    //  AMX_DBG_STATE   _FAR **statetbl   PACKED;
    //} AMX_DBG;

    #endregion

}
