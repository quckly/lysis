﻿using System;
using Amx.Core;

namespace Files
{
    using System.IO;
    using System.Collections;

    public class AmxStream : Stream
    {
        private Stream basestream;
        //private long bytesread;
        private long length;
        private byte[] buffer;

        public AmxStream(Stream stream)
            : this(stream, stream.Length)
        {

        }

        public AmxStream(Stream stream, long length)
        {
            lock (this)
            {
                buffer = new byte[8];
                basestream = stream;
                this.length = length;
                //bytesread = 0;
            }
        }

        ~AmxStream()
        {
            lock (this)
            {
                if (basestream != null)
                {
                    basestream.Close();
                    basestream = null;
                }
            }
        }


        public sealed override bool CanRead
        {
            get
            {
                return basestream.CanRead;
            }
        }

        public sealed override bool CanSeek
        {
            get
            {
                return basestream.CanSeek;
            }
        }

        public sealed override bool CanWrite
        {
            get
            {
                return false;
            }
        }


        [System.Diagnostics.DebuggerStepThroughAttribute]
        public sealed override int Read(byte[] buffer, int offset, int count)
        {
            int read = basestream.Read(buffer, offset, count);
            //bytesread += read;
            return read;
        }

        [System.Diagnostics.DebuggerStepThroughAttribute]
        public sealed override int ReadByte()
        {
            int read = basestream.ReadByte();
            //bytesread += read;
            return read;
        }

        [System.Diagnostics.DebuggerStepThroughAttribute]
        public sealed override long Seek(long offset, SeekOrigin origin)
        {
            long retval = 0;
            if (basestream.CanSeek)
            {
                retval = basestream.Seek(offset, origin);
            }
            return retval;
        }

        public sealed override void Write(byte[] buffer, int offset, int count)
        {
            return;
        }

        public sealed override void WriteByte(byte value)
        {
            return;
        }

        public sealed override void Close()
        {
            if (basestream != null)
            {
                basestream.Close();
            }
        }


        #region redirects to the base stream
        public sealed override void Flush()
        {
            basestream.Flush();
        }

        public sealed override void SetLength(long value)
        {
            return;
        }

        public sealed override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
        {
            return basestream.CreateObjRef(requestedType);
        }

        /*protected sealed override System.Threading.WaitHandle CreateWaitHandle()
        {
            return this.CreateWaitHandle();
        }*/

        public sealed override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
        {
            return basestream.BeginRead(buffer, offset, count, callback, state);
        }

        public sealed override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
        {
            return basestream.BeginWrite(buffer, offset, count, callback, state);
        }
        public sealed override int EndRead(IAsyncResult asyncResult)
        {
            return basestream.EndRead(asyncResult);
        }
        public sealed override void EndWrite(IAsyncResult asyncResult)
        {
            basestream.EndWrite(asyncResult);
        }

        public sealed override bool Equals(object obj)
        {
            return basestream.Equals(obj);
        }
        public sealed override int GetHashCode()
        {
            return basestream.GetHashCode();
        }
        public sealed override object InitializeLifetimeService()
        {
            return basestream.InitializeLifetimeService();
        }
        public sealed override string ToString()
        {
            return basestream.ToString();
        }

        #endregion


        public sealed override long Position
        {
            get
            {
                return basestream.Position;
            }
            set
            {
            }
        }


        public sealed override long Length
        {
            get
            {
                return length;
            }
        }

        [System.Diagnostics.DebuggerStepThroughAttribute]
        public byte ReadInt8()
        {
            int read = basestream.ReadByte();
            //if (read != -1)
            //{
            //    bytesread += read;
            //}
            return (byte)read;


        }
        [System.Diagnostics.DebuggerStepThroughAttribute]
        public Int16 ReadInt16()
        {
            if (this.Read(buffer, 0, 2) == 2)
            {
                return (Int16)ToInt(buffer, 2, 0);
            }
            else
            {
                throw new ArgumentException("could not read required umber of characters from the basestream");
            }
        }
        [System.Diagnostics.DebuggerStepThroughAttribute]
        public Int32 ReadInt32()
        {
            if (this.Read(buffer, 0, 4) == 4)
            {
                return (Int32)ToInt(buffer, 4, 0);
            }
            else
            {
                throw new ArgumentException("could not read required umber of characters from the basestream");
            }
        }

        [System.Diagnostics.DebuggerStepThroughAttribute]
        public Int64 ReadInt64()
        {
            if (this.Read(buffer, 0, 8) == 8)
            {
                return (Int64)ToInt(buffer, 8, 0);
            }
            else
            {
                throw new ArgumentException("could not read required umber of characters from the basestream");
            }
        }

        [System.Diagnostics.DebuggerStepThroughAttribute]
        public UInt16 ReadUInt16()
        {
            if (this.Read(buffer, 0, 2) == 2)
            {
                return (UInt16)ToInt(buffer, 2, 0);
            }
            else
            {
                throw new ArgumentException("could not read required umber of characters from the basestream");
            }
        }
        [System.Diagnostics.DebuggerStepThroughAttribute]
        public UInt32 ReadUInt32()
        {
            if (this.Read(buffer, 0, 4) == 4)
            {
                return (UInt32)ToInt(buffer, 4, 0);
            }
            else
            {
                throw new ArgumentException("could not read required umber of characters from the basestream");
            }
        }

        [System.Diagnostics.DebuggerStepThroughAttribute]
        public UInt64 ReadUInt64()
        {
            if (this.Read(buffer, 0, 8) == 8)
            {
                return (UInt64)ToInt(buffer, 8, 0);
            }
            else
            {
                throw new ArgumentException("could not read required umber of characters from the basestream");
            }
        }

        [System.Diagnostics.DebuggerStepThroughAttribute]
        public int Peek()
        {
            if (basestream != null)
            {
                long position = basestream.Position;
                Int32 retval = this.ReadInt32();
                basestream.Seek(position, SeekOrigin.Begin);
                return retval;
            }
            else
            {
                throw new ObjectDisposedException("");
            }
        }

        [System.Diagnostics.DebuggerStepThroughAttribute]
        public byte[] ReadBytes(int count)
        {
            byte[] retval;
            int dim = 0;
            int read = 0;
            //if (count > (basestream.Length - bytesread))
            //{
            //    dim = (int)(basestream.Length - bytesread);
            //}
            //else
            //{
                dim = count;
            //}
            retval = new byte[dim];
            read = basestream.Read(retval, 0, dim);
            //bytesread += read;
            if (read != dim)
            {
                retval = null;
                throw new IOException("number of bytes requested of the stream could not be read");
            }
            return retval;
        }

        //[System.Diagnostics.DebuggerStepThroughAttribute]
        public string ReadString()
        {
            //TODO: check for length and position...
            string retval = "";
            

            while (basestream.Position < basestream.Length)
            {
                char ch = (char)basestream.ReadByte();
                if (ch == 0)
                    break;
                retval += (ch );
            }

            return retval;

        }

        [System.Diagnostics.DebuggerStepThroughAttribute]
        public ValueType ReadIntValue(Bits bits)
        {
            ValueType retval;
            switch (bits)
            {
                case Bits._8:
                    retval = (ValueType)this.ReadByte();
                    break;
                case Bits._16:
                    retval = (ValueType)this.ReadInt16();
                    break;
                case Bits._32:
                    retval = (ValueType)this.ReadInt32();
                    break;
                case Bits._64:
                    retval = (ValueType)this.ReadInt64();
                    break;
                default:
                    throw new Exception("cannot read " + bits.ToString("D") + " from the basestream");
                //break;
            }
            return retval;
        }

        [System.Diagnostics.DebuggerStepThroughAttribute]
        public ValInt ReadValInt(Bits bits)
        {
            ValInt retval;
            switch (bits)
            {
                case Bits._8:
                    retval = new ValInt(this.ReadByte());
                    break;
                case Bits._16:
                    retval = new ValInt(this.ReadInt16());
                    break;
                case Bits._32:
                    retval = new ValInt(this.ReadInt32());
                    break;
                case Bits._64:
                    retval = new ValInt(this.ReadInt64());
                    break;
                default:
                    throw new Exception("cannot read " + bits.ToString("D") + " from the basestream");
                //break;
            }
            return retval;
        }


        [System.Diagnostics.DebuggerStepThroughAttribute]
        public static long ToInt(byte[] bytes, int length, int offset)
        {
            long retval = 0L;
            if ((offset + length) <= bytes.Length)
            {
                for (byte i = 0; i < length; i += 1)
                {
                    retval |= (long)(((long)bytes[offset + i]) << (8 * i));
                }
            }
            return retval;
        }

    }
}
