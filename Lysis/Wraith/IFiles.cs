﻿using System;

using Amx.Core;

namespace Files
{
    using System.Collections;

    public interface IFiletype
    {
        string FriendyName
        {
            get;
        }
        string Description
        {
            get;
        }
        string Name
        {
            get;
        }
        string Extension
        {
            get;
        }

        string FilterText
        {
            get;
        }
        string FilterMask
        {
            get;
        }


        IAmxContainer GetContainer(string path);
        IAmxInstance GetInstance(string path, Bits bits);
    }

    public interface IAmxContainer : IEnumerable
    {
        int Count
        {
            get;
        }
        string Path
        {
            get;
        }
        string Name
        {
            get;
        }
        AmxStream GetStream(IAmxDescription description);
        IAmxInstance GetFile(IAmxDescription description);
    }

    public interface IAmxDescription
    {
        /// <summary>
        /// Returns the bittedness of the entry
        /// </summary>
        Bits Bits
        {
            get;
        }
        bool Valid
        {
            get;
        }
        /// <summary>
        /// Returns the plugin name as an extensionless name
        /// </summary>
        string Name
        {
            get;
        }
        /// <summary>
        /// Returns the full path to the plugin
        /// </summary>
        string Path
        {
            get;
        }


        int Start
        {
            get;
        }
        int Length
        {
            get;
        }
    }

    public interface IAmxInstance : IAmxDescription
    {
        AmxHeader Header
        {
            get;
        }

        AmxDebugHeader DebugHeader
        {
            get;
        }

        string InfoString
        {
            get;
        }
        AmxStream GetStream();
        IAmxContainer Container
        {
            get;
        }
        IAmxDescription Description
        {
            get;
        }
    }

}
