using System;

namespace Amx.Ops
{
    using System.Collections;
    using System.Collections.Specialized;
    using System.IO;
    using System.Text.RegularExpressions;

    public class Op
    {
        private OPCODES l_code;
        private string[] l_params = null;
        private string l_name;
        private string l_description;
        private bool l_varargs;


        public Op(OPCODES code, string name, string param, string description)
        {
            l_code = code;
            l_name = name;
            if (param.IndexOf("...") == -1)
            {
                l_varargs = false;
            }
            else
            {
                l_varargs = true;
            }
            string[] t_params = param.Split(new char[] { ' ' });
            int count = 0;
            foreach (string s in t_params)
            {
                if (s != "")
                {
                    count += 1;
                }
            }
            l_params = new string[count];
            if (count > 0)
            {
                //l_params = new string[count];
                count = 0;
                foreach (string s in t_params)
                {
                    if (s != "")
                    {
                        l_params[count] = s;
                        count += 1;
                    }
                }
            }
            l_description = description;
        }


        public string Name
        {
            get
            {
                return l_name;
            }
        }

        public string[] Params
        {
            get
            {
                return l_params;
            }
        }

        public int ParamCount
        {
            get
            {
                int retval = 0;

                if (l_params != null)
                {
                    if (l_params.Length == 1 && l_params[0] == "")
                    {
                        retval = 0;
                    }
                    else
                    {
                        retval = l_params.Length;
                    }
                }


                return retval;
            }
        }
        public string Description
        {
            get
            {
                return l_description;
            }
        }

        public OPCODES Code
        {
            get
            {
                return l_code;
            }
        }

        public bool VariableLength
        {
            get
            {
                return l_varargs;
            }
        }

        /*public int ParamsCount
        {
            get
            {
                int retval = 0;
                if (!l_varargs)
                {
                    if (l_params!=null)
                    {
                        if (l_params.Length == 1 && l_params[0]=="")
                        {
                            retval = 0;
                        }
                        else
                        {
                            retval=l_params.Length;
                        }
                    }
                }
                else
                {
                    retval = -1;
                }

                return retval;
            }
        }
        */

        /*public bool Print
        {
            get
            {
                return l_print;
            }
            set
            {
                l_print = value;
            }
        }*/
        /*public int ParamLength
        {
            get
            {
                if (l_params==null)
                {
                    return 0;
                }
                else
                {
                    return l_params.Length;
                }
            }
        }
        */
    }

    public class Ops
    {
        private bool valid;
        private SortedList codes;
        private Hashtable names;
        private string l_file;
        private Regex regex;
        private Match match;

        public Ops(string path)
        {
            valid = false;
            l_file = path;
            regex = new System.Text.RegularExpressions.Regex("(.*?), (.*?), (.*?), (.*)$", System.Text.RegularExpressions.RegexOptions.Compiled);
            valid = loadfile();
        }

        private bool loadfile()
        {
            bool retval = true;
            codes = new SortedList(140);
            names = CollectionsUtil.CreateCaseInsensitiveHashtable(140);
            if (File.Exists(l_file))
            {
                StreamReader sr = null;
                try
                {
                    string line;
                    sr = new StreamReader(l_file);

                    while (sr.Peek() != -1)
                    {
                        line = sr.ReadLine();
                        match = regex.Match(line);
                        if (match.Groups.Count == 5)
                        {
                            int opnum = -1;
                            Op newop = null;
                            try
                            {
                                opnum = int.Parse(match.Groups[1].Value);
                            }
                            catch (FormatException)
                            {
                                opnum = -1;
                                continue;
                            }
                            catch (OverflowException)
                            {
                                opnum = -1;
                                continue;
                            }
                            if (opnum < 0)
                            {
                                throw new ArgumentOutOfRangeException("opcode number cannot be less than 0");
                            }
                            if (!Enum.IsDefined(typeof(OPCODES), opnum))
                            {
                                throw new ArgumentOutOfRangeException("opcode number " + opnum.ToString("D") + " is not present in the OPCODES enumeration");
                            }
                            if (codes.Contains(opnum))
                            {
                                throw new ArgumentException("the ops collection already contains the op number " + opnum.ToString("D"));
                            }

                            newop = new Op(
                                (OPCODES)opnum,
                                match.Groups[2].Value,
                                match.Groups[3].Value,
                                match.Groups[4].Value
                                );

                            codes.Add(opnum, newop);
                            names.Add(newop.Name, newop);

                        }
                        else
                        {
                            continue;
                        }
                    }
                }
                finally
                {
                    if (sr != null)
                    {
                        sr.Close();
                    }
                    sr = null;
                }
            }
            else
            {
                retval = false;
            }
            return retval;
        }

        public Op this[int index]
        {
            get
            {
                Op retval = codes[index] as Op;
                if (retval != null)
                {
                    return retval;
                }
                else
                {
                    throw new InvalidOpException("There is no opcode corresponding to the number " + index.ToString("D"));
                }
            }
        }

        public Op this[string name]
        {
            get
            {
                return names[name] as Op;
            }
        }

        public bool Valid
        {
            get
            {
                return valid;
            }
        }
    }

    public class OpListEnumerator : IEnumerator
    {
        private int position;
        private Op[] ops;

        public OpListEnumerator(SortedList oplist)
        {
            ops = null;
            oplist.Values.CopyTo(ops, 0);
        }


        #region IEnumerator Members

        public void Reset()
        {
            position = -1;
        }

        public object Current
        {
            get
            {
                if (position >= 0 && position < ops.Length)
                {
                    return ops[position];
                }
                else
                {
                    throw new InvalidOperationException("enumerator position is not valid");
                }
            }
        }

        public bool MoveNext()
        {
            position += 1;
            return (position < ops.Length);
        }

        #endregion
    }

    public class OpComparer : IComparer
    {
        #region IComparer Members
        public int Compare(object x, object y)
        {
            int retval = 0;
            Op xo = x as Op;
            Op yo = y as Op;
            if (xo != null && yo != null)
            {
                retval = xo.Code.CompareTo(yo.Code);
            }
            else if (xo == null)
            {
                retval = 1;
            }
            else
            {
                retval = -1;
            }
            // TODO:  Add OpComparer.Compare implementation
            return retval;
        }
        #endregion
    }

    public class OpIntComparer : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            int retval = 0;
            if (y is Int32)
            {
                int yi = (int)y;
                Op xo = x as Op;
                if (xo != null)
                {
                    retval = xo.Code.CompareTo(yi);
                }
                else if (xo == null)
                {
                    retval = 1;
                }
                // TODO:  Add OpComparer.Compare implementation
            }
            return retval;
        }

        #endregion
    }

    public class InvalidOpException : Exception
    {
        public InvalidOpException(string error)
            : base(error)
        {
        }

        public InvalidOpException()
        {
        }
    }

    public enum OPCODES : int
    {
        OP_NONE = 0,              // invalid opcode //
        OP_LOAD_PRI,
        OP_LOAD_ALT,
        OP_LOAD_S_PRI,
        OP_LOAD_S_ALT,
        OP_LREF_PRI,
        OP_LREF_ALT,
        OP_LREF_S_PRI,
        OP_LREF_S_ALT,
        OP_LOAD_I,
        OP_LODB_I,
        OP_CONST_PRI,
        OP_CONST_ALT,
        OP_ADDR_PRI,
        OP_ADDR_ALT,
        OP_STOR_PRI,
        OP_STOR_ALT,
        OP_STOR_S_PRI,
        OP_STOR_S_ALT,
        OP_SREF_PRI,
        OP_SREF_ALT,
        OP_SREF_S_PRI,
        OP_SREF_S_ALT,
        OP_STOR_I,
        OP_STRB_I,
        OP_LIDX,
        OP_LIDX_B,
        OP_IDXADDR,
        OP_IDXADDR_B,
        OP_ALIGN_PRI,
        OP_ALIGN_ALT,
        OP_LCTRL,
        OP_SCTRL,
        OP_MOVE_PRI,
        OP_MOVE_ALT,
        OP_XCHG,
        OP_PUSH_PRI,
        OP_PUSH_ALT,
        OP_PUSH_R,
        OP_PUSH_C,
        OP_PUSH,
        OP_PUSH_S,
        OP_POP_PRI,
        OP_POP_ALT,
        OP_STACK,
        OP_HEAP,
        OP_PROC,
        OP_RET,
        OP_RETN,
        OP_CALL,
        OP_CALL_PRI,
        OP_JUMP,
        OP_JREL,
        OP_JZER,
        OP_JNZ,
        OP_JEQ,
        OP_JNEQ,
        OP_JLESS,
        OP_JLEQ,
        OP_JGRTR,
        OP_JGEQ,
        OP_JSLESS,
        OP_JSLEQ,
        OP_JSGRTR,
        OP_JSGEQ,
        OP_SHL,
        OP_SHR,
        OP_SSHR,
        OP_SHL_C_PRI,
        OP_SHL_C_ALT,
        OP_SHR_C_PRI,
        OP_SHR_C_ALT,
        OP_SMUL,
        OP_SDIV,
        OP_SDIV_ALT,
        OP_UMUL,
        OP_UDIV,
        OP_UDIV_ALT,
        OP_ADD,
        OP_SUB,
        OP_SUB_ALT,
        OP_AND,
        OP_OR,
        OP_XOR,
        OP_NOT,
        OP_NEG,
        OP_INVERT,
        OP_ADD_C,
        OP_SMUL_C,
        OP_ZERO_PRI,
        OP_ZERO_ALT,
        OP_ZERO,
        OP_ZERO_S,
        OP_SIGN_PRI,
        OP_SIGN_ALT,
        OP_EQ,
        OP_NEQ,
        OP_LESS,
        OP_LEQ,
        OP_GRTR,
        OP_GEQ,
        OP_SLESS,
        OP_SLEQ,
        OP_SGRTR,
        OP_SGEQ,
        OP_EQ_C_PRI,
        OP_EQ_C_ALT,
        OP_INC_PRI,
        OP_INC_ALT,
        OP_INC,
        OP_INC_S,
        OP_INC_I,
        OP_DEC_PRI,
        OP_DEC_ALT,
        OP_DEC,
        OP_DEC_S,
        OP_DEC_I,
        OP_MOVS,
        OP_CMPS,
        OP_FILL,
        OP_HALT,
        OP_BOUNDS,
        OP_SYSREQ_PRI,
        OP_SYSREQ_C,
        OP_FILE,        /* obsolete since v8 or less */
        OP_LINE,        /* obsolete since v8 or less*/
        OP_SYMBOL,      /* obsolete since v8 or less*/
        OP_SRANGE,      /* obsolete since v8 or less*/
        OP_JUMP_PRI,
        OP_SWITCH,
        OP_CASETBL,     //130
        OP_SWAP_PRI,
        OP_SWAP_ALT,
        OP_PUSHADDR,
        OP_NOP,
        OP_SYSREQ_N,    ///* version 9 (replaces SYSREQ.D from earlier version)  
        OP_SYMTAG,      /* obsolete since v8 or less*/
        OP_BREAK,       //added in v8
        // ----- //
        OP_PUSH2_C,     // added in v8
        OP_PUSH2,       // added in v8
        OP_PUSH2_S,     // added in v8
        OP_PUSH2_ADR,   // added in v8
        OP_PUSH3_C,     // added in v8
        OP_PUSH3,       // added in v8
        OP_PUSH3_S,     // added in v8
        OP_PUSH3_ADR,   // added in v8
        OP_PUSH4_C,     // added in v8
        OP_PUSH4,       // added in v8
        OP_PUSH4_S,     // added in v8
        OP_PUSH4_ADR,   // added in v8
        OP_PUSH5_C,     // added in v8
        OP_PUSH5,       // added in v8
        OP_PUSH5_S,     // added in v8
        OP_PUSH5_ADR,   // added in v8
        OP_LOAD_BOTH,   // added in v8
        OP_LOAD_S_BOTH, // added in v8
        OP_CONST,       //added in v9
        OP_CONST_S,     //added in v9
        /* ----- */
        OP_SYSREQ_D,    //The pawn compiler never generates it. They are generated by the abstract machine itself
        OP_SYSREQ_ND,   //The pawn compiler never generates it. They are generated by the abstract machine itself
        /* ----- */
        OP_SM_TRACKER_PUSH_C,      //160
        OP_SM_TRACKER_POP_SETHEAP,
        OP_SM_GENARRAY,
        OP_SM_GENARRAY_Z
    }

    public static class OpHelper
    {
        public static bool isDebugOp(Op opcode)
        {
            if (opcode.Code == OPCODES.OP_FILE      //124, FILE, size ord name..., source file information pair: name and ordinal (see below)
                || opcode.Code == OPCODES.OP_LINE   //125, LINE, line ord, source line number and file ordinal (see below)
                || opcode.Code == OPCODES.OP_SYMBOL //126, SYMBOL, sze off flg name..., symbol information (see below)
                || opcode.Code == OPCODES.OP_SRANGE //127, SRANGE, lvl size, symbol range and dimensions (see below)
                || opcode.Code == OPCODES.OP_SYMTAG //136, SYMTAG, value, symbol tag
                || opcode.Code == OPCODES.OP_BREAK  //137, BREAK, , invokes optional debugger
                || opcode.Code == OPCODES.OP_BOUNDS //
                )
                return true;
            return false;
        }

        /// <summary>
        /// EQ.C.pri EQ.C.alt
        /// </summary>
        /// <param name="opcode"></param>
        /// <returns></returns>
        public static bool isCompareToConstOp(Op opcode)
        {
            if (opcode.Code == OPCODES.OP_EQ_C_ALT
                || opcode.Code == OPCODES.OP_EQ_C_PRI)
                return true;
            return false;
        }

        public static bool isCompareOp(Op opcode)
        {
            if (opcode.Code == OPCODES.OP_EQ      //95, EQ, , PRI = PRI == ALT ? 1 : 0
                || opcode.Code == OPCODES.OP_NEQ  //96, NEQ, , PRI = PRI != ALT ? 1 : 0
                || opcode.Code == OPCODES.OP_LESS //97, LESS, , PRI = PRI < ALT ? 1 : 0 (unsigned)
                || opcode.Code == OPCODES.OP_LEQ  //98, LEQ, , PRI = PRI < = ALT ? 1 : 0 (unsigned)
                || opcode.Code == OPCODES.OP_GRTR //99, GRTR, , PRI = PRI > ALT ? 1 : 0 (unsigned)
                || opcode.Code == OPCODES.OP_GEQ  //100, GEQ, , PRI = PRI > = ALT ? 1 : 0 (unsigned)
                || opcode.Code == OPCODES.OP_SLESS//101, SLESS, , PRI = PRI < ALT ? 1 : 0 (signed)
                || opcode.Code == OPCODES.OP_SLEQ //102, SLEQ, , PRI = PRI < = ALT ? 1 : 0 (signed)
                || opcode.Code == OPCODES.OP_SGRTR//103, SGRTR, , PRI = PRI > ALT ? 1 : 0 (signed)
                || opcode.Code == OPCODES.OP_SGEQ //104, SGEQ, , PRI = PRI > = ALT ? 1 : 0 (signed)
                || OpHelper.isCompareToConstOp(opcode)
                )
                return true;
            return false;
        }

        /// <summary>
        /// OP_JUMP OP_JREL OP_JUMP_PRI without SWITCH
        /// </summary>
        /// <param name="opcode"></param>
        /// <returns></returns>
        public static bool isUnconditionalJump(Op opcode)
        {
            if (opcode.Code == OPCODES.OP_JUMP
                || opcode.Code == OPCODES.OP_JREL
                || opcode.Code == OPCODES.OP_JUMP_PRI)
                return true;
            return false;
        }

        public static bool isConditionalJump(Op opcode)
        {
            if (opcode.Code == OPCODES.OP_JZER      //53, JZER, address, if PRI == 0 then CIP = [CIP + 1]
                || opcode.Code == OPCODES.OP_JNZ    //54, JNZ, address, if PRI != 0 then CIP = [CIP + 1]
                || opcode.Code == OPCODES.OP_JEQ    //55, JEQ, address, if PRI == ALT then CIP = [CIP + 1]
                || opcode.Code == OPCODES.OP_JNEQ   //56, JNEQ, address, if PRI != ALT then CIP = [CIP + 1]
                || opcode.Code == OPCODES.OP_JLESS  //57, JLESS, address, if PRI < ALT then CIP = [CIP + 1] (unsigned)
                || opcode.Code == OPCODES.OP_JLEQ   //58, JLEQ, address, if PRI < = ALT then CIP = [CIP + 1] (unsigned)
                || opcode.Code == OPCODES.OP_JGRTR  //59, JGRTR, address, if PRI > ALT then CIP = [CIP + 1] (unsigned)
                || opcode.Code == OPCODES.OP_JGEQ   //60, JGEQ, address, if PRI > = ALT then CIP = [CIP + 1] (unsigned)
                || opcode.Code == OPCODES.OP_JSLESS //61, JSLESS, address, if PRI < ALT then CIP = [CIP + 1] (signed)
                || opcode.Code == OPCODES.OP_JSLEQ  //62, JSLEQ, address, if PRI < = ALT then CIP = [CIP + 1] (signed)
                || opcode.Code == OPCODES.OP_JSGRTR //63, JSGRTR, address, if PRI > ALT then CIP = [CIP + 1] (signed)
                || opcode.Code == OPCODES.OP_JSGEQ  //64, JSGEQ, address, if PRI > = ALT then CIP = [CIP + 1] (signed)
                )
                return true;
            return false;
        }

        //public static bool isStackChangeOp(Op opcode)
        //{
        //    if (opcode.Code == OPCODES.OP_
        //        || opcode.Code == OPCODES.OP_
        //        || opcode.Code == OPCODES.OP_
        //        )
        //        return true;
        //    return false;
        //}

        public static bool isLogicWithAltOp(Op opcode)
        { //NOT FULL!
            if (opcode.Code == OPCODES.OP_SHL
                || opcode.Code == OPCODES.OP_SHR
                || opcode.Code == OPCODES.OP_SSHR
                || opcode.Code == OPCODES.OP_SHL_C_ALT //
                || opcode.Code == OPCODES.OP_SMUL
                || opcode.Code == OPCODES.OP_SDIV
                || opcode.Code == OPCODES.OP_SDIV_ALT //
                || opcode.Code == OPCODES.OP_UMUL
                || opcode.Code == OPCODES.OP_UDIV
                || opcode.Code == OPCODES.OP_UDIV_ALT //
                || opcode.Code == OPCODES.OP_ADD
                || opcode.Code == OPCODES.OP_SUB
                || opcode.Code == OPCODES.OP_SUB_ALT //
                || opcode.Code == OPCODES.OP_AND
                || opcode.Code == OPCODES.OP_OR
                || opcode.Code == OPCODES.OP_XOR
                )
                return true;
            return false;
        }

        public static bool isCallOp(Op opcode)
        {
            if (opcode.Code == OPCODES.OP_SYSREQ_C
                || opcode.Code == OPCODES.OP_CALL
                || opcode.Code == OPCODES.OP_SYSREQ_D
                )
                return true;
            return false;
        }
        
        public static bool isCallPriOp(Op opcode)
        {
            if (opcode.Code == OPCODES.OP_SYSREQ_PRI
                || opcode.Code == OPCODES.OP_CALL_PRI)
                return true;
            return false;
        }
        public static bool isReturnOp(Op opcode)
        {
            if (opcode.Code == OPCODES.OP_RET
                || opcode.Code == OPCODES.OP_RETN)
                return true;
            return false;
        }

        //public static bool isAltChanged(Op opcode)
        //{
        //    //TODO: implement
        //}

        //public static bool isPriChanged(Op opcode)
        //{
        //    //TODO: implement
        //}
    }
}
