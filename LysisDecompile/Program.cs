﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace LysisDecompile
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args != null && args[0] != null)
            {
                Process lysis = new Process();

                lysis.StartInfo.Arguments = "\"" + args[0] + "\"";
                lysis.StartInfo.RedirectStandardOutput = true;
                lysis.StartInfo.UseShellExecute = false;
                lysis.StartInfo.FileName = "Lysis.exe";

                lysis.Start();

                //lysis.BeginOutputReadLine();

                string stdout = lysis.StandardOutput.ReadToEnd();

                lysis.WaitForExit();

                using (StreamWriter write = new StreamWriter(args[0].Split('.')[0] + ".txt"))
                {
                    write.Write(stdout);
                }
            }
            else
            {
                Console.WriteLine("Example use:");
                Console.WriteLine("\tLysisDecompile.exe plugin.{amxx, smx}");
                Console.ReadKey();
            }
        }
    }
}
